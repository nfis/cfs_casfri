CREATE TABLE IF NOT EXISTS casfri50.version (
  casfri_version text,
  casfri_commit text,
  casfri_repository text,
  cfs_casfri_version text,
  cfs_casfri_commit text,
  cfs_casfri_repository text
);

INSERT INTO casfri50.version VALUES (
  'v5.4.1', '918f812c86d7709b2967ae54a113d46d9e4f55d5', 'https://github.com/mtcarto/CASFRI/tree/v5.4.1',
  'v1.1.0', 'cc3687aa1657cf5d6faabcfb1e140a49d6743407', 'https://gitlab.com/nfis/cfs_casfri/-/tree/v1.1.0/'
 );
