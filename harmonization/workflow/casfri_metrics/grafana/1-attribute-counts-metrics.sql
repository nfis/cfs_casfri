
-- Record count & percent of each attribute (non-error) by Inventory for cas_all
CREATE MATERIALIZED VIEW IF NOT EXISTS non_error_cas_all_attribute_counts AS
WITH counts AS (
	SELECT LEFT(cas_id, 4) AS inventory,
	count(*) AS total,
	sum  (casfri_area) AS total_area_ha,
	--uses cases within counts since counts can only count non values. so if a cell is an error, the count will regard it as null
	count(CASE isError(cas_id) WHEN FALSE THEN cas_id else NULL END) AS cas_id_count, 
	sum  (CASE isError(cas_id) WHEN FALSE THEN casfri_area else NULL END) AS cas_id_area_ha,
	count(CASE isError(inventory_id) WHEN FALSE THEN inventory_id else NULL END) AS inventory_id_count, 
	sum  (CASE isError(inventory_id) WHEN FALSE THEN casfri_area else NULL END) AS inventory_id_area_ha,
	count(CASE isError(orig_stand_id) WHEN FALSE THEN orig_stand_id else NULL END) AS orig_stand_id_count, 
	sum  (CASE isError(orig_stand_id) WHEN FALSE THEN casfri_area else NULL END) AS orig_stand_id_area_ha,
	count(CASE isError(stand_structure) WHEN FALSE THEN stand_structure else NULL END) AS stand_structure_count,
	sum  (CASE isError(stand_structure) WHEN FALSE THEN casfri_area else NULL END) AS stand_structure_area_ha,
	count(CASE isError(num_of_layers::text) WHEN FALSE THEN num_of_layers else NULL END) AS num_of_layers_count, 
	sum  (CASE isError(num_of_layers::text) WHEN FALSE THEN casfri_area else NULL END) AS num_of_layers_area_ha,
	count(CASE isError(map_sheet_id) WHEN FALSE THEN map_sheet_id else NULL END) AS map_sheet_id_count, 
	sum  (CASE isError(map_sheet_id) WHEN FALSE THEN casfri_area else NULL END) AS map_sheet_id_area_ha,
	count(CASE isError(casfri_area::text) WHEN FALSE THEN casfri_area else NULL END) AS casfri_area_count, 
	sum  (CASE isError(casfri_area::text) WHEN FALSE THEN casfri_area else NULL END) AS casfri_area_area_ha,
	count(CASE isError(casfri_perimeter::text) WHEN FALSE THEN casfri_perimeter else NULL END) AS casfri_perimeter_count, 
	sum  (CASE isError(casfri_perimeter::text) WHEN FALSE THEN casfri_area else NULL END) AS casfri_perimeter_area_ha,
	count(CASE isError(src_inv_area::text) WHEN FALSE THEN src_inv_area else NULL END) AS src_inv_area_count, 
	sum  (CASE isError(src_inv_area::text) WHEN FALSE THEN casfri_area else NULL END) AS src_inv_area_area_ha,
	count(CASE isError(stand_photo_year::text) WHEN FALSE THEN stand_photo_year else NULL END) AS stand_photo_year_count,
	sum  (CASE isError(stand_photo_year::text) WHEN FALSE THEN casfri_area else NULL END) AS stand_photo_year_area_ha
	FROM cas_all
	GROUP BY inventory
)
-- calculate percentages
SELECT counts.inventory, counts.total, 
counts.total_area_ha,
counts.cas_id_count, counts.cas_id_count*100/total AS cas_id_per,
counts.cas_id_area_ha,
counts.inventory_id_count, counts.inventory_id_count*100/total AS inventory_id_per,
counts.inventory_id_area_ha,
counts.orig_stand_id_count, counts.orig_stand_id_count*100/total AS orig_stand_id_per,
counts.orig_stand_id_area_ha,
counts.stand_structure_count, counts.stand_structure_count*100/total AS stand_structure_per,
counts.stand_structure_area_ha,
counts.num_of_layers_count, counts.num_of_layers_count*100/total AS num_of_layers_per,
counts.num_of_layers_area_ha,
counts.map_sheet_id_count, counts.map_sheet_id_count*100/total AS map_sheet_id_per,
counts.map_sheet_id_area_ha,
counts.casfri_area_count, counts.casfri_area_count*100/total AS casfri_area_per,
counts.casfri_area_area_ha,
counts.casfri_perimeter_count, counts.casfri_perimeter_count*100/total AS casfri_perimeter_per,
counts.casfri_perimeter_area_ha,
counts.src_inv_area_count, counts.src_inv_area_count*100/total AS src_inv_area_per,
counts.src_inv_area_area_ha,
counts.stand_photo_year_count, counts.stand_photo_year_count*100/total AS stand_photo_year_per,
counts.stand_photo_year_area_ha
FROM counts;


COMMENT ON MATERIALIZED VIEW non_error_cas_all_attribute_counts IS
'non_error_cas_all_attribute_counts provides the count of each non-error attribute in cas_all and the percentage of attributes that are not errors';


-- Record count & percent of each attribute (non-error) by Inventory for dst_all
CREATE MATERIALIZED VIEW IF NOT EXISTS non_error_dst_all_attribute_counts AS
WITH counts AS ( 
	SELECT LEFT(d.cas_id, 4) AS inventory,
	count(*) AS total,
	sum  (c.casfri_area) AS total_area_ha,
	--uses cases within counts since counts can only count non values. so if a cell is an error, the count will regard it as null
	count(CASE isError(d.cas_id) WHEN FALSE THEN d.cas_id else NULL END) AS cas_id_count, 
	sum  (CASE isError(d.cas_id) WHEN FALSE THEN c.casfri_area else NULL END) AS cas_id_area_ha,
	count(CASE isError(d.dist_type_1) WHEN FALSE THEN d.dist_type_1 else NULL END) AS dist_type_1_count, 
	sum  (CASE isError(d.dist_type_1) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_type_1_area_ha,
	count(CASE isError(d.dist_year_1::text) WHEN FALSE THEN d.dist_year_1 else NULL END) AS dist_year_1_count, 
	sum  (CASE isError(d.dist_year_1::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_year_1_area_ha,
	count(CASE isError(d.dist_ext_upper_1::text) WHEN FALSE THEN d.dist_ext_upper_1 else NULL END) AS dist_ext_upper_1_count,
	sum  (CASE isError(d.dist_ext_upper_1::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_ext_upper_1_area_ha,
	count(CASE isError(d.dist_ext_lower_1::text) WHEN FALSE THEN d.dist_ext_lower_1 else NULL END) AS dist_ext_lower_1_count,
	sum  (CASE isError(d.dist_ext_lower_1::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_ext_lower_1_area_ha, 
	count(CASE isError(d.dist_type_2) WHEN FALSE THEN d.dist_type_2 else NULL END) AS dist_type_2_count,
	sum  (CASE isError(d.dist_type_2) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_type_2_area_ha, 
	count(CASE isError(d.dist_year_2::text) WHEN FALSE THEN d.dist_year_2 else NULL END) AS dist_year_2_count, 
	sum  (CASE isError(d.dist_year_2::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_year_2_area_ha,
	count(CASE isError(d.dist_ext_upper_2::text) WHEN FALSE THEN d.dist_ext_upper_2 else NULL END) AS dist_ext_upper_2_count,
	sum  (CASE isError(d.dist_ext_upper_2::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_ext_upper_2_area_ha,
	count(CASE isError(d.dist_ext_lower_2::text) WHEN FALSE THEN d.dist_ext_lower_2 else NULL END) AS dist_ext_lower_2_count,
	sum  (CASE isError(d.dist_ext_lower_2::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_ext_lower_2_area_ha,
	count(CASE isError(d.dist_type_3) WHEN FALSE THEN d.dist_type_3 else NULL END) AS dist_type_3_count, 
	sum  (CASE isError(d.dist_type_3) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_type_3_area_ha,
	count(CASE isError(d.dist_year_3::text) WHEN FALSE THEN d.dist_year_3 else NULL END) AS dist_year_3_count, 
	sum  (CASE isError(d.dist_year_3::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_year_3_area_ha,
	count(CASE isError(d.dist_ext_upper_3::text) WHEN FALSE THEN d.dist_ext_upper_3 else NULL END) AS dist_ext_upper_3_count,
	sum  (CASE isError(d.dist_ext_upper_3::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_ext_upper_3_area_ha,
	count(CASE isError(d.dist_ext_lower_3::text) WHEN FALSE THEN d.dist_ext_lower_3 else NULL END) AS dist_ext_lower_3_count,
	sum  (CASE isError(d.dist_ext_lower_3::text) WHEN FALSE THEN c.casfri_area else NULL END) AS dist_ext_lower_3_area_ha,
	count(CASE isError(d.layer::text) WHEN FALSE THEN d.layer else NULL END) AS layer_count,
	sum  (CASE isError(d.layer::text) WHEN FALSE THEN c.casfri_area else NULL END) AS layer_area_ha
	FROM dst_all d
	INNER JOIN cas_all c ON c.cas_id = d.cas_id
	GROUP BY inventory
)
SELECT counts.inventory, counts.total, 
counts.total_area_ha,
counts.cas_id_count, counts.cas_id_count*100/total AS cas_id_per,
counts.cas_id_area_ha,
counts.dist_type_1_count, counts.dist_type_1_count*100/total AS dist_type_1_per,
counts.dist_type_1_area_ha,
counts.dist_year_1_count, counts.dist_year_1_count*100/total AS dist_year_1_per,
counts.dist_year_1_area_ha,
counts.dist_ext_upper_1_count, counts.dist_ext_upper_1_count*100/total AS dist_ext_upper_1_per,
counts.dist_ext_upper_1_area_ha,
counts.dist_ext_lower_1_count, counts.dist_ext_lower_1_count*100/total AS dist_ext_lower_1_per,
counts.dist_ext_lower_1_area_ha,
counts.dist_type_2_count, counts.dist_type_2_count*100/total AS dist_type_2_per,
counts.dist_type_2_area_ha,
counts.dist_year_2_count, counts.dist_year_2_count*100/total AS dist_year_2_per,
counts.dist_year_2_area_ha,
counts.dist_ext_upper_2_count, counts.dist_ext_upper_2_count*100/total AS dist_ext_upper_2_per,
counts.dist_ext_upper_2_area_ha,
counts.dist_ext_lower_2_count, counts.dist_ext_lower_2_count*100/total AS dist_ext_lower_2_per,
counts.dist_ext_lower_2_area_ha,
counts.dist_type_3_count, counts.dist_type_3_count*100/total AS dist_type_3_per,
counts.dist_type_3_area_ha,
counts.dist_year_3_count, counts.dist_year_3_count*100/total AS dist_year_3_per,
counts.dist_year_3_area_ha,
counts.dist_ext_upper_3_count, counts.dist_ext_upper_3_count*100/total AS dist_ext_upper_3_per,
counts.dist_ext_upper_3_area_ha,
counts.dist_ext_lower_3_count, counts.dist_ext_lower_3_count*100/total AS dist_ext_lower_3_per,
counts.dist_ext_lower_3_area_ha,
counts.layer_count, counts.layer_count*100/total AS layer_per,
counts.layer_area_ha
FROM counts;

COMMENT ON MATERIALIZED VIEW non_error_dst_all_attribute_counts IS
'non_error_dst_all_attribute_counts provides the count of each non-error attribute in dst_all and the percentage of attributes that are not errors';


-- Record count & percent of each attribute (non-error) by Inventory for eco_all
CREATE MATERIALIZED VIEW IF NOT EXISTS non_error_eco_all_attribute_counts AS
WITH counts AS (
	SELECT LEFT(e.cas_id, 4) AS inventory,
	count(*) AS total,
	sum  (c.casfri_area) AS total_area_ha,
	--uses cases within counts since counts can only count non values. so if a cell is an error, the count will regard it as null
	count(CASE isError(e.cas_id) WHEN FALSE THEN e.cas_id else NULL END) AS cas_id_count, 
	sum  (CASE isError(e.cas_id) WHEN FALSE THEN c.casfri_area else NULL END) AS cas_id_area_ha,
	count(CASE isError(e.wetland_type) WHEN FALSE THEN e.wetland_type else NULL END) AS wetland_type_count,
	sum  (CASE isError(e.wetland_type) WHEN FALSE THEN c.casfri_area else NULL END) AS wetland_type_area_ha,
	count(CASE isError(e.wet_veg_cover) WHEN FALSE THEN e.wet_veg_cover else NULL END) AS wet_veg_cover_count, 
	sum  (CASE isError(e.wet_veg_cover) WHEN FALSE THEN c.casfri_area else NULL END) AS wet_veg_cover_area_ha,
	count(CASE isError(e.wet_landform_mod) WHEN FALSE THEN e.wet_landform_mod else NULL END) AS wet_landform_mod_count,
	sum  (CASE isError(e.wet_landform_mod) WHEN FALSE THEN c.casfri_area else NULL END) AS wet_landform_mod_area_ha,
	count(CASE isError(e.wet_local_mod) WHEN FALSE THEN e.wet_local_mod else NULL END) AS wet_local_mod_count,
	sum  (CASE isError(e.wet_local_mod) WHEN FALSE THEN c.casfri_area else NULL END) AS wet_local_mod_area_ha, 
	count(CASE isError(e.eco_site) WHEN FALSE THEN e.eco_site else NULL END) AS eco_site_count,
	sum  (CASE isError(e.eco_site) WHEN FALSE THEN c.casfri_area else NULL END) AS eco_site_area_ha,
	count(CASE isError(e.layer::text) WHEN FALSE THEN e.layer else NULL END) layer_count,
	sum  (CASE isError(e.layer::text) WHEN FALSE THEN c.casfri_area else NULL END) AS layer_area_ha
	FROM eco_all e
	INNER JOIN cas_all c ON c.cas_id = e.cas_id
	GROUP BY inventory
)
SELECT counts.inventory, counts.total, 
counts.total_area_ha,
counts.cas_id_count, counts.cas_id_count*100/total AS cas_id_per,
counts.cas_id_area_ha,
counts.wetland_type_count, counts.wetland_type_count*100/total AS wetland_type_per,
counts.wetland_type_area_ha,
counts.wet_veg_cover_count, counts.wet_veg_cover_count*100/total AS wet_veg_cover_per,
counts.wet_veg_cover_area_ha,
counts.wet_landform_mod_count, counts.wet_landform_mod_count*100/total AS wet_landform_mod_per,
counts.wet_landform_mod_area_ha,
counts.wet_local_mod_count, counts.wet_local_mod_count*100/total AS wet_local_mod_per,
counts.wet_local_mod_area_ha,
counts.eco_site_count, counts.eco_site_count*100/total AS eco_site_per,
counts.eco_site_area_ha,
counts.layer_count, counts.layer_count*100/total AS layer_per,
counts.layer_area_ha
FROM counts;

COMMENT ON MATERIALIZED VIEW non_error_eco_all_attribute_counts IS
'non_error_eco_all_attribute_counts provides the count of each non-error attribute in eco_all and the percentage of attributes that are not errors';


-- Record count & percent of each attribute (non-error) by Inventory for geo_all
CREATE MATERIALIZED VIEW IF NOT EXISTS non_error_geo_all_attribute_counts AS
SELECT LEFT(cas_id, 4) AS inventory,
count(*) AS total,
avg(ST_area(geometry)) AS mean_area_m2,
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ST_area(geometry)) AS median_area_m2,
mode() WITHIN GROUP (ORDER BY ST_area(geometry)) AS mode_area_m2,
avg(ST_NPoints(geometry)) AS mean_vertex_count,
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ST_NPoints(geometry)) AS median_vertex_count,
mode() WITHIN GROUP (ORDER BY ST_NPoints(geometry)) AS mode_vertex_count,
avg(ST_Perimeter(geometry)) AS mean_perimeter_m2,
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ST_Perimeter(geometry)) AS median_perimeter_m2,
mode() WITHIN GROUP (ORDER BY ST_Perimeter(geometry)) AS mode_perimeter_m2,
avg(4*pi()*ST_area(geometry)/power(ST_Perimeter(geometry), 2)) AS mean_thinness_ratio,
PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY (4*pi()*ST_area(geometry)/power(ST_Perimeter(geometry), 2))) AS median_thinness_ratio,
mode() WITHIN GROUP (ORDER BY (4*pi()*ST_area(geometry)/power(ST_Perimeter(geometry), 2))) AS mode_thinness_ratio
FROM geo_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW non_error_geo_all_attribute_counts IS
'non_error_geo_all_attribute_counts provides the count of geometries as well as mean, median, and mode for the area, vertex count, and thinness ratio/sliver index';


-- Record count & percent of each attribute (non-error) by Inventory for lyr_all
CREATE MATERIALIZED VIEW IF NOT EXISTS non_error_lyr_all_attribute_counts AS
WITH counts AS (
	SELECT LEFT(l.cas_id, 4) AS inventory,
	count(*) AS total,
	sum  (c.casfri_area) AS total_area_ha,
	--uses cases within counts since counts can only count non values. so if a cell is an error, the count will regard it as null
	count(CASE isError(l.cas_id) WHEN FALSE THEN l.cas_id else NULL END) AS cas_id_count, 
	sum  (CASE isError(l.cas_id) WHEN FALSE THEN c.casfri_area else NULL END) AS cas_id_area_ha,
	count(CASE isError(l.soil_moist_reg) WHEN FALSE THEN l.soil_moist_reg else NULL END) AS soil_moist_reg_count,  
	sum  (CASE isError(l.soil_moist_reg) WHEN FALSE THEN c.casfri_area else NULL END) AS soil_moist_reg_area_ha,
	count(CASE isError(l.structure_per::text) WHEN FALSE THEN l.structure_per else NULL END) AS structure_per_count,  
	sum  (CASE isError(l.structure_per::text) WHEN FALSE THEN c.casfri_area else NULL END) AS structure_per_area_ha,
	count(CASE isError(l.structure_range::text) WHEN FALSE THEN l.structure_range else NULL END) AS structure_range_count, 
	sum  (CASE isError(l.structure_range::text) WHEN FALSE THEN c.casfri_area else NULL END) AS structure_range_area_ha,
	count(CASE isError(l.layer::text) WHEN FALSE THEN l.layer else NULL END) AS layer_count, 
	sum  (CASE isError(l.layer::text) WHEN FALSE THEN c.casfri_area else NULL END) AS layer_area_ha, 
	count(CASE isError(l.layer_rank::text) WHEN FALSE THEN l.layer_rank else NULL END) AS layer_rank_count, 
	sum  (CASE isError(l.layer_rank::text) WHEN FALSE THEN c.casfri_area else NULL END) AS layer_rank_area_ha,
	count(CASE isError(l.crown_closure_upper::text) WHEN FALSE THEN l.crown_closure_upper else NULL END) crown_closure_upper_count, 
	sum  (CASE isError(l.crown_closure_upper::text) WHEN FALSE THEN c.casfri_area else NULL END) AS crown_closure_upper_area_ha,
	count(CASE isError(l.crown_closure_lower::text) WHEN FALSE THEN l.crown_closure_lower else NULL END) AS crown_closure_lower_count, 
	sum  (CASE isError(l.crown_closure_lower::text) WHEN FALSE THEN c.casfri_area else NULL END) AS crown_closure_lower_area_ha,
	count(CASE isError(l.height_upper::text) WHEN FALSE THEN l.height_upper else NULL END) AS height_upper_count, 
	sum  (CASE isError(l.height_upper::text) WHEN FALSE THEN c.casfri_area else NULL END) AS height_upper_area_ha,
	count(CASE isError(l.height_lower::text) WHEN FALSE THEN l.height_lower else NULL END) AS height_lower_count, 
	sum  (CASE isError(l.height_lower::text) WHEN FALSE THEN c.casfri_area else NULL END) AS height_lower_area_ha,
	count(CASE isError(l.productivity) WHEN FALSE THEN l.productivity else NULL END) AS productivity_count, 
	sum  (CASE isError(l.productivity) WHEN FALSE THEN c.casfri_area else NULL END) AS productivity_area_ha,
	count(CASE isError(l.productivity_type) WHEN FALSE THEN l.productivity_type else NULL END) AS productivity_type_count, 
	sum  (CASE isError(l.productivity_type) WHEN FALSE THEN c.casfri_area else NULL END) AS productivity_type_area_ha,
	count(CASE isError(l.species_1) WHEN FALSE THEN l.species_1 else NULL END) AS species_1_count, 
	sum  (CASE isError(l.species_1) WHEN FALSE THEN c.casfri_area else NULL END) AS species_1_area_ha,
	count(CASE isError(l.species_per_1::text) WHEN FALSE THEN l.species_per_1 else NULL END) AS species_per_1_count, 
	sum  (CASE isError(l.species_per_1::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_1_area_ha,
	count(CASE isError(l.species_2) WHEN FALSE THEN l.species_2 else NULL END) AS species_2_count, 
	sum  (CASE isError(l.species_2) WHEN FALSE THEN c.casfri_area else NULL END) AS species_2_area_ha,
	count(CASE isError(l.species_per_2::text) WHEN FALSE THEN l.species_per_2 else NULL END) AS species_per_2_count, 
	sum  (CASE isError(l.species_per_2::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_2_area_ha,
	count(CASE isError(l.species_3) WHEN FALSE THEN l.species_3 else NULL END) AS species_3_count, 
	sum  (CASE isError(l.species_3) WHEN FALSE THEN c.casfri_area else NULL END) AS species_3_area_ha,
	count(CASE isError(l.species_per_3::text) WHEN FALSE THEN l.species_per_3 else NULL END) AS species_per_3_count, 
	sum  (CASE isError(l.species_per_3::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_3_area_ha,
	count(CASE isError(l.species_4) WHEN FALSE THEN l.species_4 else NULL END) AS species_4_count, 
	sum  (CASE isError(l.species_4) WHEN FALSE THEN c.casfri_area else NULL END) AS species_4_area_ha,
	count(CASE isError(l.species_per_4::text) WHEN FALSE THEN l.species_per_4 else NULL END) AS species_per_4_count, 
	sum  (CASE isError(l.species_per_4::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_4_area_ha,
	count(CASE isError(l.species_5) WHEN FALSE THEN l.species_5 else NULL END) AS species_5_count, 
	sum  (CASE isError(l.species_5) WHEN FALSE THEN c.casfri_area else NULL END) AS species_5_area_ha,
	count(CASE isError(l.species_per_5::text) WHEN FALSE THEN l.species_per_5 else NULL END) AS species_per_5_count, 
	sum  (CASE isError(l.species_per_5::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_5_area_ha,
	count(CASE isError(l.species_6) WHEN FALSE THEN l.species_6 else NULL END) AS species_6_count, 
	sum  (CASE isError(l.species_6) WHEN FALSE THEN c.casfri_area else NULL END) AS species_6_area_ha,
	count(CASE isError(l.species_per_6::text) WHEN FALSE THEN l.species_per_6 else NULL END) AS species_per_6_count, 
	sum  (CASE isError(l.species_per_6::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_6_area_ha,
	count(CASE isError(l.species_7) WHEN FALSE THEN l.species_7 else NULL END) AS species_7_count, 
	sum  (CASE isError(l.species_7) WHEN FALSE THEN c.casfri_area else NULL END) AS species_7_area_ha,
	count(CASE isError(l.species_per_7::text) WHEN FALSE THEN l.species_per_7 else NULL END) AS species_per_7_count, 
	sum  (CASE isError(l.species_per_7::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_7_area_ha,
	count(CASE isError(l.species_8) WHEN FALSE THEN l.species_8 else NULL END) AS species_8_count, 
	sum  (CASE isError(l.species_8) WHEN FALSE THEN c.casfri_area else NULL END) AS species_8_area_ha,
	count(CASE isError(l.species_per_8::text) WHEN FALSE THEN l.species_per_8 else NULL END) AS species_per_8_count, 
	sum  (CASE isError(l.species_per_8::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_8_area_ha,
	count(CASE isError(l.species_9) WHEN FALSE THEN l.species_9 else NULL END) AS species_9_count, 
	sum  (CASE isError(l.species_9) WHEN FALSE THEN c.casfri_area else NULL END) AS species_9_area_ha,
	count(CASE isError(l.species_per_9::text) WHEN FALSE THEN l.species_per_9 else NULL END) AS species_per_9_count, 
	sum  (CASE isError(l.species_per_9::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_9_area_ha,
	count(CASE isError(l.species_10) WHEN FALSE THEN l.species_10 else NULL END) AS species_10_count, 
	sum  (CASE isError(l.species_10) WHEN FALSE THEN c.casfri_area else NULL END) AS species_10_area_ha,
	count(CASE isError(l.species_per_10::text) WHEN FALSE THEN l.species_per_10 else NULL END) AS species_per_10_count, 
	sum  (CASE isError(l.species_per_10::text) WHEN FALSE THEN c.casfri_area else NULL END) AS species_per_10_area_ha,
	count(CASE isError(l.origin_upper::text) WHEN FALSE THEN l.origin_upper else NULL END) AS origin_upper_count, 
	sum  (CASE isError(l.origin_upper::text) WHEN FALSE THEN c.casfri_area else NULL END) AS origin_upper_area_ha,
	count(CASE isError(l.origin_lower::text) WHEN FALSE THEN l.origin_lower else NULL END) AS origin_lower_count, 
	sum  (CASE isError(l.origin_lower::text) WHEN FALSE THEN c.casfri_area else NULL END) AS origin_lower_area_ha,
	count(CASE isError(l.site_class) WHEN FALSE THEN l.site_class else NULL END) AS site_class_count, 
	sum  (CASE isError(l.site_class) WHEN FALSE THEN c.casfri_area else NULL END) AS site_class_area_ha,
	count(CASE isError(l.site_index::text) WHEN FALSE THEN l.site_index else NULL END) AS site_index_count,
	sum  (CASE isError(l.site_index::text) WHEN FALSE THEN c.casfri_area else NULL END) AS site_index_area_ha
	FROM lyr_all l
	INNER JOIN cas_all c ON c.cas_id = l.cas_id
	GROUP BY inventory
)
SELECT counts.inventory, counts.total, 
counts.total_area_ha,
counts.cas_id_count, counts.cas_id_count*100/total AS cas_id_per,
counts.cas_id_area_ha,
counts.soil_moist_reg_count, counts.soil_moist_reg_count*100/total AS soil_moist_reg_per,
counts.soil_moist_reg_area_ha,
counts.structure_per_count, counts.structure_per_count*100/total AS structure_per_per,
counts.structure_per_area_ha,
counts.structure_range_count, counts.structure_range_count*100/total AS structure_range_per,
counts.structure_range_area_ha,
counts.layer_count, counts.layer_count*100/total AS layer_per,
counts.layer_area_ha,
counts.layer_rank_count, counts.layer_rank_count*100/total AS layer_rank_per,
counts.layer_rank_area_ha,
counts.crown_closure_upper_count, counts.crown_closure_upper_count*100/total AS crown_closure_upper_per,
counts.crown_closure_upper_area_ha,
counts.crown_closure_lower_count, counts.crown_closure_lower_count*100/total AS crown_closure_lower_per,
counts.crown_closure_lower_area_ha,
counts.height_upper_count, counts.height_upper_count*100/total AS height_upper_per,
counts.height_upper_area_ha,
counts.height_lower_count, counts.height_lower_count*100/total AS height_lower_per,
counts.height_lower_area_ha,
counts.productivity_count, counts.productivity_count*100/total AS productivity_per,
counts.productivity_area_ha,
counts.productivity_type_count, counts.productivity_type_count*100/total AS productivity_type_per,
counts.productivity_type_area_ha,
counts.species_1_count, counts.species_1_count*100/total AS species_1_per,
counts.species_1_area_ha,
counts.species_per_1_count, counts.species_per_1_count*100/total AS species_per_1_per,
counts.species_per_1_area_ha,
counts.species_2_count, counts.species_2_count*100/total AS species_2_per,
counts.species_2_area_ha,
counts.species_per_2_count, counts.species_per_2_count*100/total AS species_per_2_per,
counts.species_per_2_area_ha,
counts.species_3_count, counts.species_3_count*100/total AS species_3_per,
counts.species_3_area_ha,
counts.species_per_3_count, counts.species_per_3_count*100/total AS species_per_3_per,
counts.species_per_3_area_ha,
counts.species_4_count, counts.species_4_count*100/total AS species_4_per,
counts.species_4_area_ha,
counts.species_per_4_count, counts.species_per_4_count*100/total AS species_per_4_per,
counts.species_per_4_area_ha,
counts.species_5_count, counts.species_5_count*100/total AS species_5_per,
counts.species_5_area_ha,
counts.species_per_5_count, counts.species_per_5_count*100/total AS species_per_5_per,
counts.species_per_5_area_ha,
counts.species_6_count, counts.species_6_count*100/total AS species_6_per,
counts.species_6_area_ha,
counts.species_per_6_count, counts.species_per_6_count*100/total AS species_per_6_per,
counts.species_per_6_area_ha,
counts.species_7_count, counts.species_7_count*100/total AS species_7_per,
counts.species_7_area_ha,
counts.species_per_7_count, counts.species_per_7_count*100/total AS species_per_7_per,
counts.species_per_7_area_ha,
counts.species_8_count, counts.species_8_count*100/total AS species_8_per,
counts.species_8_area_ha,
counts.species_per_8_count, counts.species_per_8_count*100/total AS species_per_8_per,
counts.species_per_8_area_ha,
counts.species_9_count, counts.species_9_count*100/total AS species_9_per,
counts.species_9_area_ha,
counts.species_per_9_count, counts.species_per_9_count*100/total AS species_per_9_per,
counts.species_per_9_area_ha,
counts.species_10_count, counts.species_10_count*100/total AS species_10_per,
counts.species_10_area_ha,
counts.species_per_10_count, counts.species_per_10_count*100/total AS species_per_10_per,
counts.species_per_10_area_ha,
counts.origin_upper_count, counts.origin_upper_count*100/total AS _per,
counts.origin_upper_area_ha,
counts.origin_lower_count, counts.origin_lower_count*100/total AS origin_lower_per,
counts.origin_lower_area_ha,
counts.site_class_count, counts.site_class_count*100/total AS site_class_per,
counts.site_class_area_ha,
counts.site_index_count, counts.site_index_count*100/total AS site_index_per,
counts.site_index_area_ha
FROM counts;

COMMENT ON MATERIALIZED VIEW non_error_lyr_all_attribute_counts IS
'non_error_lyr_all_attribute_counts provides the count of each non-error attribute in lyr_all and the percentage of attributes that are not errors';


-- Record count & percent of each attribute (non-error) by Inventory for nfl_all
CREATE MATERIALIZED VIEW IF NOT EXISTS non_error_nfl_all_attribute_counts AS
WITH counts AS (
	SELECT LEFT(n.cas_id, 4) AS inventory,
	count(*) AS total,
	sum  (casfri_area) AS total_area_ha,
	--uses cases within counts since counts can only count non values. so if a cell is an error, the count will regard it as null
	count(CASE isError(n.cas_id) WHEN FALSE THEN n.cas_id else NULL END) AS cas_id_count, 
	sum  (CASE isError(n.cas_id) WHEN FALSE THEN c.casfri_area else NULL END) AS cas_id_area_ha,
	count(CASE isError(n.soil_moist_reg) WHEN FALSE THEN n.soil_moist_reg else NULL END) AS soil_moist_reg_count, 
	sum  (CASE isError(n.soil_moist_reg) WHEN FALSE THEN c.casfri_area else NULL END) AS soil_moist_reg_area_ha,
	count(CASE isError(n.structure_per::text) WHEN FALSE THEN n.structure_per else NULL END) AS structure_per_count, 
	sum  (CASE isError(n.structure_per::text) WHEN FALSE THEN c.casfri_area else NULL END) AS structure_per_area_ha,
	count(CASE isError(n.layer::text) WHEN FALSE THEN n.layer else NULL END) AS layer_count, 
	sum  (CASE isError(n.layer::text) WHEN FALSE THEN c.casfri_area else NULL END) AS layer_area_ha,
	count(CASE isError(n.layer_rank::text) WHEN FALSE THEN n.layer_rank else NULL END) AS layer_rank_count,
	sum  (CASE isError(n.layer_rank::text) WHEN FALSE THEN c.casfri_area else NULL END) AS layer_rank_area_ha,
	count(CASE isError(n.crown_closure_upper::text) WHEN FALSE THEN n.crown_closure_upper else NULL END) crown_closure_upper_count,
	sum  (CASE isError(n.crown_closure_upper::text) WHEN FALSE THEN c.casfri_area else NULL END) AS crown_closure_upper_area_ha,
	count(CASE isError(n.crown_closure_lower::text) WHEN FALSE THEN n.crown_closure_lower else NULL END) AS crown_closure_lower_count,
	sum  (CASE isError(n.crown_closure_lower::text) WHEN FALSE THEN c.casfri_area else NULL END) AS crown_closure_lower_area_ha,
	count(CASE isError(n.height_upper::text) WHEN FALSE THEN n.height_upper else NULL END) AS height_upper_count,
	sum  (CASE isError(n.height_upper::text) WHEN FALSE THEN c.casfri_area else NULL END) AS height_upper_area_ha,
	count(CASE isError(n.height_lower::text) WHEN FALSE THEN n.height_lower else NULL END) AS height_lower_count,
	sum  (CASE isError(n.height_lower::text) WHEN FALSE THEN c.casfri_area else NULL END) AS height_lower_area_ha,
	count(CASE isError(n.nat_non_veg) WHEN FALSE THEN n.nat_non_veg else NULL END) AS nat_non_veg_count,
	sum  (CASE isError(n.nat_non_veg) WHEN FALSE THEN c.casfri_area else NULL END) AS nat_non_veg_area_ha,
	count(CASE isError(n.non_for_anth) WHEN FALSE THEN n.non_for_anth else NULL END) AS non_for_anth_count,
	sum  (CASE isError(n.non_for_anth) WHEN FALSE THEN c.casfri_area else NULL END) AS non_for_anth_area_ha,
	count(CASE isError(n.non_for_veg) WHEN FALSE THEN n.non_for_veg else NULL END) AS non_for_veg_count,
	sum  (CASE isError(n.non_for_veg) WHEN FALSE THEN c.casfri_area else NULL END) AS non_for_veg_area_ha
	FROM nfl_all n
	INNER JOIN cas_all c ON c.cas_id = n.cas_id
	GROUP BY inventory
)
SELECT counts.inventory, counts.total, 
counts.total_area_ha,
counts.cas_id_count, counts.cas_id_count*100/total AS cas_id_per,
counts.cas_id_area_ha,
counts.soil_moist_reg_count, counts.soil_moist_reg_count*100/total AS soil_moist_reg_per,
counts.soil_moist_reg_area_ha,
counts.structure_per_count, counts.structure_per_count*100/total AS structure_per_per,
counts.structure_per_area_ha,
counts.layer_count, counts.layer_count*100/total AS layer_per,
counts.layer_area_ha,
counts.layer_rank_count, counts.layer_rank_count*100/total AS layer_rank_per,
counts.layer_rank_area_ha,
counts.crown_closure_upper_count, counts.crown_closure_upper_count*100/total AS crown_closure_upper_per,
counts.crown_closure_upper_area_ha,
counts.crown_closure_lower_count, counts.crown_closure_lower_count*100/total AS crown_closure_lower_per,
counts.crown_closure_lower_area_ha,
counts.height_upper_count, counts.height_upper_count*100/total AS height_upper_per,
counts.height_upper_area_ha,
counts.height_lower_count, counts.height_lower_count*100/total AS height_lower_per,
counts.height_lower_area_ha,
counts.nat_non_veg_count, counts.nat_non_veg_count*100/total AS nat_non_veg_per,
counts.nat_non_veg_area_ha,
counts.non_for_anth_count, counts.non_for_anth_count*100/total AS non_for_anth_per,
counts.non_for_anth_area_ha,
counts.non_for_veg_count, counts.non_for_veg_count*100/total AS non_for_veg_per,
counts.non_for_veg_area_ha
FROM counts;

COMMENT ON MATERIALIZED VIEW non_error_nfl_all_attribute_counts IS
'non_error_nfl_all_attribute_counts provides the count of each non-error attribute in nfl_all and the percentage of attributes that are not errors';


-- Feature count and sum of areas in each table per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.features_per_inv AS
SELECT c.inventory, 
c.total AS cas_all_count, c.total_area_ha AS cas_all_area_ha,
d.total AS dst_all_count, d.total_area_ha AS dst_all_area_ha,
e.total AS eco_all_count, e.total_area_ha AS eco_all_area_ha,
l.total AS lyr_all_count, l.total_area_ha AS lyr_all_area_ha,
n.total AS nfl_all_count, n.total_area_ha AS nfl_all_area_ha
FROM non_error_cas_all_attribute_counts c
FULL JOIN non_error_dst_all_attribute_counts d ON c.inventory = d.inventory
FULL JOIN non_error_eco_all_attribute_counts e ON c.inventory = e.inventory
FULL JOIN non_error_lyr_all_attribute_counts l ON c.inventory = l.inventory
FULL JOIN non_error_nfl_all_attribute_counts n ON c.inventory = n.inventory
ORDER BY inventory;

COMMENT ON MATERIALIZED VIEW casfri_metrics.features_per_inv IS
'features_per_inv provides counts for features and area sums in each main casfri table per inventory';


-- Count and percentage of each error code by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS error_code_by_inv AS
WITH string_table AS (
	--Converts rows of tables into a string (excluding cas_id and orig_stand_id since they shouldn't have any error codes
	SELECT cas_id, string_agg(
	', ' || inventory_id || ', ' || stand_structure || ', ' || 
	num_of_layers || ', ' ||  map_sheet_id || ', ' || 
	casfri_area || ', ' ||  casfri_perimeter || ', ' 
	|| src_inv_area || ', ' ||  stand_photo_year || ', ', '') AS string
	FROM cas_all
	GROUP BY cas_id
	UNION ALL 
	SELECT cas_id, string_agg(
	', ' || dist_type_1 || ', ' || dist_year_1 || ', ' || 
	dist_ext_upper_1 || ', ' || dist_ext_lower_1 || ', ' ||  
	dist_type_2 || ', ' || dist_year_2 || ', ' ||  
	dist_ext_upper_2 || ', ' || dist_ext_lower_2 || ', ' ||  
	dist_type_3 || ', ' || dist_year_3 || ', ' ||  
	dist_ext_upper_3 || ', ' || dist_ext_lower_3 || ', ' ||
	layer , '') AS string
	FROM dst_all
	GROUP BY cas_id
	union all 
	SELECT cas_id, string_agg(
	wetland_type || ', ' || wet_veg_cover || ', ' || 
	wet_landform_mod || ', ' || wet_local_mod || ', ' ||  
	eco_site || ', ' || layer, '') AS string
	FROM eco_all
	GROUP BY cas_id
	union all 
	SELECT cas_id, string_agg(
	soil_moist_reg || ', ' || structure_per || ', ' || 
	structure_range || ', ' || layer || ', ' ||  
	layer_rank || ', ' || crown_closure_upper || ', ' || 
	crown_closure_lower || ', ' || height_upper || ', ' || 
	height_lower || ', ' || productivity || ', ' || 
	productivity_type || ', ' || species_1 || ', ' || 
	species_per_1 || ', ' || species_2 || ', ' || 
	species_per_2 || ', ' || species_3 || ', ' || 
	species_per_3 || ', ' || species_4 || ', ' || 
	species_per_4 || ', ' || species_5 || ', ' || 
	species_per_5 || ', ' || species_6 || ', ' || 
	species_per_6 || ', ' || species_7 || ', ' || 
	species_per_7 || ', ' || species_8 || ', ' || 
	species_per_8 || ', ' || species_9 || ', ' || 
	species_per_9 || ', ' || species_10 || ', ' || 
	species_per_10 || ', ' || origin_upper || ', ' || 
	origin_lower || ', ' || site_class || ', ' || 
	site_index, '') AS string
	FROM lyr_all
	GROUP BY cas_id
	union all 
	SELECT cas_id, string_agg(
	soil_moist_reg || ', ' || structure_per || ', ' || 
	layer || ', ' || layer_rank || ', ' ||  
	crown_closure_upper || ', ' ||  crown_closure_lower || ', ' || 
	height_upper || ', ' || height_lower || ', ' ||
	nat_non_veg, '') AS string
	FROM nfl_all
	GROUP BY cas_id
), errors_table AS (
	--Gets counts of all error codes by inventory
	SELECT LEFT(cas_id, 4) AS inventory, 
	sum(countInstances(string, ', EMPTY_STRING,')) AS EMPTY_STRING,
	sum(countInstances(string, ', NULL_VALUE,') + countInstances(string, ', -8888,')) AS NULL_VALUE,
	sum(countInstances(string, ', NOT_APPLICABLE,') + countInstances(string, ', -8887,')) AS NOT_APPLICABLE,
	sum(countInstances(string, ', UNKNOWN_VALUE,') + countInstances(string, ', -8886,')) AS UNKNOWN_VALUE,
	sum(countInstances(string, ', OUT_OF_RANGE,') + countInstances(string, ', -9999,'))AS OUT_OF_RANGE,
	sum(countInstances(string, ', NOT_IN_SET,') + countInstances(string, ', -9998,')) AS NOT_IN_SET,
	sum(countInstances(string, ', INVALID_VALUE,') + countInstances(string, ', -9997,'))AS INVALID_VALUE,
	sum(countInstances(string, ', WRONG_TYPE,') + countInstances(string, ', -9995,')) AS WRONG_TYPE,
	sum(countInstances(string, ', INVALID_GEOMETRY,') + countInstances(string, ', -7779,')) AS INVALID_GEOMETRY,
	sum(countInstances(string, ', NO_INTERSECT,') + countInstances(string, ', -7778,')) AS NO_INTERSECT,
	sum(countInstances(string, ', TRANSLATION_ERROR,') + countInstances(string, ', -3333,')) AS TRANSLATION_ERROR
	FROM string_table
	GROUP BY inventory
), totals AS (
	--gets a total count of all errors per inventory
	SELECT e.inventory, 
	e.EMPTY_STRING + e.NULL_VALUE + e.NOT_APPLICABLE + e.UNKNOWN_VALUE + e.OUT_OF_RANGE + e.NOT_IN_SET + e.INVALID_VALUE + e.WRONG_TYPE + e.INVALID_GEOMETRY + e.NO_INTERSECT + e.TRANSLATION_ERROR 
	AS total
	FROM errors_table e
)
-- converts counts to percentages
SELECT e.inventory, t.total AS total_errors,
e.EMPTY_STRING, e.EMPTY_STRING::float*100.0/t.total::float AS EMPTY_STRING_PER,
e.NULL_VALUE, e.NULL_VALUE::float*100.0/t.total::float AS NULL_VALUE_PER,
e.NOT_APPLICABLE, e.NOT_APPLICABLE::float*100.0/t.total::float AS NOT_APPLICABLE_PER,
e.UNKNOWN_VALUE, e.UNKNOWN_VALUE::float*100.0/t.total::float AS UNKNOWN_VALUE_PER,
e.OUT_OF_RANGE, e.OUT_OF_RANGE::float*100.0/t.total::float AS OUT_OF_RANGE_PER,
e.NOT_IN_SET, e.NOT_IN_SET::float*100.0/t.total::float AS NOT_IN_SET_PER,
e.INVALID_VALUE, e.INVALID_VALUE::float*100.0/t.total::float AS INVALID_VALUE_PER,
e.WRONG_TYPE, e.WRONG_TYPE::float*100.0/t.total::float AS WRONG_TYPE_PER,
e.INVALID_GEOMETRY, e.INVALID_GEOMETRY::float*100.0/t.total::float AS INVALID_GEOMETRY_PER,
e.NO_INTERSECT, e.NO_INTERSECT::float*100.0/t.total::float AS NO_INTERSECT_PER,
e.TRANSLATION_ERROR, e.TRANSLATION_ERROR::float*100.0/t.total::float AS TRANSLATION_ERROR_PER
FROM errors_table e
INNER JOIN totals t ON t.inventory = e.inventory;
	
	
COMMENT ON MATERIALIZED VIEW error_code_by_inv IS
'error_code_by_inv provides counts of each error code throughout all CASFRI tables per inventory, as well as the percentage that error composes of all errors in the inventory';

