# To run, navigate to the /harmonization/workflow/casfri_metrics/grafana/pdf-export directory
# Then run 'bash jurisdiction_disturbance_reports_script.sh'

GRAF_TUNNEL_PORT=8000
GRAF_RUN_HEADLESS=1
GRAF_KEY=glsa_7JVGe8SqjJXJoOPxl91dCk8SLJTns7z3_394e3c94
GRAF_IP=localhost:$GRAF_TUNNEL_PORT
GRAF_TEMPLATES=latex_templates
GRAF_TEMPLATE=jurisdiction_disturbance_template
DATE="$(date +'%m-%d-%Y')"
GRAF_OUTPUT_DIR=jurisdiction_disturbance_reports_$DATE
OUTPUT_SUFFIX=$DATE.pdf
# You can specify variable values in the time span command argument since this is just passed into the URL
TIME_SPAN="from=now-135y&to=now&var-jurisdiction="
DASH="fdsnl2reru9s0a"
FILE_NAME="dists"

declare -A juris

juris[AB]="AB"
juris[BC]="BC"
juris[MB]="MB"
juris[NB]="NB"
juris[NS]="NS"
juris[NT]="NT"
juris[ON]="ON"
juris[PE]="PE"
juris[QC]="QC"
juris[YT]="YT"


mkdir $GRAF_OUTPUT_DIR

for key in "${!juris[@]}"; do

  output_file=${key}_${FILE_NAME}_${OUTPUT_SUFFIX}

  grafana-reporter \
    -cmd_enable=$GRAF_RUN_HEADLESS \
    -cmd_apiKey $GRAF_KEY \
    -cmd_ts ${TIME_SPAN}${key} \
    -ip  $GRAF_IP\
    -cmd_dashboard $DASH \
    -cmd_o $GRAF_OUTPUT_DIR/$output_file \
    -grid-layout=1 \
    -cmd_template $GRAF_TEMPLATE \
    -templates $GRAF_TEMPLATES \
    2>&1 | tee logs/${key}_${FILE_NAME}_${DATE}.txt
done
