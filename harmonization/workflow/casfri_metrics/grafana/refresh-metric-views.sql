SET search_path = casfri_metrics, casfri50, casfri_web, casfri_post_processing, translation, public;

-- In order of when they should be run
-- Everything in a numbered section is parrallel friendly with one another
------------------------
-- 1
------------------------
-- attribute-counts-metrics
REFRESH MATERIALIZED VIEW  non_error_cas_all_attribute_counts;
REFRESH MATERIALIZED VIEW  non_error_dst_all_attribute_counts;
REFRESH MATERIALIZED VIEW  non_error_eco_all_attribute_counts;
REFRESH MATERIALIZED VIEW  non_error_geo_all_attribute_counts;
REFRESH MATERIALIZED VIEW  non_error_lyr_all_attribute_counts;
REFRESH MATERIALIZED VIEW  non_error_nfl_all_attribute_counts;
REFRESH MATERIALIZED VIEW  error_code_by_inv;

-- all-inventories-metrics
REFRESH MATERIALIZED VIEW  number_of_inv;
REFRESH MATERIALIZED VIEW  stand_heights_by_inventory;
REFRESH MATERIALIZED VIEW  stand_origin_by_inventory;
REFRESH MATERIALIZED VIEW  range_of_years_per_inv;
REFRESH MATERIALIZED VIEW  dst_area_ranking_by_inv;
REFRESH MATERIALIZED VIEW  forest_layer_area_by_inv;
REFRESH MATERIALIZED VIEW  dst_type_area_by_jurisdiction_year;
REFRESH MATERIALIZED VIEW  all_species_disturbed_area_per_jurisdiction;
REFRESH MATERIALIZED VIEW  change_in_nat_non_veg_types;
REFRESH MATERIALIZED VIEW  change_in_non_for_anth_types;
REFRESH MATERIALIZED VIEW  change_in_non_for_veg_types;
REFRESH MATERIALIZED VIEW  all_species_area_counts_per_inv;
REFRESH MATERIALIZED VIEW  species_area_by_stand_origin_year_per_jurisdiction;
REFRESH MATERIALIZED VIEW  stand_change_in_leading_genus;
REFRESH MATERIALIZED VIEW  genus_converted_to_nfl;
REFRESH MATERIALIZED VIEW  disturbance_coords_per_jurisdiction;
REFRESH MATERIALIZED VIEW  soil_moist_reg_per_inventory;
REFRESH MATERIALIZED VIEW  soil_moist_reg_per_species;
REFRESH MATERIALIZED VIEW  forest_productivity_per_inventory;
REFRESH MATERIALIZED VIEW  forest_productivity_per_species;
REFRESH MATERIALIZED VIEW  forest_productivity_type_per_inventory;
REFRESH MATERIALIZED VIEW  forest_productivity_type_per_species;
REFRESH MATERIALIZED VIEW  eco_wetland_type_per_inventory;
REFRESH MATERIALIZED VIEW  eco_wet_veg_cover_per_inventory;
REFRESH MATERIALIZED VIEW  eco_wet_landform_mod_per_inventory;
REFRESH MATERIALIZED VIEW  eco_wet_local_mod_per_inventory;
REFRESH MATERIALIZED VIEW  treed_polygon_sizes_overall;
REFRESH MATERIALIZED VIEW  treed_polygon_sizes_per_jurisdiction;
REFRESH MATERIALIZED VIEW  range_of_origin_upper_per_inv;
REFRESH MATERIALIZED VIEW  range_of_origin_lower_per_inv;
REFRESH MATERIALIZED VIEW  range_of_height_upper_per_inv;
REFRESH MATERIALIZED VIEW  range_of_height_lower_per_inv;
REFRESH MATERIALIZED VIEW  range_of_crown_closure_upper_per_inv;
REFRESH MATERIALIZED VIEW  range_of_crown_closure_lower_per_inv;
REFRESH MATERIALIZED VIEW  forest_layer_stand_ages;

-- latest-cut-metrics
REFRESH MATERIALIZED VIEW  features_per_inv_latest;
REFRESH MATERIALIZED VIEW  stand_heights_by_inventory_latest;
REFRESH MATERIALIZED VIEW  stand_origin_by_inventory_latest;
REFRESH MATERIALIZED VIEW  range_of_years_per_inv_latest;
REFRESH MATERIALIZED VIEW  all_species_area_counts_per_inv_latest;
REFRESH MATERIALIZED VIEW  all_species_1_to_10_area_count_per_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  species_area_by_stand_origin_year_per_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  forest_layer_area_by_inv_latest;
REFRESH MATERIALIZED VIEW  dst_type_area_by_jurisdiction_year_latest;
REFRESH MATERIALIZED VIEW  all_species_disturbed_area_per_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  dst_area_ranking_by_inv_latest;
REFRESH MATERIALIZED VIEW  disturbance_coords_per_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  soil_moist_reg_per_species_per_inventory_latest;
REFRESH MATERIALIZED VIEW  forest_productivity_per_species_per_inventory_latest;
REFRESH MATERIALIZED VIEW  forest_productivity_type_per_species_per_inventory_latest;
REFRESH MATERIALIZED VIEW  eco_wetland_type_per_inventory_latest;
REFRESH MATERIALIZED VIEW  eco_wet_veg_cover_per_inventory_latest;
REFRESH MATERIALIZED VIEW  eco_wet_landform_mod_per_inventory_latest;
REFRESH MATERIALIZED VIEW  eco_wet_local_mod_per_inventory_latest;
REFRESH MATERIALIZED VIEW  treed_polygon_sizes_overall_latest;
REFRESH MATERIALIZED VIEW  treed_polygon_sizes_per_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  forest_layer_stand_ages_latest;

-- 1990-cut-metrics
REFRESH MATERIALIZED VIEW  features_per_inv_1990;
REFRESH MATERIALIZED VIEW  stand_heights_by_inventory_1990;
REFRESH MATERIALIZED VIEW  stand_origin_by_inventory_1990;
REFRESH MATERIALIZED VIEW  range_of_years_per_inv_1990;
REFRESH MATERIALIZED VIEW  all_species_area_counts_per_inv_1990;
REFRESH MATERIALIZED VIEW  all_species_1_to_10_area_count_per_jurisdiction_1990;
REFRESH MATERIALIZED VIEW  species_area_by_stand_origin_year_per_jurisdiction_1990;
REFRESH MATERIALIZED VIEW  forest_layer_area_by_inv_1990;
REFRESH MATERIALIZED VIEW  dst_type_area_by_jurisdiction_year_1990;
REFRESH MATERIALIZED VIEW  all_species_disturbed_area_per_jurisdiction_1990;
REFRESH MATERIALIZED VIEW  dst_area_ranking_by_inv_1990;
REFRESH MATERIALIZED VIEW  disturbance_coords_per_jurisdiction_1990;
REFRESH MATERIALIZED VIEW  soil_moist_reg_per_species_per_inventory_1990;
REFRESH MATERIALIZED VIEW  forest_productivity_per_species_per_inventory_1990;
REFRESH MATERIALIZED VIEW  forest_productivity_type_per_species_per_inventory_1990;

------------------------
-- 2 (Dependent on something in 1)
------------------------
-- attribute-counts-metrics
REFRESH MATERIALIZED VIEW  features_per_inv;

-- all-inventories-metrics
REFRESH MATERIALIZED VIEW  stand_heights_by_jurisdiction;
REFRESH MATERIALIZED VIEW  stand_origin_by_jurisdiction;
REFRESH MATERIALIZED VIEW  range_of_years_per_jurisdiction;
REFRESH MATERIALIZED VIEW  most_common_dst_by_inv_top_5;
REFRESH MATERIALIZED VIEW  dst_type_area_by_year;
REFRESH MATERIALIZED VIEW  all_species_disturbed_area;
REFRESH MATERIALIZED VIEW  all_species_area_counts_per_jurisdiction;
REFRESH MATERIALIZED VIEW  all_species_area_counts;
REFRESH MATERIALIZED VIEW  all_species_1_to_10_area_count_per_jurisdiction;
REFRESH MATERIALIZED VIEW  species_area_by_stand_origin_year;
REFRESH MATERIALIZED VIEW  disturbance_coords;
REFRESH MATERIALIZED VIEW  stand_photo_year_histogram;
REFRESH MATERIALIZED VIEW  origin_upper_histogram;
REFRESH MATERIALIZED VIEW  origin_lower_histogram;
REFRESH MATERIALIZED VIEW  height_upper_histogram;
REFRESH MATERIALIZED VIEW  height_lower_histogram;
REFRESH MATERIALIZED VIEW  crown_closure_upper_histogram;
REFRESH MATERIALIZED VIEW  crown_closure_lower_histogram;
REFRESH MATERIALIZED VIEW  range_of_stand_age_per_inv;
REFRESH MATERIALIZED VIEW  stand_age_histogram;

-- latest-cut-metrics
REFRESH MATERIALIZED VIEW  stand_heights_by_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  stand_origin_by_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  range_of_years_per_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  all_species_area_counts_per_jurisdiction_latest;
REFRESH MATERIALIZED VIEW  all_species_area_counts_latest;
REFRESH MATERIALIZED VIEW  all_species_1_to_10_area_count_latest;
REFRESH MATERIALIZED VIEW  species_area_by_stand_origin_year_latest;
REFRESH MATERIALIZED VIEW  dst_type_area_by_year_latest;
REFRESH MATERIALIZED VIEW  all_species_disturbed_area_latest;
REFRESH MATERIALIZED VIEW  disturbance_coords_latest;
REFRESH MATERIALIZED VIEW  range_of_stand_age_per_inv_latest;

-- 1990-cut-metrics
REFRESH MATERIALIZED VIEW  stand_heights_by_jurisdiction_1990;
REFRESH MATERIALIZED VIEW  stand_origin_by_jurisdiction_1990;
REFRESH MATERIALIZED VIEW  range_of_years_per_jurisdiction_1990;
REFRESH MATERIALIZED VIEW  all_species_area_counts_1990;
REFRESH MATERIALIZED VIEW  all_species_1_to_10_area_count_1990;
REFRESH MATERIALIZED VIEW  species_area_by_stand_origin_year_1990;
REFRESH MATERIALIZED VIEW  dst_type_area_by_year_1990;
REFRESH MATERIALIZED VIEW  all_species_disturbed_area_1990;
REFRESH MATERIALIZED VIEW  disturbance_coords_1990;

------------------------
-- 3 (Dependent on something in 2)
------------------------
-- all-inventories-metrics
REFRESH MATERIALIZED VIEW  species_most_often_disturbed;
REFRESH MATERIALIZED VIEW  all_species_1_to_10_area_count;

-- latest-cut-metrics
REFRESH MATERIALIZED VIEW  species_most_often_disturbed_latest;
REFRESH MATERIALIZED VIEW  stand_age_histogram_latest;

-- 1990-cut-metrics
REFRESH MATERIALIZED VIEW  species_most_often_disturbed_1990;


















