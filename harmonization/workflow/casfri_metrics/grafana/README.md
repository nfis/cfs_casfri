# CFS-CASFRI Grafana Info

### PDF Exports

PDF export configuration info can be found in grafana/pdf-export/config-info.txt

The PDF report scripts are best for most uses. You can also create dashboard links to create reports in your browser. Go to Dashboard settings > Links
 
The link will be /api/plugins/mahendrapaipuri-dashboardreporter-app/resources/report?dashUid=DASHBOARD_ID&layout=grid

The dashboard ID is the alphanumeric code in the URL.
 
PDF export panels must be formatted slightly differently. Panels should be fairly large, and they cannot have a transparent background. Also, factor in text size since the panels come out a lot smaller in the reports than they are viewed in Grafana.

PDF export dashboards are stored in a seperate folder. When you add a new panel to a generic dashboard, you should also add an equivalent panel to the export dashboard since this won't be added automatically.

### Variables

Variables are used fairly heavily in the dashboards. Most commonly, they are used to set jurisdictions and inventories, but may also be used for species codes, or for more complex logic in validation inspection.

Grafana variables can be passed into a URL using &var-VAR_NAME=EX, This trick is how the pdf reports are created so that the dashboard can be used to create various reports for each jurisdiction.

Variables can be created and controlled in a dashboard by going to Dashboard settings > Variables.

They can be accessed in a panel using: 
```
${VAR_NAME}
```
If the variable has multiple values, use: 
```
${VAR_NAME:sqlstring}
```
and this is generally best used for use cases where you have a query with something like: 
```
WHERE ATTRIBUTE IN (${VAR_NAME:sqlstring})
```

### New panels

Adding a new panel in Grafana should be fairly easy.

1. Ensure you have an existing table or materialized view with the data you need. We want the panel to load quickly, so most of the aggregation and parsing of large datasets should already be done.
2. Ensure that grafana has select permissions on the table 
```
GRANT SELECT ON TABLE schema.table TO grafana;
```
3. Click Add > Visualization to access the panel editor. Under the query tab, switch from "Builder" to "Code", since it is generally easier to write the queries there.
4. Write a query to access the data you need. Take note to select only the exact columns you need, and make sure to specify the schema of the table you are selecting from since the search path likely isnt setup right.
5. Follow examples from existing panels to get ideas on how to represent it with visuals.


### Refreshing Metrics

Metric views need to be refreshed when the database changes. Most can be run in parrallel, but some are dependendent on one another.

Use refresh-metric-views.sql to refresh existing views. They are setup in order of priority so that as many views as possible can be run in parrallel.

Add any new materialized views to refresh-metric-views.sql. They can be added under category 1, unless they depend on another view, then it must be placed in the category after the dependency appears

Metric helper functions do not need to be recreated unless they are changed.

Run metric-table-conversions.sql as a script to refresh metric tables for attribute counts.
Run rawfri-metrics.sql as a script to refresh raw metric tables.






  
