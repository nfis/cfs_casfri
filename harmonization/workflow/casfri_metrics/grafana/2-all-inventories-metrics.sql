

-- CASFRI Inventory count
CREATE MATERIALIZED VIEW IF NOT EXISTS number_of_inv AS
SELECT count(distinct inventory_id) AS num_inv
FROM hdr_all;

COMMENT ON MATERIALIZED VIEW number_of_inv IS
'number_of_inv provides a count of all inventories in the cas_all table';


-- Range of stand heights by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_heights_by_inventory AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(height_lower::text) = false THEN height_lower ELSE NULL end) AS min_stand_height, 
max(CASE WHEN isError(height_upper::text) = false THEN height_upper ELSE NULL end) AS max_stand_height
FROM lyr_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW stand_heights_by_inventory IS
'stand_heights_by_inventory provides the range of recorded stand heights for each inventory';


-- Range of stand heights by jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_heights_by_jurisdiction AS
SELECT left(inventory, 2) AS jurisdiction, 
min(min_stand_height) AS min_stand_height, 
max(max_stand_height) AS max_stand_height
FROM stand_heights_by_inventory
GROUP BY jurisdiction;

COMMENT ON MATERIALIZED VIEW stand_heights_by_jurisdiction IS
'stand_heights_by_jurisdiction provides the range of recorded stand heights for each jurisdiction';


-- Range of stand origins by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_origin_by_inventory AS
SELECT LEFT(cas_id, 4) AS inventory, 
min(origin_lower) AS min_stand_origin, 
max(origin_upper) AS max_stand_origin
FROM lyr_all
WHERE isError(origin_lower::text) = false AND isError(origin_upper::text) = false
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW stand_origin_by_inventory IS
'stand_origin_by_inventory provides the range of recorded stand origin years for each inventory';


-- Range of stand origins by jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_origin_by_jurisdiction AS
SELECT LEFT(inventory, 2) AS jurisdiction, 
min(min_stand_origin) AS min_stand_origin, 
max(max_stand_origin) AS max_stand_origin
FROM stand_origin_by_inventory
GROUP BY jurisdiction;

COMMENT ON MATERIALIZED VIEW stand_origin_by_jurisdiction IS
'stand_origin_by_jurisdiction provides the range of recorded stand origin years for each jurisdiction';


-- Range of stand photo years by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_years_per_inv AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(stand_photo_year::text) = false THEN stand_photo_year ELSE NULL end) AS min_stand_photo_year, 
max(CASE WHEN isError(stand_photo_year::text) = false THEN stand_photo_year ELSE NULL end) AS max_stand_photo_year
FROM cas_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_years_per_inv IS
'range_of_years_per_inv provides the range of recorded stand photo years for each inventory';


-- Range of stand photo years by jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_years_per_jurisdiction AS
SELECT left(inventory, 2) AS jurisdiction, 
min(min_stand_photo_year) AS min_stand_photo_year, 
max(max_stand_photo_year) AS max_stand_photo_year
FROM range_of_years_per_inv
GROUP BY jurisdiction;

COMMENT ON MATERIALIZED VIEW range_of_years_per_jurisdiction IS
'range_of_years_per_jurisdiction provides the range of recorded stand photo years for each jurisdiction';


-- Disturbance type area and feature count ranked per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS dst_area_ranking_by_inv AS
WITH dist_types AS (
	SELECT LEFT(dst_all.cas_id, 4) AS inventory, dist_type_1 AS dist_type, sum(cas_all.casfri_area*getDistExt(dist_ext_upper_1, dist_ext_lower_1)/100) AS area_ha, 
	count(dist_type_1) AS feature_count
	FROM dst_all
	INNER JOIN cas_all ON cas_all.cas_id = dst_all.cas_id
	where isError(dist_type_1) = false
	GROUP BY inventory, dist_type_1
	-- Union each disturbance type and its areas and feature counts
	UNION ALL 
	SELECT LEFT(dst_all.cas_id, 4) AS inventory, dist_type_2 AS dist_type, sum(cas_all.casfri_area*getDistExt(dist_ext_upper_2, dist_ext_lower_2)/100) AS area_ha, 
	count(dist_type_2) AS feature_count
	FROM dst_all
	INNER JOIN cas_all ON cas_all.cas_id = dst_all.cas_id
	where isError(dist_type_2) = false
	GROUP BY inventory, dist_type_2
	UNION ALL 
	SELECT LEFT(dst_all.cas_id, 4) AS inventory, dist_type_3 AS dist_type, sum(cas_all.casfri_area*getDistExt(dist_ext_upper_3, dist_ext_lower_3)/100) AS area_ha, 
	count(dist_type_3) AS feature_count
	FROM dst_all
	INNER JOIN cas_all ON cas_all.cas_id = dst_all.cas_id
	where isError(dist_type_3) = false
	GROUP BY inventory, dist_type_3
)
--Sum up individual disturbance type sums and assign ranking based on feature count
SELECT inventory, dist_type, sum(area_ha) AS area_ha, sum(feature_count) AS feature_count,
rank() over (
	partition BY inventory
	ORDER BY sum(feature_count) DESC
	)
FROM dist_types
GROUP BY inventory, dist_type
ORDER BY inventory, RANK;


COMMENT ON MATERIALIZED VIEW dst_area_ranking_by_inv IS
'dst_area_ranking_by_inv provides the total area and feature count of all disturbances, per inventory';


-- Area and feature count of top 5 disturbance types per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS most_common_dst_by_inv_top_5 AS
SELECT *
FROM dst_area_ranking_by_inv
WHERE RANK <= 5
ORDER BY inventory, RANK;

COMMENT ON MATERIALIZED VIEW most_common_dst_by_inv_top_5 IS
'most_common_dst_by_inv_top_5 provides the total area and feature count of the top 5 most common disturbances, per inventory';


-- Forest layer area by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_layer_area_by_inv AS
SELECT LEFT(cas_all.cas_id, 4) AS inventory, 
sum(CASE WHEN layer = 1 THEN cas_all.casfri_area ELSE 0 end) AS layer_1_area_ha,
sum(CASE WHEN layer = 2 THEN cas_all.casfri_area ELSE 0 end) AS layer_2_area_ha,
sum(CASE WHEN layer = 3 THEN cas_all.casfri_area ELSE 0 end) AS layer_3_area_ha,
sum(CASE WHEN layer = 4 THEN cas_all.casfri_area ELSE 0 end) AS layer_4_area_ha
FROM lyr_all
INNER JOIN cas_all ON cas_all.cas_id = lyr_all.cas_id 
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW forest_layer_area_by_inv IS
'forest_layer_area_by_inv provides the sum of all forested area in each inventory, for each layer';


-- Disturbance type area by year per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS dst_type_area_by_jurisdiction_year AS
SELECT dist_type_filter.year, dist_type_filter.dist_type, dist_type_filter.jurisdiction, sum(dist_type_filter.area_ha) AS area_ha 
FROM(
	SELECT dist_year_1 AS year, dist_type_1 AS dist_type, LEFT(dst_all.cas_id, 2) AS jurisdiction, 
	sum(cas_all.casfri_area*getDistExt(dist_ext_upper_1, dist_ext_lower_1)/100) AS area_ha, 
		rank() over (
		partition by dist_type_1
		ORDER BY dist_type_1
		)
	FROM dst_all
	INNER JOIN cas_all ON cas_all.cas_id = dst_all.cas_id
	WHERE isError(dist_type_1) = false
	AND isError(dist_year_1::text) = false
	GROUP BY year, dist_type_1, LEFT(dst_all.cas_id, 2)
	-- Stack each of dist_1, dist_2, dist_3 then sum the areas and regroup
	UNION ALL 
	SELECT dist_year_2 AS year, dist_type_2 AS dist_type, LEFT(dst_all.cas_id, 2) AS jurisdiction, 
	sum(cas_all.casfri_area*getDistExt(dist_ext_upper_2, dist_ext_lower_2)/100) AS area_ha, 
		rank() over (
		partition by dist_type_2
		ORDER BY dist_type_2
		)
	FROM dst_all
	INNER JOIN cas_all ON cas_all.cas_id = dst_all.cas_id
	where isError(dist_type_2) = false
	AND isError(dist_year_2::text) = false
	GROUP BY year, dist_type_2, LEFT(dst_all.cas_id, 2)
	UNION ALL 
	SELECT dist_year_3 AS year, dist_type_3 AS dist_type, LEFT(dst_all.cas_id, 2) AS jurisdiction, 
	sum(cas_all.casfri_area*getDistExt(dist_ext_upper_3, dist_ext_lower_3)/100) AS area_ha, 
		rank() over (
		partition by dist_type_3
		ORDER BY dist_type_3
		)
	FROM dst_all
	INNER JOIN cas_all ON cas_all.cas_id = dst_all.cas_id
	WHERE isError(dist_type_3) = false
	AND isError(dist_year_3::text) = false
	GROUP BY year, dist_type_3, LEFT(dst_all.cas_id, 2)
) dist_type_filter
GROUP BY year, dist_type, jurisdiction
ORDER BY year ASC, jurisdiction;	


COMMENT ON MATERIALIZED VIEW dst_type_area_by_jurisdiction_year IS
'dst_type_area_by_jurisdiction_year provides the sum of areas affected by each disturbance type for each jurisdiction, in each year';


-- Disturbance type area by year
CREATE MATERIALIZED VIEW IF NOT EXISTS dst_type_area_by_year AS
SELECT YEAR, dist_type, sum(area_ha) AS area_ha
FROM dst_type_area_by_jurisdiction_year
GROUP BY year, dist_type
ORDER BY year ASC, sum(area_ha) DESC;		

COMMENT ON MATERIALIZED VIEW dst_type_area_by_year IS
'dst_type_area_by_year provides the sum of areas affected by each disturbance type for each year';


-- Area of each disturbance type per species, per jurisdicition
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_disturbed_area_per_jurisdiction AS
WITH all_species AS (
	SELECT cas_id, species_1 AS species, species_per_1 AS species_per
	FROM lyr_all
	WHERE isError(species_1) = FALSE AND isError(species_per_1::text) = FALSE
	UNION ALL
	SELECT cas_id, species_2 AS species, species_per_2 AS species_per
	FROM lyr_all
	WHERE isError(species_2) = FALSE AND isError(species_per_2::text) = FALSE
	UNION ALL
	SELECT cas_id, species_3 AS species, species_per_3 AS species_per
	FROM lyr_all
	WHERE isError(species_3) = FALSE AND isError(species_per_3::text) = FALSE
	UNION ALL
	SELECT cas_id, species_4 AS species, species_per_4 AS species_per
	FROM lyr_all
	WHERE isError(species_4) = FALSE AND isError(species_per_4::text) = FALSE
	UNION ALL
	SELECT cas_id, species_5 AS species, species_per_5 AS species_per
	FROM lyr_all
	WHERE isError(species_5) = FALSE AND isError(species_per_5::text) = FALSE
	UNION ALL
	SELECT cas_id, species_6 AS species, species_per_6 AS species_per
	FROM lyr_all
	WHERE isError(species_6) = FALSE AND isError(species_per_6::text) = FALSE
	UNION ALL
	SELECT cas_id, species_7 AS species, species_per_7 AS species_per
	FROM lyr_all
	WHERE isError(species_7) = FALSE AND isError(species_per_7::text) = FALSE
	UNION ALL
	SELECT cas_id, species_8 AS species, species_per_8 AS species_per
	FROM lyr_all
	WHERE isError(species_8) = FALSE AND isError(species_per_8::text) = FALSE
	UNION ALL
	SELECT cas_id, species_9 AS species, species_per_9 AS species_per
	FROM lyr_all
	WHERE isError(species_9) = FALSE AND isError(species_per_9::text) = FALSE
	UNION ALL
	SELECT cas_id, species_10 AS species, species_per_10 AS species_per
	FROM lyr_all
	WHERE isError(species_10) = FALSE AND isError(species_per_10::text) = FALSE
), all_dist_types AS (
	SELECT d.cas_id, d.dist_type_1 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_1, dist_ext_lower_1)/100 AS casfri_area
	FROM dst_all d
	INNER JOIN cas_all c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_1) = FALSE
	UNION ALL
	SELECT d.cas_id, d.dist_type_2 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_2, dist_ext_lower_2)/100 AS casfri_area
	FROM dst_all d
	INNER JOIN cas_all c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_2) = FALSE
	UNION ALL
	SELECT d.cas_id, d.dist_type_3 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_3, dist_ext_lower_3)/100 AS casfri_area
	FROM dst_all d
	INNER JOIN cas_all c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_3) = FALSE
)
SELECT LEFT(d.cas_id, 2) AS jurisdiction, d.dist_type, 
s.species,
sum(d.casfri_area*s.species_per/100) AS area_ha,
count(d.dist_type) AS feature_count,
RANK () OVER (
	PARTITION BY LEFT(d.cas_id, 2), d.dist_type
	ORDER BY count(d.dist_type) desc
)
FROM all_dist_types d
INNER JOIN all_species s ON d.cas_id = s.cas_id
GROUP BY jurisdiction, d.dist_type, s.species
ORDER BY jurisdiction, feature_count desc;

COMMENT ON MATERIALIZED VIEW all_species_disturbed_area_per_jurisdiction IS
'all_species_disturbed_area_per_jurisdiction provides the feature and area count of each species disturbed, per jurisdiction';


-- Area of each disturbance type per species
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_disturbed_area AS
WITH all_species AS (
	SELECT dist_type, species, sum(area_ha) AS area_ha, sum(feature_count) AS feature_count
	FROM all_species_disturbed_area_per_jurisdiction 
	GROUP BY dist_type, species
)
SELECT dist_type, species, area_ha, feature_count,
RANK () OVER (
	PARTITION BY dist_type
	ORDER BY feature_count desc
)
FROM all_species 
ORDER BY feature_count desc;

COMMENT ON MATERIALIZED VIEW all_species_disturbed_area IS
'all_species_disturbed_area provides the feature and area count of each species disturbed, across all inventories';


-- Species most often disturbed per disturbance type
CREATE MATERIALIZED VIEW IF NOT EXISTS casfri_metrics.species_most_often_disturbed AS
SELECT dist_type, species, area_ha, feature_count
FROM all_species_disturbed_area
WHERE RANK = 1
ORDER BY feature_count desc;

COMMENT ON MATERIALIZED VIEW casfri_metrics.species_most_often_disturbed IS
'species_most_often_disturbed provides the top species disturbed per disturbance type, with the feature and area count, across all inventories';


-- Change in area of NFL nat_non_veg types between consecutive inventories
CREATE MATERIALIZED VIEW IF NOT EXISTS change_in_nat_non_veg_types AS
WITH sums AS (
	SELECT LEFT(n.cas_id, 4) AS inventory,
	SUM(CASE n.nat_non_veg when 'ALPINE' THEN c.casfri_area ELSE 0 END) AS ALPINE_area_ha,
	SUM(CASE n.nat_non_veg when 'EXPOSED_LAND' THEN c.casfri_area ELSE 0 END) AS EXPOSED_LAND_area_ha,
	SUM(CASE n.nat_non_veg when 'FLOOD' THEN c.casfri_area ELSE 0 END) AS FLOOD_area_ha,
	SUM(CASE n.nat_non_veg when 'ISLAND' THEN c.casfri_area ELSE 0 END) AS ISLAND_area_ha,
	SUM(CASE n.nat_non_veg when 'LAKE' THEN c.casfri_area ELSE 0 END) AS LAKE_area_ha,
	SUM(CASE n.nat_non_veg when 'OCEAN' THEN c.casfri_area ELSE 0 END) AS OCEAN_area_ha,
	SUM(CASE n.nat_non_veg when 'RIVER' THEN c.casfri_area ELSE 0 END) AS RIVER_area_ha,
	SUM(CASE n.nat_non_veg when 'ROCK_RUBBLE' THEN c.casfri_area ELSE 0 END) AS ROCK_RUBBLE_area_ha,
	SUM(CASE n.nat_non_veg when 'SAND' THEN c.casfri_area ELSE 0 END) AS SAND_area_ha,
	SUM(CASE n.nat_non_veg when 'SLIDE' THEN c.casfri_area ELSE 0 END) AS SLIDE_area_ha,
	SUM(CASE n.nat_non_veg when 'SNOW_ICE' THEN c.casfri_area ELSE 0 END) AS SNOW_ICE_area_ha,
	SUM(CASE n.nat_non_veg when 'TIDAL_FLATS' THEN c.casfri_area ELSE 0 END) AS TIDAL_FLATS_area_ha,
	SUM(CASE n.nat_non_veg when 'WATER_SEDIMENT' THEN c.casfri_area ELSE 0 END) AS WATER_SEDIMENT_area_ha,
	SUM(CASE n.nat_non_veg when 'WATERBODY' THEN c.casfri_area ELSE 0 END) AS WATERBODY_area_ha,
	SUM(CASE n.nat_non_veg when 'OTHER' THEN c.casfri_area ELSE 0 END) AS OTHER_area_ha,
	SUM(CASE when (n.nat_non_veg = 'NULL_VALUE') OR (n.nat_non_veg = 'EMPTY_STRING') OR (n.nat_non_veg = 'UNKNOWN_VALUE') OR (n.nat_non_veg = 'NOT_IN_SET') OR (non_for_anth = 'INVALID_VALUE') OR (non_for_anth = 'NOT_APPLICABLE') THEN c.casfri_area ELSE 0 END) AS ERROR_area_ha
	FROM nfl_all n
	INNER JOIN cas_all c ON n.cas_id = c.cas_id
	GROUP BY inventory
), totals AS (
	SELECT s.inventory, s.ALPINE_area_ha + s.EXPOSED_LAND_area_ha  + s.FLOOD_area_ha  + s.ISLAND_area_ha  + s.LAKE_area_ha  + s.OCEAN_area_ha  + s.RIVER_area_ha  + s.ROCK_RUBBLE_area_ha  + s.SAND_area_ha  + s.SLIDE_area_ha  + s.SNOW_ICE_area_ha  + s.TIDAL_FLATS_area_ha  + s.WATER_SEDIMENT_area_ha  + s.WATERBODY_area_ha  + s.OTHER_area_ha  + s.ERROR_area_ha  AS total
	FROM sums s
)
SELECT s.inventory, 
t.total AS total_nfl_area_ha, 
-- This case statement will set change to null if the row is the first row in a jurisdiction
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
t.total - lag(t.total) OVER (ORDER BY s.inventory) END) AS total_nfl_change,
s.ALPINE_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.ALPINE_area_ha - lag(s.ALPINE_area_ha) OVER (ORDER BY s.inventory) END) AS ALPINE_change,
s.EXPOSED_LAND_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.EXPOSED_LAND_area_ha - lag(s.EXPOSED_LAND_area_ha) OVER (ORDER BY s.inventory) END) AS EXPOSED_LAND_change,
s.FLOOD_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.FLOOD_area_ha - lag(s.FLOOD_area_ha) OVER (ORDER BY s.inventory) END) AS FLOOD_change,
s.ISLAND_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.ISLAND_area_ha - lag(s.ISLAND_area_ha) OVER (ORDER BY s.inventory) END) AS ISLAND_change,
s.LAKE_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.LAKE_area_ha - lag(s.LAKE_area_ha) OVER (ORDER BY s.inventory) END) AS LAKE_change,
s.OCEAN_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.OCEAN_area_ha - lag(s.OCEAN_area_ha) OVER (ORDER BY s.inventory) END) AS OCEAN_change,
s.RIVER_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.RIVER_area_ha - lag(s.RIVER_area_ha) OVER (ORDER BY s.inventory) END) AS RIVER_change,
s.ROCK_RUBBLE_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.ROCK_RUBBLE_area_ha - lag(s.ROCK_RUBBLE_area_ha) OVER (ORDER BY s.inventory) END) AS ROCK_RUBBLE_change,
s.SAND_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.SAND_area_ha - lag(s.SAND_area_ha) OVER (ORDER BY s.inventory) END) AS SAND_change,
s.SLIDE_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.SLIDE_area_ha - lag(s.SLIDE_area_ha) OVER (ORDER BY s.inventory) END) AS SLIDE_change,
s.SNOW_ICE_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.SNOW_ICE_area_ha - lag(s.SNOW_ICE_area_ha) OVER (ORDER BY s.inventory) END) AS SNOW_ICE_change,
s.TIDAL_FLATS_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.TIDAL_FLATS_area_ha - lag(s.TIDAL_FLATS_area_ha) OVER (ORDER BY s.inventory) END) AS TIDAL_FLATS_change,
s.WATER_SEDIMENT_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.WATER_SEDIMENT_area_ha - lag(s.WATER_SEDIMENT_area_ha) OVER (ORDER BY s.inventory) END) AS WATER_SEDIMENT_change,
s.WATERBODY_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.WATERBODY_area_ha - lag(s.WATERBODY_area_ha) OVER (ORDER BY s.inventory) END) AS WATERBODY_change,
s.OTHER_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.OTHER_area_ha - lag(s.OTHER_area_ha) OVER (ORDER BY s.inventory) END) AS OTHER_change,
s.ERROR_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.ERROR_area_ha - lag(s.ERROR_area_ha) OVER (ORDER BY s.inventory) END) AS ERROR_change
FROM sums s
INNER JOIN totals t ON t.inventory = s.inventory
ORDER BY s.inventory ASC;

COMMENT ON MATERIALIZED VIEW change_in_nat_non_veg_types IS
'change_in_nat_non_veg_types provides the area of nat_non_veg types and the change between consecutive inventories';


-- Change in area of NFL non_for_anth types between consecutive inventories
CREATE MATERIALIZED VIEW IF NOT EXISTS change_in_non_for_anth_types AS
WITH sums AS (
	SELECT left(n.cas_id, 4) AS inventory,
	SUM(CASE n.non_for_anth when 'BORROW_PIT' THEN c.casfri_area ELSE 0 END) AS BORROW_PIT_area_ha,
	SUM(CASE n.non_for_anth when 'CULTIVATED' THEN c.casfri_area ELSE 0 END) AS CULTIVATED_area_ha,
	SUM(CASE n.non_for_anth when 'FACILITY_INFRASTRUCTURE' THEN c.casfri_area ELSE 0 END) AS FACILITY_INFRASTRUCTURE_area_ha,
	SUM(CASE n.non_for_anth when 'INDUSTRIAL' THEN c.casfri_area ELSE 0 END) AS INDUSTRIAL_area_ha,
	SUM(CASE n.non_for_anth when 'LAGOON' THEN c.casfri_area ELSE 0 END) AS LAGOON_area_ha,
	SUM(CASE n.non_for_anth when 'SETTLEMENT' THEN c.casfri_area ELSE 0 END) AS SETTLEMENT_area_ha,
	SUM(CASE n.non_for_anth when 'OTHER' THEN c.casfri_area ELSE 0 END) AS OTHER_area_ha,
	SUM(CASE when (n.non_for_anth = 'NULL_VALUE') OR (n.non_for_anth = 'EMPTY_STRING') OR (n.non_for_anth = 'UNKNOWN_VALUE') OR (n.non_for_anth = 'NOT_IN_SET') OR (n.non_for_anth = 'INVALID_VALUE') OR (n.non_for_anth = 'NOT_APPLICABLE') THEN c.casfri_area ELSE 0 END) AS ERROR_area_ha
	FROM nfl_all n
	INNER JOIN cas_all c ON n.cas_id = c.cas_id
	GROUP BY inventory
), totals AS (
	SELECT s.inventory, s.BORROW_PIT_area_ha + s.CULTIVATED_area_ha  + s.FACILITY_INFRASTRUCTURE_area_ha  + s.INDUSTRIAL_area_ha  + s.LAGOON_area_ha  + s.SETTLEMENT_area_ha + s.OTHER_area_ha  + s.ERROR_area_ha AS total
	FROM sums s
)
SELECT s.inventory, 
t.total AS total_nfl_area_ha, 
-- This case statement will set change to null if the row is the first row in a jurisdiction
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
t.total - lag(t.total) OVER (ORDER BY s.inventory) END) AS total_nfl_change,
s.BORROW_PIT_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.BORROW_PIT_area_ha - lag(s.BORROW_PIT_area_ha) OVER (ORDER BY s.inventory) END) AS BORROW_PIT_change,
s.CULTIVATED_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.CULTIVATED_area_ha - lag(s.CULTIVATED_area_ha) OVER (ORDER BY s.inventory) END) AS CULTIVATED_change,
s.FACILITY_INFRASTRUCTURE_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.FACILITY_INFRASTRUCTURE_area_ha - lag(s.FACILITY_INFRASTRUCTURE_area_ha) OVER (ORDER BY s.inventory) END) AS FACILITY_INFRASTRUCTURE_change,
s.INDUSTRIAL_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.INDUSTRIAL_area_ha - lag(s.INDUSTRIAL_area_ha) OVER (ORDER BY s.inventory) END) AS INDUSTRIAL_change,
s.LAGOON_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.LAGOON_area_ha - lag(s.LAGOON_area_ha) OVER (ORDER BY s.inventory) END) AS LAGOON_change,
s.SETTLEMENT_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.SETTLEMENT_area_ha - lag(s.SETTLEMENT_area_ha) OVER (ORDER BY s.inventory) END) AS SETTLEMENT_change,
s.OTHER_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.OTHER_area_ha - lag(s.OTHER_area_ha) OVER (ORDER BY s.inventory) END) AS OTHER_change,
s.ERROR_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.ERROR_area_ha - lag(s.ERROR_area_ha) OVER (ORDER BY s.inventory) END) AS ERROR_change
FROM sums s
INNER JOIN totals t ON t.inventory = s.inventory
ORDER BY s.inventory ASC;

COMMENT ON MATERIALIZED VIEW change_in_non_for_anth_types IS
'change_in_non_for_anth_types provides the area of non_for_anth types and the change between consecutive inventories';


-- Change in area of NFL non_for_veg types between consecutive inventories
CREATE MATERIALIZED VIEW IF NOT EXISTS change_in_non_for_veg_types AS
WITH sums AS (
	SELECT left(n.cas_id, 4) AS inventory,
	SUM(CASE n.non_for_veg when 'ALPINE_FOREST' THEN c.casfri_area ELSE 0 END) AS ALPINE_FOREST_area_ha,
	SUM(CASE n.non_for_veg when 'BRYOID' THEN c.casfri_area ELSE 0 END) AS BRYOID_area_ha,
	SUM(CASE n.non_for_veg when 'FORBS' THEN c.casfri_area ELSE 0 END) AS FORBS_area_ha,
	SUM(CASE n.non_for_veg when 'GRAMINOIDS' THEN c.casfri_area ELSE 0 END) AS GRAMINOIDS_area_ha,
	SUM(CASE n.non_for_veg when 'HERBS' THEN c.casfri_area ELSE 0 END) AS HERBS_area_ha,
	SUM(CASE n.non_for_veg when 'LOW_SHRUB' THEN c.casfri_area ELSE 0 END) AS LOW_SHRUB_area_ha,
	SUM(CASE n.non_for_veg when 'OPEN_MUSKEG' THEN c.casfri_area ELSE 0 END) AS OPEN_MUSKEG_area_ha,
	SUM(CASE n.non_for_veg when 'TALL_SHRUB' THEN c.casfri_area ELSE 0 END) AS TALL_SHRUB_area_ha,
	SUM(CASE n.non_for_veg when 'TUNDRA' THEN c.casfri_area ELSE 0 END) AS TUNDRA_area_ha,
	SUM(CASE n.non_for_veg when 'OTHER' THEN c.casfri_area ELSE 0 END) AS OTHER_area_ha,
	SUM(CASE WHEN (n.non_for_veg = 'NULL_VALUE') OR (n.non_for_veg = 'EMPTY_STRING') OR (n.non_for_veg = 'UNKNOWN_VALUE') OR (n.non_for_veg = 'NOT_IN_SET') OR (n.non_for_veg = 'INVALID_VALUE') OR (n.non_for_veg = 'NOT_APPLICABLE') THEN c.casfri_area ELSE 0 END) AS ERROR_area_ha
	FROM nfl_all n
	INNER JOIN cas_all c ON n.cas_id = c.cas_id
	GROUP BY inventory
), totals AS (
	SELECT s.inventory, s.ALPINE_FOREST_area_ha + s.BRYOID_area_ha  + s.FORBS_area_ha  + s.GRAMINOIDS_area_ha  + s.HERBS_area_ha  + s.LOW_SHRUB_area_ha  + s.OPEN_MUSKEG_area_ha  + s.TALL_SHRUB_area_ha + s.TUNDRA_area_ha + s.OTHER_area_ha  + s.ERROR_area_ha  AS total
	FROM sums s
)
SELECT s.inventory, 
t.total AS total_nfl_area_ha, 
-- This case statement will set change to null if the row is the first row in a jurisdiction
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
t.total - lag(t.total) OVER (ORDER BY s.inventory) END) AS total_nfl_change,
s.ALPINE_FOREST_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.ALPINE_FOREST_area_ha - lag(s.ALPINE_FOREST_area_ha) OVER (ORDER BY s.inventory) END) AS ALPINE_FOREST_change,
s.BRYOID_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.BRYOID_area_ha - lag(s.BRYOID_area_ha) OVER (ORDER BY s.inventory) END) AS BRYOID_change,
s.FORBS_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.FORBS_area_ha - lag(s.FORBS_area_ha) OVER (ORDER BY s.inventory) END) AS FORBS_change,
s.GRAMINOIDS_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.GRAMINOIDS_area_ha - lag(s.GRAMINOIDS_area_ha) OVER (ORDER BY s.inventory) END) AS GRAMINOIDS_change,
s.HERBS_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.HERBS_area_ha - lag(s.HERBS_area_ha) OVER (ORDER BY s.inventory) END) AS HERBS_change,
s.LOW_SHRUB_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.LOW_SHRUB_area_ha - lag(s.LOW_SHRUB_area_ha) OVER (ORDER BY s.inventory) END) AS LOW_SHRUB_change,
s.OPEN_MUSKEG_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.OPEN_MUSKEG_area_ha - lag(s.OPEN_MUSKEG_area_ha) OVER (ORDER BY s.inventory) END) AS OPEN_MUSKEG_change,
s.TALL_SHRUB_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.TALL_SHRUB_area_ha - lag(s.TALL_SHRUB_area_ha) OVER (ORDER BY s.inventory) END) AS TALL_SHRUB_change,
s.TUNDRA_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.TUNDRA_area_ha - lag(s.TUNDRA_area_ha) OVER (ORDER BY s.inventory) END) AS TUNDRA_change,
s.OTHER_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.OTHER_area_ha - lag(s.OTHER_area_ha) OVER (ORDER BY s.inventory) END) AS OTHER_change,
s.ERROR_area_ha, 
(CASE WHEN LEFT(lag(s.inventory) OVER (ORDER BY s.inventory), 2) != left(s.inventory, 2) THEN NULL ELSE 
s.ERROR_area_ha - lag(s.ERROR_area_ha) OVER (ORDER BY s.inventory) END) AS ERROR_change
FROM sums s
INNER JOIN totals t ON t.inventory = s.inventory
ORDER BY s.inventory ASC;

COMMENT ON MATERIALIZED VIEW change_in_non_for_veg_types IS
'change_in_non_for_veg_types provides the area of non_for_veg types and the change between consecutive inventories';


-- Species area and count per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_area_counts_per_inv AS
WITH species_table AS (
	-- Filter table beforehand to optimize
	SELECT LEFT(l.cas_id, 4) AS inventory, species_1, species_per_1, species_2, species_per_2, species_3, species_per_3, species_4, species_per_4, species_5, species_per_5,
		   species_6, species_per_6, species_7, species_per_7, species_8, species_per_8, species_9, species_per_9, species_10, species_per_10, c.casfri_area AS area
	FROM lyr_all l
	INNER JOIN cas_all c ON l.cas_id = c.cas_id
), all_species AS (
	SELECT inventory, species_1 AS species, count(*) AS feature_count, sum(area*species_per_1/100) AS area_ha
	FROM species_table
	WHERE isError(species_1) = FALSE AND isError(species_per_1::text) = FALSE
	GROUP BY inventory, species_1
	UNION ALL
	SELECT inventory, species_2 AS species, count(*) AS feature_count, sum(area*species_per_2/100) AS area_ha
	FROM species_table
	WHERE isError(species_2) = FALSE AND isError(species_per_2::text) = FALSE
	GROUP BY inventory, species_2
	UNION ALL
	SELECT inventory, species_3 AS species, count(*) AS feature_count, sum(area*species_per_3/100) AS area_ha
	FROM species_table
	WHERE isError(species_3) = FALSE AND isError(species_per_3::text) = FALSE
	GROUP BY inventory, species_3
	UNION ALL
	SELECT inventory, species_4 AS species, count(*) AS feature_count, sum(area*species_per_4/100) AS area_ha
	FROM species_table
	WHERE isError(species_4) = FALSE AND isError(species_per_4::text) = FALSE
	GROUP BY inventory, species_4
	UNION ALL
	SELECT inventory, species_5 AS species, count(*) AS feature_count, sum(area*species_per_5/100) AS area_ha
	FROM species_table
	WHERE isError(species_5) = FALSE AND isError(species_per_5::text) = FALSE
	GROUP BY inventory, species_5
	UNION ALL
	SELECT inventory, species_6 AS species, count(*) AS feature_count, sum(area*species_per_6/100) AS area_ha
	FROM species_table
	WHERE isError(species_6) = FALSE AND isError(species_per_6::text) = FALSE
	GROUP BY inventory, species_6
	UNION ALL
	SELECT inventory, species_7 AS species, count(*) AS feature_count, sum(area*species_per_7/100) AS area_ha
	FROM species_table
	WHERE isError(species_7) = FALSE AND isError(species_per_7::text) = FALSE
	GROUP BY inventory, species_7
	UNION ALL
	SELECT inventory, species_8 AS species, count(*) AS feature_count, sum(area*species_per_8/100) AS area_ha
	FROM species_table
	WHERE isError(species_8) = FALSE AND isError(species_per_8::text) = FALSE
	GROUP BY inventory, species_8
	UNION ALL
	SELECT inventory, species_9 AS species, count(*) AS feature_count, sum(area*species_per_9/100) AS area_ha
	FROM species_table
	WHERE isError(species_9) = FALSE AND isError(species_per_9::text) = FALSE
	GROUP BY inventory, species_9
	UNION ALL
	SELECT inventory, species_10 AS species, count(*) AS feature_count, sum(area*species_per_10/100) AS area_ha
	FROM species_table
	WHERE isError(species_10) = FALSE AND isError(species_per_10::text) = FALSE
	GROUP BY inventory, species_10
)
SELECT inventory, species, sum(feature_count) AS feature_count, sum(area_ha) AS area_ha
FROM all_species
GROUP BY inventory, species
ORDER BY inventory, sum(area_ha) desc;

COMMENT ON MATERIALIZED VIEW all_species_area_counts_per_inv IS
'all_species_area_counts_per_inv provides the total area and feature count of each species type in the lyr_all table, per inventory';


-- Total species area and count per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_area_counts_per_jurisdiction AS
SELECT LEFT(inventory, 2) AS jurisdiction, species, sum(feature_count) AS feature_count, sum(area_ha) AS area_ha, 
rank() over (PARTITION BY LEFT(inventory, 2) ORDER BY sum(area_ha) desc)
FROM all_species_area_counts_per_inv
GROUP BY jurisdiction, species
ORDER BY jurisdiction, area_ha desc;

COMMENT ON MATERIALIZED VIEW all_species_area_counts_per_jurisdiction IS
'all_species_area_counts_per_jurisdiction provides the total area and feature count of each species type in the lyr_all table, per jurisdiction';


-- Total species area and count
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_area_counts AS
SELECT species, sum(feature_count) AS feature_count, sum(area_ha) AS area_ha, 
rank() over (order by sum(area_ha) desc)
FROM all_species_area_counts_per_inv
GROUP BY species
ORDER BY area_ha desc;

COMMENT ON MATERIALIZED VIEW all_species_area_counts IS
'all_species_area_counts provides the total area and feature count of each species type in the lyr_all table';


-- Total species area and count, per species rank, per jurisdiciton
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_1_to_10_area_count_per_jurisdiction AS
WITH species_table AS (
	-- Filter table beforehand to optimize
	SELECT LEFT(l.cas_id, 2) AS jurisdiction, species_1, species_per_1, species_2, species_per_2, species_3, species_per_3, species_4, species_per_4, species_5, species_per_5,
	       species_6, species_per_6, species_7, species_per_7, species_8, species_per_8, species_9, species_per_9, species_10, species_per_10, c.casfri_area AS area
	FROM lyr_all l
	INNER JOIN cas_all c ON c.cas_id = l.cas_id
), species_1 AS (
	SELECT jurisdiction, species_1, count(*) AS species_1_feature_count, sum(area*species_per_1/100) AS species_1_area_ha
	FROM species_table
	WHERE isError(species_1) = FALSE AND isError(species_per_1::text) = FALSE
	GROUP BY jurisdiction, species_1
), species_2 AS (
	SELECT jurisdiction, species_2, count(*) AS species_2_feature_count, sum(area*species_per_2/100) AS species_2_area_ha
	FROM species_table
	WHERE isError(species_2) = FALSE AND isError(species_per_2::text) = FALSE
	GROUP BY jurisdiction, species_2
), species_3 AS (
	SELECT jurisdiction, species_3, count(*) AS species_3_feature_count, sum(area*species_per_3/100) AS species_3_area_ha
	FROM species_table
	WHERE isError(species_3) = FALSE AND isError(species_per_3::text) = FALSE
	GROUP BY jurisdiction, species_3
), species_4 AS (
	SELECT jurisdiction, species_4, count(*) AS species_4_feature_count, sum(area*species_per_4/100) AS species_4_area_ha
	FROM species_table
	WHERE isError(species_4) = FALSE AND isError(species_per_4::text) = FALSE
	GROUP BY jurisdiction, species_4
), species_5 AS (
	SELECT jurisdiction, species_5, count(*) AS species_5_feature_count, sum(area*species_per_5/100) AS species_5_area_ha
	FROM species_table
	WHERE isError(species_5) = FALSE AND isError(species_per_5::text) = FALSE
	GROUP BY jurisdiction, species_5
), species_6 AS (
	SELECT jurisdiction, species_6, count(*) AS species_6_feature_count, sum(area*species_per_6/100) AS species_6_area_ha
	FROM species_table
	WHERE isError(species_6) = FALSE AND isError(species_per_6::text) = FALSE
	GROUP BY jurisdiction, species_6
), species_7 AS (
	SELECT jurisdiction, species_7, count(*) AS species_7_feature_count, sum(area*species_per_7/100) AS species_7_area_ha
	FROM species_table
	WHERE isError(species_7) = FALSE AND isError(species_per_7::text) = FALSE
	GROUP BY jurisdiction, species_7
), species_8 AS (
	SELECT jurisdiction, species_8, count(*) AS species_8_feature_count, sum(area*species_per_8/100) AS species_8_area_ha
	FROM species_table
	WHERE isError(species_8) = FALSE AND isError(species_per_8::text) = FALSE
	GROUP BY jurisdiction, species_8
), species_9 AS (
	SELECT jurisdiction, species_9, count(*) AS species_9_feature_count, sum(area*species_per_9/100) AS species_9_area_ha
	FROM species_table
	WHERE isError(species_9) = FALSE AND isError(species_per_9::text) = FALSE
	GROUP BY jurisdiction, species_9
), species_10 AS (
	SELECT jurisdiction, species_10, count(*) AS species_10_feature_count, sum(area*species_per_10/100) AS species_10_area_ha
	FROM species_table
	WHERE isError(species_10) = FALSE AND isError(species_per_10::text) = FALSE
	GROUP BY jurisdiction, species_10
)
SELECT s1.jurisdiction, 
-- Case statement is needed here to not display a null species if a species only is ever a species_5 for example
CASE WHEN (s1.species_1 IS NOT null) THEN s1.species_1 
WHEN (s2.species_2 IS NOT null) THEN s2.species_2
WHEN (s3.species_3 IS NOT null) THEN s3.species_3
WHEN (s4.species_4 IS NOT null) THEN s4.species_4
WHEN (s5.species_5 IS NOT null) THEN s5.species_5
WHEN (s6.species_6 IS NOT null) THEN s6.species_6
WHEN (s7.species_7 IS NOT null) THEN s7.species_7
WHEN (s8.species_8 IS NOT null) THEN s8.species_8
WHEN (s9.species_9 IS NOT null) THEN s9.species_9
ELSE s10.species_10 END AS species,
s1.species_1_feature_count, s1.species_1_area_ha, 
s2.species_2_feature_count, s2.species_2_area_ha,
s3.species_3_feature_count, s3.species_3_area_ha,
s4.species_4_feature_count, s4.species_4_area_ha,
s5.species_5_feature_count, s5.species_5_area_ha,
s6.species_6_feature_count, s6.species_6_area_ha,
s7.species_7_feature_count, s7.species_7_area_ha,
s8.species_8_feature_count, s8.species_8_area_ha,
s9.species_9_feature_count, s9.species_9_area_ha,
s10.species_10_feature_count, s10.species_10_area_ha
FROM species_1 s1
FULL JOIN species_2 s2 ON s1.species_1 = s2.species_2 AND s1.jurisdiction = s2.jurisdiction
FULL JOIN species_3 s3 ON s1.species_1 = s3.species_3 AND s1.jurisdiction = s3.jurisdiction
FULL JOIN species_4 s4 ON s1.species_1 = s4.species_4 AND s1.jurisdiction = s4.jurisdiction
FULL JOIN species_5 s5 ON s1.species_1 = s5.species_5 AND s1.jurisdiction = s5.jurisdiction
FULL JOIN species_6 s6 ON s1.species_1 = s6.species_6 AND s1.jurisdiction = s6.jurisdiction
FULL JOIN species_7 s7 ON s1.species_1 = s7.species_7 AND s1.jurisdiction = s7.jurisdiction
FULL JOIN species_8 s8 ON s1.species_1 = s8.species_8 AND s1.jurisdiction = s8.jurisdiction
FULL JOIN species_9 s9 ON s1.species_1 = s9.species_9 AND s1.jurisdiction = s9.jurisdiction
FULL JOIN species_10 s10 ON s1.species_1 = s10.species_10 AND s1.jurisdiction = s10.jurisdiction;

COMMENT ON MATERIALIZED VIEW all_species_1_to_10_area_count_per_jurisdiction IS
'all_species_1_to_10_area_count_per_jurisdiction provides the area and feature count of each of species_1 to species_10 in the lyr_all table, per jurisdiction';


-- Total species area and count, per species rank
CREATE MATERIALIZED VIEW IF NOT EXISTS all_species_1_to_10_area_count AS
SELECT species,
sum(species_1_feature_count) AS species_1_feature_count, sum(species_1_area_ha) AS species_1_area_ha, 
sum(species_2_feature_count) AS species_2_feature_count, sum(species_2_area_ha) AS species_2_area_ha,
sum(species_3_feature_count) AS species_3_feature_count, sum(species_3_area_ha) AS species_3_area_ha,
sum(species_4_feature_count) AS species_4_feature_count, sum(species_4_area_ha) AS species_4_area_ha,
sum(species_5_feature_count) AS species_5_feature_count, sum(species_5_area_ha) AS species_5_area_ha,
sum(species_6_feature_count) AS species_6_feature_count, sum(species_6_area_ha) AS species_6_area_ha,
sum(species_7_feature_count) AS species_7_feature_count, sum(species_7_area_ha) AS species_7_area_ha,
sum(species_8_feature_count) AS species_8_feature_count, sum(species_8_area_ha) AS species_8_area_ha,
sum(species_9_feature_count) AS species_9_feature_count, sum(species_9_area_ha) AS species_9_area_ha,
sum(species_10_feature_count) AS species_10_feature_count, sum(species_10_area_ha) AS species_10_area_ha
FROM all_species_1_to_10_area_count_per_jurisdiction
GROUP BY species
ORDER BY species;

COMMENT ON MATERIALIZED VIEW all_species_1_to_10_area_count IS
'all_species_1_to_10_area_count provides the area and feature count of each of species_1 to species_10 in the lyr_all table';


-- Area of species originating in each year, per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS species_area_by_stand_origin_year_per_jurisdiction AS
WITH species_filter AS (
	SELECT l.cas_id, (origin_lower+origin_upper)/2 AS est_origin_year, c.casfri_area AS area_ha, 
	species_1, species_2, species_3, species_4, species_5, species_6, species_7, species_8, species_9, species_10,
	species_per_1, species_per_2, species_per_3, species_per_4, species_per_5, species_per_6, species_per_7, species_per_8, species_per_9, species_per_10
	FROM lyr_all l
	INNER JOIN cas_all c ON l.cas_id = c.cas_id
	WHERE isError(origin_upper::text) =  FALSE AND isError(origin_lower::text) = FALSE
), all_species AS (
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_1 AS species, sum(species_per_1*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_1) = FALSE and isError(species_per_1::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_2 AS species, sum(species_per_2*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_2) = FALSE and isError(species_per_2::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_3 AS species, sum(species_per_3*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_3) = FALSE and isError(species_per_3::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_4 AS species, sum(species_per_4*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_4) = FALSE and isError(species_per_4::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_5 AS species, sum(species_per_5*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_5) = FALSE and isError(species_per_5::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_6 AS species, sum(species_per_6*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_6) = FALSE and isError(species_per_6::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_7 AS species, sum(species_per_7*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_7) = FALSE and isError(species_per_7::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_8 AS species, sum(species_per_8*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_8) = FALSE and isError(species_per_8::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_9 AS species, sum(species_per_9*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_9) = FALSE and isError(species_per_9::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
	UNION ALL
	SELECT est_origin_year, LEFT(cas_id, 2) AS jurisdiction, species_10 AS species, sum(species_per_10*area_ha/100) AS area_ha
	FROM species_filter
	WHERE isError(species_10) = FALSE and isError(species_per_10::text) = FALSE
	GROUP BY est_origin_year, jurisdiction, species
)
SELECT est_origin_year, jurisdiction, species, sum(area_ha) AS area_ha
FROM all_species 
GROUP BY est_origin_year, jurisdiction, species
ORDER BY est_origin_year, jurisdiction, species;

COMMENT ON MATERIALIZED VIEW casfri_metrics.species_area_by_stand_origin_year_per_jurisdiction IS
'species_area_by_stand_origin_year_per_jurisdiction provides the total area of each species per origin year, per jurisdiction. Origin year is average of origin_upper and origin_lower';


-- Area of species originating in each year
CREATE MATERIALIZED VIEW IF NOT EXISTS species_area_by_stand_origin_year AS
SELECT est_origin_year, species, sum(area_ha) AS area_ha
FROM species_area_by_stand_origin_year_per_jurisdiction 
GROUP BY est_origin_year, species
ORDER BY est_origin_year, species;

COMMENT ON MATERIALIZED VIEW casfri_metrics.species_area_by_stand_origin_year IS
'species_area_by_stand_origin_year provides the total area of each species per origin year. Origin year is average of origin_upper and origin_lower';


-- Count of stands changed leadign genus between consecutive inventories
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_change_in_leading_genus AS
WITH species_list AS (
	SELECT LEFT(cas_id, 4) AS inventory, ltrim(split_part(cas_id, '-', 4), 'x') AS stand_id, left(species_1, 4) AS genus
	FROM lyr_all l
	WHERE layer = 1
), prev_genus_list AS (
	SELECT inventory, stand_id, 
	genus, 
	(CASE WHEN lag(stand_id) OVER (ORDER BY stand_id, inventory) != stand_id THEN NULL 
		  WHEN LAG(inventory) OVER (ORDER BY stand_id, inventory) = inventory THEN NULL ELSE 
	lag(genus) OVER (ORDER BY stand_id, inventory) END) AS prev_genus
	FROM species_list
)
SELECT inventory, count(*) AS count_stands_changed_leading_genus
FROM prev_genus_list
WHERE genus != prev_genus
AND prev_genus IS NOT null
GROUP BY inventory
ORDER BY inventory;

COMMENT ON MATERIALIZED VIEW stand_change_in_leading_genus IS
'stand_change_in_leading_genus records the count of stands that changed leading genus since the previous inventory, per inventory';


-- Area of genus types converted to an anth NFL type
CREATE MATERIALIZED VIEW IF NOT EXISTS genus_converted_to_nfl AS
WITH nfl_types AS (
	SELECT cas_id, LEFT(cas_id, 4) AS inventory,
	ltrim(split_part(cas_id, '-', 4), 'x') AS stand_id, (RIGHT(LEFT(cas_id, 4), 2)::Integer - 1)::TEXT AS prev,
	(CASE WHEN isError(nat_non_veg) = FALSE THEN 'NAT_NON_VEG'
		  WHEN isError(non_for_anth) = FALSE THEN 'NON_FOR_ANTH'
		  WHEN isError(non_for_veg) = FALSE THEN 'NON_FOR_VEG'
		  ELSE 'UNKNOWN' END) AS nfl_type 
	FROM nfl_all
	WHERE layer = 1 
	-- Filter out first inventory for every jurisdiction
	AND LEFT(cas_id, 4) NOT IN (
	WITH prev_invs AS (
	SELECT inventory_id, lag(inventory_id) OVER (ORDER BY inventory_id) AS prev_inv
	FROM hdr_all
	)
		SELECT inventory_id
		FROM prev_invs
		WHERE left(inventory_id, 2) != left(prev_inv, 2) OR prev_inv IS NULL)
), lyrs AS (
	SELECT cas_id, LEFT(cas_id, 4) AS inventory,
	ltrim(split_part(cas_id, '-', 4), 'x') AS stand_id,	LEFT(species_1, 4) AS genus
	FROM lyr_all
	WHERE layer = 1 AND isError(species_1) = false
)
SELECT n.inventory, l.genus, n.nfl_type, sum(c.casfri_area) AS area_ha, count(*) AS feature_count
FROM nfl_types n
INNER JOIN lyrs l ON (n.stand_id = l.stand_id AND 
(LEFT(n.inventory, 2)||RIGHT('00'::text||((RIGHT(LEFT(n.cas_id, 4), 2)::Integer - 1)::TEXT),2) = l.inventory))
INNER JOIN cas_all c ON l.cas_id = c.cas_id
GROUP BY n.inventory, genus, nfl_type
ORDER BY n.inventory;

COMMENT ON MATERIALIZED VIEW genus_converted_to_nfl IS
'genus_converted_to_nfl records the count and area of each stand that was converted to nfl, aggregated by species_1 genus';



-- Centroid coordinates of the 100 largest disturbances of each type per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS disturbance_coords_per_jurisdiction AS
WITH dists AS (
	SELECT d.cas_id, LEFT(d.cas_id, 2) AS jurisdiction, dist_type_1 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_1, dist_ext_lower_1)/100 AS area_ha
	FROM dst_all d
	INNER JOIN cas_all c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_1) = FALSE
	UNION ALL
	SELECT d.cas_id, LEFT(d.cas_id, 2) AS jurisdiction, dist_type_2 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_2, dist_ext_lower_2)/100 AS area_ha
	FROM dst_all d
	INNER JOIN cas_all c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_2) = FALSE
	UNION ALL
	SELECT d.cas_id, LEFT(d.cas_id, 2) AS jurisdiction, dist_type_3 AS dist_type, c.casfri_area*getDistExt(dist_ext_upper_3, dist_ext_lower_3)/100 AS area_ha
	FROM dst_all d
	INNER JOIN cas_all c ON c.cas_id = d.cas_id
	WHERE isError(dist_type_3) = FALSE
), ranks AS (
	SELECT cas_id, jurisdiction, dist_type, area_ha, rank() OVER (PARTITION BY jurisdiction, dist_type ORDER BY area_ha DESC)
	FROM dists
)
SELECT jurisdiction, r.dist_type, r.area_ha, st_x(st_transform(st_centroid(g.geometry),4326)) as lon, st_y(st_transform(st_centroid(g.geometry),4326)) as lat
FROM ranks r
INNER JOIN geo_all g ON r.cas_id = g.cas_id
WHERE r.RANK <= 100;

COMMENT ON MATERIALIZED VIEW disturbance_coords_per_jurisdiction IS
'disturbance_coords_per_jurisdiction records the area and centroid coordinates of the 100 largest disturbances per jurisdiction for each disturbance type';


-- Centroid coordinates of the 100 largest disturbances of each type 
CREATE MATERIALIZED VIEW IF NOT EXISTS disturbance_coords AS
WITH ranks AS (
	SELECT dist_type, area_ha, lon, lat, rank() OVER (PARTITION BY dist_type ORDER BY area_ha desc)
	FROM disturbance_coords_per_jurisdiction
)
SELECT dist_type, area_ha, lon, lat
FROM ranks r
WHERE r.RANK <= 100;

COMMENT ON MATERIALIZED VIEW disturbance_coords IS
'disturbance_coords records the area and centroid coordinates of the 100 largest disturbances for each disturbance type';


-- Soil moisture regime areas per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS soil_moist_reg_per_inventory AS
WITH soil_moist AS (
	SELECT left(l.cas_id, 4) AS inventory, l.soil_moist_reg, sum(c.casfri_area) AS area_ha
	FROM lyr_all l
	INNER JOIN cas_all c ON c.cas_id = l.cas_id
	WHERE layer = 1
	GROUP BY inventory, l.soil_moist_reg
), inventories AS (
	SELECT DISTINCT inventory
	FROM soil_moist
), aquatic AS (
	SELECT inventory, area_ha AS aquatic_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'AQUATIC'
), moist AS (
	SELECT inventory, area_ha AS moist_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'MOIST'
), mesic AS (
	SELECT inventory, area_ha AS mesic_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'MESIC'
), dry AS (
	SELECT inventory, area_ha AS dry_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'DRY'
), wet AS (
	SELECT inventory, area_ha AS wet_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'WET'
), error AS (
	SELECT inventory, sum(area_ha) AS error_area_ha
	FROM soil_moist
	WHERE isError(soil_moist_reg) = TRUE
	GROUP BY inventory
)
SELECT s.inventory, aquatic_area_ha, moist_area_ha, mesic_area_ha, dry_area_ha, wet_area_ha, error_area_ha
FROM inventories s
FULL JOIN aquatic a ON s.inventory = a.inventory 
FULL JOIN moist m ON s.inventory = m.inventory 
FULL JOIN mesic me ON s.inventory = me.inventory 
FULL JOIN dry d ON s.inventory = d.inventory 
FULL JOIN wet w ON s.inventory = w.inventory
FULL JOIN error e ON s.inventory = e.inventory
ORDER BY inventory; 

COMMENT ON MATERIALIZED VIEW soil_moist_reg_per_inventory IS
'soil_moist_reg_per_inventory records the area of each soil moisture regime type per inventory';


-- Soil moisture regime areas per leading species
CREATE MATERIALIZED VIEW IF NOT EXISTS soil_moist_reg_per_species AS
WITH soil_moist AS (
	SELECT l.species_1 AS species, l.soil_moist_reg, sum(c.casfri_area*species_per_1/100) AS area_ha
	FROM lyr_all l
	INNER JOIN cas_all c ON c.cas_id = l.cas_id
	WHERE isError(l.species_1) = FALSE AND isError(species_per_1::text) = FALSE AND layer = 1
	GROUP BY l.species_1, l.soil_moist_reg
), species AS (
	SELECT DISTINCT species
	FROM soil_moist
), aquatic AS (
	SELECT species, area_ha AS aquatic_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'AQUATIC'
), moist AS (
	SELECT species, area_ha AS moist_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'MOIST'
), mesic AS (
	SELECT species, area_ha AS mesic_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'MESIC'
), dry AS (
	SELECT species, area_ha AS dry_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'DRY'
), wet AS (
	SELECT species, area_ha AS wet_area_ha
	FROM soil_moist
	WHERE soil_moist_reg = 'WET'
), error AS (
	SELECT species, sum(area_ha) AS error_area_ha
	FROM soil_moist
	WHERE isError(soil_moist_reg) = true
	GROUP BY species
)
SELECT s.species, aquatic_area_ha, moist_area_ha, mesic_area_ha, dry_area_ha, wet_area_ha, error_area_ha
FROM species s
FULL JOIN aquatic a ON s.species = a.species 
FULL JOIN moist m ON s.species = m.species 
FULL JOIN mesic me ON s.species = me.species 
FULL JOIN dry d ON s.species = d.species 
FULL JOIN wet w ON s.species = w.species
FULL JOIN error e ON s.species = e.species
ORDER BY species; 

COMMENT ON MATERIALIZED VIEW soil_moist_reg_per_species IS
'soil_moist_reg_per_species records the area of each soil moisture regime type per leading species';


-- Forest productivity areas per inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_productivity_per_inventory AS
WITH prods AS (
	SELECT left(l.cas_id, 4) AS inventory, productivity, sum(c.casfri_area) AS area_ha
	FROM lyr_all l
	INNER JOIN cas_all c ON l.cas_id = c.cas_id
	WHERE l.layer = 1
	GROUP BY inventory, productivity
), inventories AS (
	SELECT DISTINCT inventory
	FROM prods
), non_productive_forest AS (
	SELECT inventory, area_ha AS non_productive_forest_area_ha
	FROM prods
	WHERE productivity = 'NON_PRODUCTIVE_FOREST'
), productive_forest AS (
	SELECT inventory, area_ha AS productive_forest_area_ha
	FROM prods
	WHERE productivity = 'PRODUCTIVE_FOREST'
), error AS (
	SELECT inventory, sum(area_ha) AS error_area_ha
	FROM prods
	WHERE isError(productivity) = TRUE
	GROUP BY inventory
)
SELECT i.inventory, non_productive_forest_area_ha, productive_forest_area_ha, error_area_ha
FROM inventories i
FULL JOIN non_productive_forest n ON n.inventory = i.inventory
FULL JOIN productive_forest p ON p.inventory = i.inventory
FULL JOIN error e ON e.inventory = i.inventory
ORDER BY inventory;

COMMENT ON MATERIALIZED VIEW forest_productivity_per_inventory IS
'forest_productivity_per_inventory records the area of each forest layer productivity value per inventory';


-- Forest productivity areas per leading species
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_productivity_per_species AS
WITH prods AS (
	SELECT l.species_1 AS species, productivity, sum(c.casfri_area*species_per_1/100) AS area_ha
	FROM lyr_all l
	INNER JOIN cas_all c ON l.cas_id = c.cas_id
	WHERE l.layer = 1 AND isError(species_1) = FALSE AND isError(species_per_1::text) = false
	GROUP BY species, productivity
), species AS (
	SELECT DISTINCT species
	FROM prods
), non_productive_forest AS (
	SELECT species, area_ha AS non_productive_forest_area_ha
	FROM prods
	WHERE productivity = 'NON_PRODUCTIVE_FOREST'
), productive_forest AS (
	SELECT species, area_ha AS productive_forest_area_ha
	FROM prods
	WHERE productivity = 'PRODUCTIVE_FOREST'
), error AS (
	SELECT species, sum(area_ha) AS error_area_ha
	FROM prods
	WHERE isError(productivity) = TRUE
	GROUP BY species
)
SELECT i.species, non_productive_forest_area_ha, productive_forest_area_ha, error_area_ha
FROM species i
FULL JOIN non_productive_forest n ON n.species = i.species
FULL JOIN productive_forest p ON p.species = i.species
FULL JOIN error e ON e.species = i.species
ORDER BY species;

COMMENT ON MATERIALIZED VIEW forest_productivity_per_species IS
'forest_productivity_per_species records the area of each forest layer productivity value per leading species';


-- Forest productivity type areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_productivity_type_per_inventory AS
WITH prods AS (
	SELECT left(l.cas_id, 4) AS inventory, productivity_type, sum(c.casfri_area) AS area_ha
	FROM lyr_all l
	INNER JOIN cas_all c ON l.cas_id = c.cas_id
	WHERE l.layer = 1
	GROUP BY inventory, productivity_type
), inventories AS (
	SELECT DISTINCT inventory
	FROM prods
), harvestable AS (
	SELECT inventory, area_ha AS harvestable_area_ha
	FROM prods
	WHERE productivity_type = 'HARVESTABLE'
), protection_forest AS (
	SELECT inventory, area_ha AS protection_forest_area_ha
	FROM prods
	WHERE productivity_type = 'PROTECTION_FOREST'
), treed_muskeg AS (
	SELECT inventory, area_ha AS treed_muskeg_area_ha
	FROM prods
	WHERE productivity_type = 'TREED_MUSKEG'
), treed_rock AS (
	SELECT inventory, area_ha AS treed_rock_area_ha
	FROM prods
	WHERE productivity_type = 'TREED_ROCK'
), alpine_forest AS (
	SELECT inventory, area_ha AS alpine_forest_area_ha
	FROM prods
	WHERE productivity_type = 'ALPINE_FOREST'
), scrub_shrub AS (
	SELECT inventory, area_ha AS scrub_shrub_area_ha
	FROM prods
	WHERE productivity_type = 'SCRUB_SHRUB'
), alder AS (
	SELECT inventory, area_ha AS alder_area_ha
	FROM prods
	WHERE productivity_type = 'ALDER'
), error AS (
	SELECT inventory, sum(area_ha) AS error_area_ha
	FROM prods
	WHERE isError(productivity_type) = TRUE
	GROUP BY inventory
)
SELECT i.inventory, harvestable_area_ha, protection_forest_area_ha, treed_muskeg_area_ha, treed_rock_area_ha, 
alpine_forest_area_ha, scrub_shrub_area_ha, alder_area_ha, error_area_ha
FROM inventories i
FULL JOIN harvestable h ON h.inventory = i.inventory
FULL JOIN protection_forest p ON p.inventory = i.inventory
FULL JOIN treed_muskeg tm ON tm.inventory = i.inventory
FULL JOIN treed_rock tr ON tr.inventory = i.inventory
FULL JOIN alpine_forest af ON af.inventory = i.inventory
FULL JOIN scrub_shrub ss ON ss.inventory = i.inventory
FULL JOIN alder a ON a.inventory = i.inventory
FULL JOIN error e ON e.inventory = i.inventory
ORDER BY inventory;

COMMENT ON MATERIALIZED VIEW forest_productivity_type_per_inventory IS
'forest_productivity_type_per_inventory records the area of each forest layer productivity type per inventory';


-- Forest productivity type per leading species
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_productivity_type_per_species AS
WITH prods AS (
	SELECT l.species_1 AS species, productivity_type, sum(c.casfri_area*species_per_1/100) AS area_ha
	FROM lyr_all l
	INNER JOIN cas_all c ON l.cas_id = c.cas_id
	WHERE l.layer = 1 AND isError(species_1) = FALSE AND isError(species_per_1::text) = FALSE
	GROUP BY species, productivity_type
), species AS (
	SELECT DISTINCT species
	FROM prods
), harvestable AS (
	SELECT species, area_ha AS harvestable_area_ha
	FROM prods
	WHERE productivity_type = 'HARVESTABLE'
), protection_forest AS (
	SELECT species, area_ha AS protection_forest_area_ha
	FROM prods
	WHERE productivity_type = 'PROTECTION_FOREST'
), treed_muskeg AS (
	SELECT species, area_ha AS treed_muskeg_area_ha
	FROM prods
	WHERE productivity_type = 'TREED_MUSKEG'
), treed_rock AS (
	SELECT species, area_ha AS treed_rock_area_ha
	FROM prods
	WHERE productivity_type = 'TREED_ROCK'
), alpine_forest AS (
	SELECT species, area_ha AS alpine_forest_area_ha
	FROM prods
	WHERE productivity_type = 'ALPINE_FOREST'
), scrub_shrub AS (
	SELECT species, area_ha AS scrub_shrub_area_ha
	FROM prods
	WHERE productivity_type = 'SCRUB_SHRUB'
), alder AS (
	SELECT species, area_ha AS alder_area_ha
	FROM prods
	WHERE productivity_type = 'ALDER'
), error AS (
	SELECT species, sum(area_ha) AS error_area_ha
	FROM prods
	WHERE isError(productivity_type) = TRUE
	GROUP BY species
)
SELECT i.species, harvestable_area_ha, protection_forest_area_ha, treed_muskeg_area_ha, treed_rock_area_ha, 
alpine_forest_area_ha, scrub_shrub_area_ha, alder_area_ha, error_area_ha
FROM species i
FULL JOIN harvestable h ON h.species = i.species
FULL JOIN protection_forest p ON p.species = i.species
FULL JOIN treed_muskeg tm ON tm.species = i.species
FULL JOIN treed_rock tr ON tr.species = i.species
FULL JOIN alpine_forest af ON af.species = i.species
FULL JOIN scrub_shrub ss ON ss.species = i.species
FULL JOIN alder a ON a.species = i.species
FULL JOIN error e ON e.species = i.species
ORDER BY species;

COMMENT ON MATERIALIZED VIEW forest_productivity_type_per_species IS
'forest_productivity_type_per_species records the area of each forest layer productivity type per leading species';


-- Eco site wetland type areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS eco_wetland_type_per_inventory AS
SELECT LEFT(e.cas_id, 4) AS inventory, wetland_type, sum(c.casfri_area) AS area_ha
FROM eco_all e
INNER JOIN cas_all c ON e.cas_id = c.cas_id
GROUP BY inventory, wetland_type
ORDER BY inventory, wetland_type;

COMMENT ON MATERIALIZED VIEW eco_wetland_type_per_inventory IS
'eco_wetland_type_per_inventory records the area of each eco_all wetland type per inventory';


-- Eco site wet_veg_cover areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS eco_wet_veg_cover_per_inventory AS
SELECT LEFT(e.cas_id, 4) AS inventory, wet_veg_cover, sum(c.casfri_area) AS area_ha
FROM eco_all e
INNER JOIN cas_all c ON e.cas_id = c.cas_id
GROUP BY inventory, wet_veg_cover
ORDER BY inventory, wet_veg_cover;

COMMENT ON MATERIALIZED VIEW eco_wet_veg_cover_per_inventory IS
'eco_wet_veg_cover_per_inventory records the area of each eco_all wet_veg_cover type per inventory';


-- Eco site wet_landform_mod areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS eco_wet_landform_mod_per_inventory AS
SELECT LEFT(e.cas_id, 4) AS inventory, wet_landform_mod, sum(c.casfri_area) AS area_ha
FROM eco_all e
INNER JOIN cas_all c ON e.cas_id = c.cas_id
GROUP BY inventory, wet_landform_mod
ORDER BY inventory, wet_landform_mod;

COMMENT ON MATERIALIZED VIEW eco_wet_landform_mod_per_inventory IS
'eco_wet_landform_mod_per_inventory records the area of each eco_all wet_landform_mod type per inventory';


-- Eco site wet_local_mod areas by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS eco_wet_local_mod_per_inventory AS
SELECT LEFT(e.cas_id, 4) AS inventory, wet_local_mod, sum(c.casfri_area) AS area_ha
FROM eco_all e
INNER JOIN cas_all c ON e.cas_id = c.cas_id
GROUP BY inventory, wet_local_mod
ORDER BY inventory, wet_local_mod;

COMMENT ON MATERIALIZED VIEW eco_wet_local_mod_per_inventory IS
'eco_wet_local_mod_per_inventory records the area of each eco_all wet_local_mod type per inventory';


-- Treed polygon areas
CREATE MATERIALIZED VIEW IF NOT EXISTS treed_polygon_sizes_overall AS
SELECT avg(c.casfri_area) AS mean_area_ha,
stddev(c.casfri_area) AS std_dev
FROM lyr_all l
INNER JOIN cas_all c ON c.cas_id = l.cas_id
WHERE layer = 1;

COMMENT ON MATERIALIZED VIEW treed_polygon_sizes_overall IS
'treed_polygon_sizes_overall records the area and standard deviation of the total forested area in CASFRI layer 1';


-- Treed polygon areas per jurisdiction
CREATE MATERIALIZED VIEW IF NOT EXISTS treed_polygon_sizes_per_jurisdiction AS
SELECT LEFT(c.cas_id, 2) AS jurisdiction, avg(c.casfri_area) AS mean_area_ha,
stddev(c.casfri_area) AS std_dev
FROM lyr_all l
INNER JOIN cas_all c ON c.cas_id = l.cas_id
WHERE layer = 1
GROUP BY jurisdiction;

COMMENT ON MATERIALIZED VIEW treed_polygon_sizes_per_jurisdiction IS
'treed_polygon_sizes_per_jurisdiction records the area and standard deviation of the total forested area in CASFRI layer 1, per jurisdiction';


-- stand_photo_year aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW stand_photo_year_histogram AS
WITH ranges AS (
	SELECT foo.inventory, r.min_stand_photo_year, r.max_stand_photo_year
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_years_per_inv r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_stand_photo_year,2000), COALESCE(max_stand_photo_year,2020)) stand_photo_year
	FROM ranges
), years AS (
	SELECT left(cas_id,4) AS inventory, stand_photo_year, count(*)
	FROM casfri50.cas_all
	WHERE casfri_metrics.isError(stand_photo_year) = FALSE
	GROUP BY inventory, stand_photo_year
)
SELECT s.inventory, s.stand_photo_year, COALESCE(count, 0) AS count
FROM years y
FULL JOIN series s ON s.inventory = y.inventory AND s.stand_photo_year = y.stand_photo_year
ORDER BY inventory, stand_photo_year;

COMMENT ON MATERIALIZED VIEW stand_photo_year_histogram IS
'stand_photo_year_histogram aggregates the stand_photo_years per inventory';


-- Range of origin_upper by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_origin_upper_per_inv AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(origin_upper::text) = false THEN origin_upper ELSE NULL end) AS min_origin_upper, 
max(CASE WHEN isError(origin_upper::text) = false THEN origin_upper ELSE NULL end) AS max_origin_upper
FROM lyr_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_origin_upper_per_inv IS
'range_of_origin_upper_per_inv provides the range of recorded origin_upper for each inventory';


-- Range of origin_lower by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_origin_lower_per_inv AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(origin_lower::text) = false THEN origin_lower ELSE NULL end) AS min_origin_lower, 
max(CASE WHEN isError(origin_lower::text) = false THEN origin_lower ELSE NULL end) AS max_origin_lower
FROM lyr_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_origin_lower_per_inv IS
'range_of_origin_lower_per_inv provides the range of recorded origin_lower for each inventory';


-- origin_upper aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW origin_upper_histogram AS
WITH ranges AS (
	SELECT foo.inventory, r.min_origin_upper, r.max_origin_upper
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_origin_upper_per_inv r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_origin_upper,2000), COALESCE(max_origin_upper,2020)) origin_upper
	FROM ranges
), years AS (
	SELECT left(cas_id,4) AS inventory, origin_upper, count(*)
	FROM casfri50.lyr_all
	WHERE casfri_metrics.isError(origin_upper) = FALSE
	GROUP BY inventory, origin_upper
)
SELECT s.inventory, s.origin_upper, COALESCE(count, 0) AS count
FROM years y
FULL JOIN series s ON s.inventory = y.inventory AND s.origin_upper = y.origin_upper
ORDER BY inventory, origin_upper;

COMMENT ON MATERIALIZED VIEW origin_upper_histogram IS
'origin_upper_histogram aggregates the origin_upper per inventory';


-- origin_lower aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW origin_lower_histogram AS
WITH ranges AS (
	SELECT foo.inventory, r.min_origin_lower, r.max_origin_lower
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_origin_lower_per_inv r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_origin_lower,2000), COALESCE(max_origin_lower,2020)) origin_lower
	FROM ranges
), years AS (
	SELECT left(cas_id,4) AS inventory, origin_lower, count(*)
	FROM casfri50.lyr_all
	WHERE casfri_metrics.isError(origin_lower) = FALSE
	GROUP BY inventory, origin_lower
)
SELECT s.inventory, s.origin_lower, COALESCE(count, 0) AS count
FROM years y
FULL JOIN series s ON s.inventory = y.inventory AND s.origin_lower = y.origin_lower
ORDER BY inventory, origin_lower;

COMMENT ON MATERIALIZED VIEW origin_lower_histogram IS
'origin_lower_histogram aggregates the origin_lower per inventory';


-- Range of height_upper by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_height_upper_per_inv AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(height_upper::text) = false THEN height_upper ELSE NULL end) AS min_height_upper, 
max(CASE WHEN isError(height_upper::text) = false THEN height_upper ELSE NULL end) AS max_height_upper
FROM lyr_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_height_upper_per_inv IS
'range_of_height_upper_per_inv provides the range of recorded height_upper for each inventory';


-- Range of height_lower by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_height_lower_per_inv AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(height_lower::text) = false THEN height_lower ELSE NULL end) AS min_height_lower, 
max(CASE WHEN isError(height_lower::text) = false THEN height_lower ELSE NULL end) AS max_height_lower
FROM lyr_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_height_lower_per_inv IS
'range_of_height_lower_per_inv provides the range of recorded height_lower for each inventory';


-- height_upper aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW IF NOT EXISTS height_upper_histogram AS
WITH ranges AS (
	SELECT foo.inventory, round(r.min_height_upper::numeric,0)::int AS min_height_upper, round(r.max_height_upper::numeric,0)::int AS max_height_upper
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_height_upper_per_inv r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_height_upper,0), COALESCE(max_height_upper,100)) height_upper
	FROM ranges
), agg AS (
	SELECT left(cas_id,4) AS inventory, round(height_upper::numeric,0)::int AS height_upper, count(*)
	FROM casfri50.lyr_all
	WHERE casfri_metrics.isError(round(height_upper::numeric,0)::int) = FALSE
	GROUP BY inventory, round(height_upper::numeric,0)::int
)
SELECT s.inventory, s.height_upper, COALESCE(count, 0) AS count
FROM agg y
FULL JOIN series s ON s.inventory = y.inventory AND s.height_upper = y.height_upper
ORDER BY inventory, height_upper;

COMMENT ON MATERIALIZED VIEW height_upper_histogram IS
'height_upper_histogram aggregates the height_upper per inventory';



-- height_lower aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW IF NOT EXISTS height_lower_histogram AS
WITH ranges AS (
	SELECT foo.inventory, round(r.min_height_lower::numeric,0)::int AS min_height_lower, round(r.max_height_lower::numeric,0)::int AS max_height_lower
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_height_lower_per_inv r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_height_lower,0), COALESCE(max_height_lower,100)) height_lower
	FROM ranges
), agg AS (
	SELECT left(cas_id,4) AS inventory, round(height_lower::numeric,0)::int AS height_lower, count(*)
	FROM casfri50.lyr_all
	WHERE casfri_metrics.isError(round(height_lower::numeric,0)::int) = FALSE
	GROUP BY inventory, round(height_lower::numeric,0)::int
)
SELECT s.inventory, s.height_lower, COALESCE(count, 0) AS count
FROM agg y
FULL JOIN series s ON s.inventory = y.inventory AND s.height_lower = y.height_lower
ORDER BY inventory, height_lower;

COMMENT ON MATERIALIZED VIEW height_lower_histogram IS
'height_lower_histogram aggregates the height_lower per inventory';


-- Range of crown_closure_upper by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_crown_closure_upper_per_inv AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(crown_closure_upper::text) = false THEN crown_closure_upper ELSE NULL end) AS min_crown_closure_upper, 
max(CASE WHEN isError(crown_closure_upper::text) = false THEN crown_closure_upper ELSE NULL end) AS max_crown_closure_upper
FROM lyr_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_crown_closure_upper_per_inv IS
'range_of_crown_closure_upper_per_inv provides the range of recorded crown_closure_upper for each inventory';



-- Range of crown_closure_lower by inventory
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_crown_closure_lower_per_inv AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN isError(crown_closure_lower::text) = false THEN crown_closure_lower ELSE NULL end) AS min_crown_closure_lower, 
max(CASE WHEN isError(crown_closure_lower::text) = false THEN crown_closure_lower ELSE NULL end) AS max_crown_closure_lower
FROM lyr_all
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_crown_closure_lower_per_inv IS
'range_of_crown_closure_lower_per_inv provides the range of recorded crown_closure_lower for each inventory';



-- crown_closure_upper aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW IF NOT EXISTS crown_closure_upper_histogram AS
WITH ranges AS (
	SELECT foo.inventory, r.min_crown_closure_upper, r.max_crown_closure_upper
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_crown_closure_upper_per_inv r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_crown_closure_upper,0), COALESCE(max_crown_closure_upper,100)) crown_closure_upper
	FROM ranges
), agg AS (
	SELECT left(cas_id,4) AS inventory, crown_closure_upper, count(*)
	FROM casfri50.lyr_all
	WHERE casfri_metrics.isError(crown_closure_upper) = FALSE
	GROUP BY inventory, crown_closure_upper
)
SELECT s.inventory, s.crown_closure_upper, COALESCE(count, 0) AS count
FROM agg y
FULL JOIN series s ON s.inventory = y.inventory AND s.crown_closure_upper = y.crown_closure_upper
ORDER BY inventory, crown_closure_upper;

COMMENT ON MATERIALIZED VIEW crown_closure_upper_histogram IS
'crown_closure_upper_histogram aggregates the crown_closure_upper per inventory';


-- crown_closure_lower aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW IF NOT EXISTS crown_closure_lower_histogram AS
WITH ranges AS (
	SELECT foo.inventory, r.min_crown_closure_lower, r.max_crown_closure_lower
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_crown_closure_lower_per_inv r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_crown_closure_lower,0), COALESCE(max_crown_closure_lower,100)) crown_closure_lower
	FROM ranges
), agg AS (
	SELECT left(cas_id,4) AS inventory, crown_closure_lower, count(*)
	FROM casfri50.lyr_all
	WHERE casfri_metrics.isError(crown_closure_lower) = FALSE
	GROUP BY inventory, crown_closure_lower
)
SELECT s.inventory, s.crown_closure_lower, COALESCE(count, 0) AS count
FROM agg y
FULL JOIN series s ON s.inventory = y.inventory AND s.crown_closure_lower = y.crown_closure_lower
ORDER BY inventory, crown_closure_lower;

COMMENT ON MATERIALIZED VIEW crown_closure_lower_histogram IS
'crown_closure_lower_histogram aggregates the crown_closure_lower per inventory';


-- Stand ages for each forest stand
CREATE MATERIALIZED VIEW IF NOT EXISTS forest_layer_stand_ages AS
SELECT cas_id, layer, casfri_post_processing.getStandAge(cas_id, layer, origin_upper, 'casfri50', 'casfri_validation') AS stand_age
FROM casfri50.lyr_all
WHERE layer_rank IN (1, -8887);

COMMENT ON MATERIALIZED VIEW forest_layer_stand_ages IS
'forest_layer_stand_ages provides the age of each layer_rank 1 forest stand in lyr_all';


-- Range of stand_age by inventory 
CREATE MATERIALIZED VIEW IF NOT EXISTS range_of_stand_age_per_inv AS
SELECT left(cas_id, 4) AS inventory, 
min(CASE WHEN casfri_metrics.isError(stand_age::text) = FALSE THEN stand_age ELSE NULL END) AS min_stand_age, 
max(CASE WHEN casfri_metrics.isError(stand_age::text) = FALSE THEN stand_age ELSE NULL END) AS max_stand_age
FROM forest_layer_stand_ages
GROUP BY inventory;

COMMENT ON MATERIALIZED VIEW range_of_stand_age_per_inv IS
'range_of_stand_age_per_inv provides the range of recorded stand_age for each inventory';


-- stand_age aggregation to be displayed in grafana
CREATE MATERIALIZED VIEW IF NOT EXISTS stand_age_histogram AS
WITH ranges AS (
	SELECT foo.inventory, r.min_stand_age, r.max_stand_age
	FROM casfri_metrics.getListInvs() AS foo(inventory)
	INNER JOIN casfri_metrics.range_of_stand_age_per_inv r ON r.inventory = foo.inventory
), series AS (
	SELECT inventory, generate_series(COALESCE(min_stand_age,0), COALESCE(max_stand_age,900)) stand_age
	FROM ranges
), agg AS (
	SELECT left(cas_id,4) AS inventory, stand_age, count(*)
	FROM forest_layer_stand_ages
	WHERE casfri_metrics.isError(stand_age) = FALSE
	GROUP BY inventory, stand_age
)
SELECT s.inventory, s.stand_age, COALESCE(count, 0) AS count
FROM agg y
FULL JOIN series s ON s.inventory = y.inventory AND s.stand_age = y.stand_age
ORDER BY inventory, stand_age;

COMMENT ON MATERIALIZED VIEW stand_age_histogram IS
'stand_age_histogram aggregates the stand_age per inventory';
