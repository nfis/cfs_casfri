
-- Simple function to check if a text input is any of the casfri error codes. returns true if so, false otherwise.
CREATE OR REPLACE FUNCTION IsError(item text)
RETURNS boolean AS
	$func$
	BEGIN
		IF(
		$1 = 'EMPTY_STRING' OR 
		$1 = 'NULL_VALUE' OR 
		$1 = 'NOT_APPLICABLE' OR 
		$1 = 'UNKNOWN_VALUE' OR 
		$1 = 'OUT_OF_RANGE' OR 
		$1 = 'NOT_IN_SET' OR 
		$1 = 'INVALID_VALUE' OR 
		$1 = 'WRONG_TYPE' OR 
		$1 = 'INVALID_GEOMETRY' OR 
		$1 = 'NO_INTERSECT' OR 
		$1 = 'TRANSLATION_ERROR' OR 
		$1 = '-8888' OR 
		$1 = '-8887' OR 
		$1 = '-8886' OR 
		$1 = '-9999' OR 
		$1 = '-9998' OR 
		$1 = '-9997' OR 
		$1 = '-9995' OR 
		$1 = '-7779' OR 
		$1 = '-7778' OR 
		$1 = '-3333'
		)
			THEN return true;
		ELSE 
			return false;
		END IF;
	END;
	$func$
LANGUAGE 'plpgsql';



-- Simple function to check if a numeric input is any of the casfri error codes. returns true if so, false otherwise.
CREATE OR REPLACE FUNCTION IsError(item numeric)
RETURNS boolean AS
	$func$
	BEGIN
		IF(
		$1::text = 'EMPTY_STRING' OR 
		$1::text = 'NULL_VALUE' OR 
		$1::text = 'NOT_APPLICABLE' OR 
		$1::text = 'UNKNOWN_VALUE' OR 
		$1::text = 'OUT_OF_RANGE' OR 
		$1::text = 'NOT_IN_SET' OR 
		$1::text = 'INVALID_VALUE' OR 
		$1::text = 'WRONG_TYPE' OR 
		$1::text = 'INVALID_GEOMETRY' OR 
		$1::text = 'NO_INTERSECT' OR 
		$1::text = 'TRANSLATION_ERROR' OR 
		$1::text = '-8888' OR 
		$1::text = '-8887' OR 
		$1::text = '-8886' OR 
		$1::text = '-9999' OR 
		$1::text = '-9998' OR 
		$1::text = '-9997' OR 
		$1::text = '-9995' OR 
		$1::text = '-7779' OR 
		$1::text = '-7778' OR 
		$1::text = '-3333'
		)
			THEN return true;
		ELSE 
			return false;
		END IF;
	END;
	$func$
LANGUAGE 'plpgsql';


-- Displays the distinct values of each attribute in any table
CREATE OR REPLACE FUNCTION getDistinctValues(schema_name TEXT, table_name TEXT)
RETURNS TABLE (table_column TEXT, distinct_value TEXT[])
LANGUAGE 'plpgsql' AS
$func$
	BEGIN
		RETURN QUERY 
		 EXECUTE format('select KEY AS table_column, array_agg(distinct value) AS distinct_value
			from %s.%s, jsonb_each_text(to_jsonb(%s))
			group by key',
			schema_name, table_name, table_name);
	END;
$func$;


-- Returns a table of all inventories in casfri
-- BC09 is ignored since it contains no data
CREATE OR REPLACE FUNCTION getListInvs()
RETURNS TABLE (inventory VARCHAR)
LANGUAGE 'plpgsql' AS
$func$
	BEGIN
		RETURN QUERY 
		 EXECUTE format('select distinct inventory_id as inventory from hdr_all where inventory_id != ''BC09'' order by inventory');
	END;
$func$;


-- Given a dist_ext_lower and dist_ext_upper, will return the average of both values. 
-- If only one is an error, will return the non-error value. If both are errors, will return 100.
CREATE OR REPLACE FUNCTION getDistExt(dist_ext_upper NUMERIC, dist_ext_lower numeric)
RETURNS numeric AS
	$func$
	BEGIN
		IF(
			isError(dist_ext_upper::text) = TRUE AND isError(dist_ext_lower::text) = TRUE
		) THEN RETURN 100;
		ELSIF(
			isError(dist_ext_upper::text) = TRUE AND isError(dist_ext_lower::text) = FALSE
		)THEN RETURN dist_ext_lower;
		ELSIF(
			isError(dist_ext_upper::text) = FALSE AND isError(dist_ext_lower::text) = TRUE
		)THEN RETURN dist_ext_upper;
		ELSE RETURN (dist_ext_upper + dist_ext_lower)/2;
		END IF;
	END;
	$func$
LANGUAGE 'plpgsql';


-- Takes any of the non_error_*_attribute_counts materialized views and converts it into a table suitable for displaying on grafana.
-- This helps grafana group bar columns correctly and look the best. Only selects columns with the specified suffix.
-- Ignores columns starting with 'total'. Essentially it transposes the table. See example result:
--   
--              -> run attribute_counts_table_converter('schema_name', 'table_source_name', 'table_dest_name', 'area_ha') ->
-- 
-- schema_name.table_source_name:                               schema_name.table_dest_name:
-- inventory | cas_id_area_ha | cas_id_count | inventory_id_area_ha   |        attribute_name       | ab29_area_ha | ab30_area_ha  | bc08_area_ha  |
--------------------------------------------------------                        --------------------------------------------------------------------
--   AB29    |     12345      |    000000    |        67890           |        cas_id_area_ha       |    12345     |     45321     |    11111
--   AB30    |     45321      |    000000    |        56743           |        inventory_id_area_ha |    67890     |     56743     |    86300
--   BC08    |     11111      |    000000    |        86300           |
--
CREATE OR REPLACE FUNCTION attribute_counts_table_converter(schema_name TEXT, table_source_name TEXT, table_dest_name TEXT, suffix TEXT)
RETURNS void
LANGUAGE 'plpgsql' AS
$func$
	DECLARE 
		invs VARCHAR;
		attr VARCHAR;
		query VARCHAR := format('CREATE TABLE %s.%s ( attribute_name varchar(255), ', schema_name, table_dest_name);
		attributequery VARCHAR := format('SELECT table_column AS attribute_name FROM getdistinctvalues(''%s'', ''%s'') WHERE (RIGHT(table_column, 7) = ''%s'' or RIGHT(table_column, 5) = ''%s'') and left(table_column, 5) != ''total'';',
		schema_name, table_source_name, suffix, suffix);
	BEGIN
		-- Create table creation statement with each inventory as a column
		FOR invs IN SELECT * FROM getListInvs() LOOP
		 	query := query || invs || '_' || suffix || ' float, ';
		END LOOP;
		query := query || ' table_end varchar);';
		EXECUTE query;
		
		-- Insert each distinct attribute name into  the new table
		EXECUTE format('INSERT INTO %s.%s (attribute_name) SELECT table_column AS attribute_name FROM getdistinctvalues(''%s'', ''%s'') WHERE (RIGHT(table_column, 7) = ''%s'' or RIGHT(table_column, 5) = ''%s'') and left(table_column, 5) != ''total'';',
		schema_name, table_dest_name, schema_name, table_source_name, suffix, suffix);
		-- Iterate through each distinct inventory
		FOR invs IN SELECT * FROM getListInvs() LOOP 
			-- Iterate through each distinct attribute
			FOR attr IN EXECUTE attributequery LOOP 
				-- Update inventory's column at specific attribute's row with corresponding value from source table
				EXECUTE format('UPDATE %s.%s SET %s_%s = (SELECT %s FROM %s.%s WHERE inventory=''%s'') WHERE attribute_name = ''%s'';', 
				schema_name, table_dest_name, invs, suffix, attr, schema_name, table_source_name, invs, attr);
			END LOOP;
		END LOOP;
	END;
$func$;


-- Converts a duration in seconds into a text representation of the number of hours, minutes, and seconds
CREATE OR REPLACE FUNCTION prettyDuration(duration double precision)
RETURNS text 
LANGUAGE plpgsql AS 
$func$
  DECLARE
    hours integer;
    mins integer;
    secs integer;
  BEGIN
    hours := (duration::integer / (3600))::integer;
   	mins := (duration::integer / 60)::integer - (hours * 60)::integer;
    secs := duration - (60 * mins) - (3600 * hours);
    RETURN hours || ' hr, ' || mins || ' min, ' || secs || ' sec';
  END;
$func$;


-- Given a current and expected number of operations and a start time, makes a message detailing the estimated amount of time remaining to complete remaining operations
CREATE OR REPLACE FUNCTION getProgressMessage(current_num bigint, expected_num int, startTime timestamptz DEFAULT NULL)
RETURNS text 
LANGUAGE plpgsql AS 
$func$
DECLARE
	msg text = '';
	percentDone numeric;
	remainingTime double precision;
	elapsedTime double precision;
BEGIN
    percentDone = current_num::numeric/expected_num * 100;
    IF NOT startTime IS NULL THEN
      elapsedTime = EXTRACT(EPOCH FROM clock_timestamp() - startTime);
      remainingTime = ((100 - percentDone) * elapsedTime)/percentDone;
      msg = '(' || ROUND(percentDone,2) || '%) ' || prettyDuration(remainingTime) || ' remaining';
    END IF;
    RETURN msg;
END;
$func$;


-- Given a string to search in and a string to search for, counts the number of instances found within the string
CREATE OR REPLACE FUNCTION countInstances(search_in TEXT, search_for TEXT)
RETURNS INTEGER 
LANGUAGE plpgsql AS 
$func$
  BEGIN
    RETURN (CHAR_LENGTH(search_in) - CHAR_LENGTH(REPLACE(search_in, search_for, '')))  / CHAR_LENGTH(search_for);
  END;
$func$;
		
