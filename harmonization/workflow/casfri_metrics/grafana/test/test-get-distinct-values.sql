-- Tests getDistinctValues function to ensure that the correct result is returned.
-- Expected behaviour of the function:
-- Produces a table with a column containing each column name in the table, and a column containing a list of each distinct value in the corresponding column

CREATE TABLE IF NOT EXISTS casfri_metrics.get_distinct_test_table (
	col1 int,
	col2 varchar(255),
	col3 int
);

INSERT INTO casfri_metrics.get_distinct_test_table VALUES 
(1, 'test', '15'),
(2, 'test', '17'),
(3, 'test2', '15');

WITH get_distinct_test_result AS (
	SELECT * 
	FROM casfri_metrics.getDistinctValues('casfri_metrics', 'get_distinct_test_table')
	ORDER BY table_column
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (table_column = 'col1' AND distinct_value = array['1', '2', '3'])
		OR   (table_column = 'col2' AND distinct_value = array['test', 'test2'])
		OR   (table_column = 'col3' AND distinct_value = array['15', '17'])
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM get_distinct_test_result
)
SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are three distinct values for each column of the get_distinct_test_result
	AND (SELECT count(DISTINCT table_column) FROM get_distinct_test_result) = 3
	AND (SELECT count(DISTINCT distinct_value) FROM get_distinct_test_result) = 3
	-- Check that there are exactly 3 rows in the result
	AND (SELECT count(*) FROM get_distinct_test_result) = 3
	THEN 'Test passed' ELSE 'Test failed'
END AS get_distinct_test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_metrics.get_distinct_test_table;

