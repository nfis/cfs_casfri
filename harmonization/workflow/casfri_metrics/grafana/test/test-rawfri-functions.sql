-- Tests all functions in 6-rawfri-metrics.sql to ensure that the correct result is returned.
-- Run as script to see results of each test (you can also comment out the drop table at the end of the file to persist results)
-- Ensure that your search path is set with casfri_metrics first. Most table creations should default to casfri_metrics, but things may break if a different schema is used for this test

DROP TABLE IF EXISTS casfri_metrics.rawfri_test_results;
CREATE TABLE IF NOT EXISTS casfri_metrics.rawfri_test_results (test TEXT, result TEXT);
----------------------------------------------------------------------------------

-- Test for columnContainsNonNumerics(table_name TEXT, column_name TEXT, schema_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.columnContainsNonNumerics_test_table (
	col1 TEXT,
	col2 TEXT,
	col3 TEXT,
	col4 TEXT);

INSERT INTO columnContainsNonNumerics_test_table VALUES 
('1', '23', '34', NULL),
('13', '18', 'test', NULL),
('56', NULL, NULL, NULL),
('90', '17', '14', NULL);

WITH row_validity AS (
	SELECT columnContainsNonNumerics('columnContainsNonNumerics_test_table', 'col1', 'casfri_metrics') = false AS value
	UNION ALL
	SELECT columnContainsNonNumerics('columnContainsNonNumerics_test_table', 'col2', 'casfri_metrics') = false AS value
	UNION ALL
	SELECT columnContainsNonNumerics('columnContainsNonNumerics_test_table', 'col3', 'casfri_metrics') = true AS value
	UNION ALL
	SELECT columnContainsNonNumerics('columnContainsNonNumerics_test_table', 'col4', 'casfri_metrics') = true AS value
)
INSERT INTO rawfri_test_results SELECT 'columnContainsNonNumerics' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_metrics.columnContainsNonNumerics_test_table;

----------------------------------------------------------------------------------

-- Test for getNumericTextColumnNames(inventory TEXT, schema_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.getNumericTextColumnNames_test_table (
	col1 INT,
	col2 TEXT,
	col3 TEXT
);

INSERT INTO getNumericTextColumnNames_test_table VALUES 
(1, '14', '15'),
(2, '12', 'test'),
(3, NULL, NULL);


WITH getNumericTextColumnNames_test_result AS (
	SELECT * 
	FROM getNumericTextColumnNames('getNumericTextColumnNames_test_table', 'casfri_metrics')
	ORDER BY column_name
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN column_name = 'col2' AND data_type = 'text' AND ordinal_position = 2
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM getNumericTextColumnNames_test_result
)
INSERT INTO rawfri_test_results SELECT 'getNumericTextColumnNames' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 1 row in the result
	AND (SELECT count(*) FROM getNumericTextColumnNames_test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_metrics.getNumericTextColumnNames_test_table;

----------------------------------------------------------------------------------

-- Tests for getColumnNames(inventory TEXT, column_type TEXT, schema_name TEXT)
-- One test for each of the three column_type modes
CREATE TABLE IF NOT EXISTS casfri_metrics.getColumnNames_test_table (
	col1 INT,
	col2 VARCHAR(255),
	col3 NUMERIC,
	col4 TIMESTAMPTZ,
	col5 TEXT
);

INSERT INTO getColumnNames_test_table VALUES 
(1, 'test', 6.0, TO_TIMESTAMP('2019-04-06 11:20:21','YYYY-MM-DD HH:MI:SS'), '5');

SELECT * 
	FROM getColumnNames('getColumnNames_test_table', 'numeric', 'casfri_metrics')
	ORDER BY column_name;

-- Numeric mode
WITH getColumnNames_test_result AS (
	SELECT * 
	FROM getColumnNames('getColumnNames_test_table', 'numeric', 'casfri_metrics')
	ORDER BY column_name
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (column_name = 'col1' AND data_type = 'integer' AND ordinal_position = 1)
		OR (column_name = 'col3' AND data_type = 'numeric' AND ordinal_position = 3)
		OR (column_name = 'col5' AND data_type = 'text' AND ordinal_position = 5)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM getColumnNames_test_result
)
INSERT INTO rawfri_test_results SELECT 'getColumnNames Numeric' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 3 rows in the result
	AND (SELECT count(*) FROM getColumnNames_test_result) = 3
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

-- Text mode
WITH getColumnNames_test_result AS (
	SELECT * 
	FROM getColumnNames('getColumnNames_test_table', 'text', 'casfri_metrics')
	ORDER BY column_name
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (column_name = 'col2' AND data_type = 'character varying' AND ordinal_position = 2)
		OR (column_name = 'col5' AND data_type = 'text' AND ordinal_position = 5)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM getColumnNames_test_result
)
INSERT INTO rawfri_test_results SELECT 'getColumnNames Text' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 2 rows in the result
	AND (SELECT count(*) FROM getColumnNames_test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

-- Date mode
WITH getColumnNames_test_result AS (
	SELECT * 
	FROM getColumnNames('getColumnNames_test_table', 'date', 'casfri_metrics')
	ORDER BY column_name
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (column_name = 'col4' AND data_type = 'timestamp with time zone' AND ordinal_position = 4)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM getColumnNames_test_result
)
INSERT INTO rawfri_test_results SELECT 'getColumnNames Date' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 1 row in the result
	AND (SELECT count(*) FROM getColumnNames_test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

-- All mode
WITH getColumnNames_test_result AS (
	SELECT * 
	FROM getColumnNames('getColumnNames_test_table', 'all', 'casfri_metrics')
	ORDER BY column_name
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (column_name = 'col1' AND data_type = 'integer' AND ordinal_position = 1)
		OR (column_name = 'col2' AND data_type = 'character varying' AND ordinal_position = 2)
		OR (column_name = 'col3' AND data_type = 'numeric' AND ordinal_position = 3)
		OR (column_name = 'col4' AND data_type = 'timestamp with time zone' AND ordinal_position = 4)
		OR (column_name = 'col5' AND data_type = 'text' AND ordinal_position = 5)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM getColumnNames_test_result
)
INSERT INTO rawfri_test_results SELECT 'getColumnNames All' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 5 rows in the result
	AND (SELECT count(*) FROM getColumnNames_test_result) = 5
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_metrics.getColumnNames_test_table;

----------------------------------------------------------------------------------

-- Test for createColumnNamesList(target_table_name TEXT, schema_name TEXT, inventory_source TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.inv1 (
	col1 NUMERIC,
	col2 INTEGER,
	col3 TEXT,
	col4 VARCHAR(255)
);

INSERT INTO inv1 VALUES 
	(1, 2, '4', 'testy'),
	(9, 10, '7', 'tests'),
	(2, NULL, NULL, NULL),
	(NULL, 2, '100', '12');

CREATE TABLE IF NOT EXISTS casfri_metrics.inv2 (
	col5 VARCHAR(255),
	col6 TEXT
);

INSERT INTO inv2 VALUES 
	('12', NULL),
	('45', NULL),
	('78', NULL),
	(NULL, NULL);

CREATE TABLE IF NOT EXISTS casfri_metrics.inv3 (
	col7 VARCHAR(255)
);

INSERT INTO inv3 VALUES 
	('test');

SELECT * FROM createColumnNamesList('createColumnNamesList_test_result', 'casfri_metrics', '(VALUES (''inv1''), (''inv2''), (''inv3''))  AS foo(inventory)');

WITH row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory = 'inv1'  AND column_name = 'col1' AND data_type = 'numeric' AND ordinal_position = 1)
		OR (inventory = 'inv1'  AND column_name = 'col2' AND data_type = 'numeric' AND ordinal_position = 2)
		OR(inventory = 'inv1'  AND column_name = 'col3' AND data_type = 'numeric' AND ordinal_position = 3)
		OR(inventory = 'inv1'  AND column_name = 'col4' AND data_type = 'text' AND ordinal_position = 4)
		OR(inventory = 'inv2'  AND column_name = 'col5' AND data_type = 'numeric' AND ordinal_position = 1)
		OR(inventory = 'inv2'  AND column_name = 'col6' AND data_type = 'text' AND ordinal_position = 2)
		OR(inventory = 'inv3'  AND column_name = 'col7' AND data_type = 'text' AND ordinal_position = 1)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM createColumnNamesList_test_result
)
INSERT INTO rawfri_test_results SELECT 'createColumnNamesList' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 6 distinct rows in the result
	AND (SELECT count(*) FROM createColumnNamesList_test_result) = 7
	AND (SELECT count(DISTINCT column_name) FROM createColumnNamesList_test_result) = 7
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_metrics.inv1;
DROP TABLE IF EXISTS casfri_metrics.inv2;
DROP TABLE IF EXISTS casfri_metrics.inv3;
DROP TABLE IF EXISTS casfri_metrics.createColumnNamesList_test_result;

----------------------------------------------------------------------------------

-- Test for getNumericValsForColumn(col TEXT, table_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.getNumericValsForColumn_test_table (
	col1 INT
);

DELETE FROM getNumericValsForColumn_test_table;
INSERT INTO getNumericValsForColumn_test_table VALUES
	(1),
	(9),
	(2),
	(NULL);

WITH getNumericValsForColumn_test_result AS (
	SELECT * 
	FROM getNumericValsForColumn('col1', 'getNumericValsForColumn_test_table', 'casfri_metrics', 1)
	ORDER BY column_name
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory = 'getNumericValsForColumn_test_table'  AND column_name = 'col1' 
		AND min = 1 AND max = 9 AND avg = 4 AND median = 2 AND MODE = 1 AND stddev = 4.3588989435406736 AND ordinal_position = 1 AND null_count = 1 AND total_vals = 4)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM getNumericValsForColumn_test_result
)
INSERT INTO rawfri_test_results SELECT 'getNumericValsForColumn' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 1 row in the result
	AND (SELECT count(*) FROM getNumericValsForColumn_test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_metrics.getNumericValsForColumn_test_table;

----------------------------------------------------------------------------------

-- Test for getTextValsForColumn(col TEXT, table_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.getTextValsForColumn_test_table (
	col1 VARCHAR
);

DELETE FROM getTextValsForColumn_test_table;
INSERT INTO getTextValsForColumn_test_table VALUES
	('test1'),
	('test1'),
	('test2');

WITH getTextValsForColumn_test_result AS (
	SELECT * 
	FROM getTextValsForColumn('col1', 'getTextValsForColumn_test_table', 'casfri_metrics', 1)
	ORDER BY column_name
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory = 'getTextValsForColumn_test_table'  AND column_name = 'col1' AND value = 'test1' AND count = 2)
		OR (inventory = 'getTextValsForColumn_test_table'  AND column_name = 'col1' AND value = 'test2' AND count = 1 AND ordinal_position = 1)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM getTextValsForColumn_test_result
)
INSERT INTO rawfri_test_results SELECT 'getTextValsForColumn' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 1 row in the result
	AND (SELECT count(*) FROM getTextValsForColumn_test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_metrics.getTextValsForColumn_test_table;

----------------------------------------------------------------------------------

-- Test for getDateValsForColumn(col TEXT, table_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.getDateValsForColumn_test_table (
	col1 TIMESTAMPTZ
);

DELETE FROM getDateValsForColumn_test_table;
INSERT INTO getDateValsForColumn_test_table VALUES
	(TO_TIMESTAMP(
    '2017-03-31 9:30:20',
    'YYYY-MM-DD HH:MI:SS'
	)),
	(TO_TIMESTAMP(
    '2019-04-06 11:20:21',
    'YYYY-MM-DD HH:MI:SS'
	)),
	(TO_TIMESTAMP(
    '2024-10-13 3:32:23',
    'YYYY-MM-DD HH:MI:SS'
	));


WITH getDateValsForColumn_test_result AS (
	SELECT * 
	FROM getDateValsForColumn('col1', 'getDateValsForColumn_test_table', 'casfri_metrics', 1)
	ORDER BY column_name
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory = 'getDateValsForColumn_test_table'  AND column_name = 'col1' 
		AND min::TEXT = '2017-03-31' AND max::TEXT = '2024-10-13' AND avg::TEXT = '2020-06-06' AND median_year = 2019 AND mode_year = 2017  AND ordinal_position = 1)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM getDateValsForColumn_test_result
)
INSERT INTO rawfri_test_results SELECT 'getDateValsForColumn' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 1 row in the result
	AND (SELECT count(*) FROM getDateValsForColumn_test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_metrics.getDateValsForColumn_test_table;

----------------------------------------------------------------------------------

-- Test for generateRawNumericStats(inventory_source TEXT, result_table_name TEXT, schema_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.inv1 (
	col1 NUMERIC,
	col2 INTEGER
);

INSERT INTO inv1 VALUES 
	(1, 2),
	(9, 10),
	(2, NULL),
	(NULL, 2);

CREATE TABLE IF NOT EXISTS casfri_metrics.inv2 (
	col3 NUMERIC,
	col4 TEXT,
	col5 VARCHAR
);

INSERT INTO inv2 VALUES 
	(5, '12', 'test'),
	(4, '10', 'test'),
	(3, NULL, 'test'),
	(NULL, '3', 'test');

-- Generate column name source
SELECT * FROM createColumnNamesList('test_columns_list', 'casfri_metrics', '(VALUES (''inv1''), (''inv2''))  AS foo(inventory)');

SELECT * FROM generateRawNumericStats('(VALUES (''inv1''), (''inv2''))  AS foo(inventory)', 'generateRawNumericStats_test_result', 'casfri_metrics', 'test_columns_list');

WITH row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory = 'inv1'  AND column_name = 'col1' AND min = 1 AND max = 9 AND avg = 4 AND median = 2 AND mode = 1 AND stddev = 4.3588989435406736 AND ordinal_position = 1 AND null_count = 1 AND total_vals = 4)
		OR (inventory = 'inv1'  AND column_name = 'col2' AND min = 2 AND max = 10 AND avg = 4.6666666666666667 AND median = 2 AND mode = 2 AND stddev = 4.6188021535170061 AND ordinal_position = 2 AND null_count = 1 AND total_vals = 4)
		OR (inventory = 'inv2'  AND column_name = 'col3' AND min = 3 AND max = 5 AND avg = 4 AND median = 4 AND mode = 3 AND stddev = 1 AND ordinal_position = 1 AND null_count = 1 AND total_vals = 4)
		OR (inventory = 'inv2'  AND column_name = 'col4' AND min = 3 AND max = 12 AND avg = 8.3333333333333333 AND median = 10 AND mode = 3 AND stddev = 4.7258156262526084 AND ordinal_position = 2 AND null_count = 1 AND total_vals = 4)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM generateRawNumericStats_test_result
)
INSERT INTO rawfri_test_results SELECT 'generateRawNumericStats' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 4 distinct rows in the result
	AND (SELECT count(*) FROM generateRawNumericStats_test_result) = 4
	AND (SELECT count(DISTINCT column_name) FROM generateRawNumericStats_test_result) = 4
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_metrics.inv1;
DROP TABLE IF EXISTS casfri_metrics.inv2;
DROP TABLE IF EXISTS casfri_metrics.generateRawNumericStats_test_result;

-- Drop column source
DROP TABLE IF EXISTS casfri_metrics.test_columns_list;

----------------------------------------------------------------------------------

-- Test for generateRawTextStats(inventory_source TEXT, result_table_name TEXT, schema_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.inv1 (
	col1 TEXT,
	col2 TEXT
);

INSERT INTO inv1 VALUES 
	('test', 'testy'),
	('test2', 'testy');

CREATE TABLE IF NOT EXISTS casfri_metrics.inv2 (
	col3 TEXT,
	col4 TEXT,
	field_id VARCHAR(255), -- Should be in the lsit fo ignored COLUMNS
	col5 NUMERIC
);

INSERT INTO inv2 VALUES 
	('tester', 'testy', 'ignore_me', 1),
	(NULL, 'testy', 'ignore_me', 2);

-- Generate column name source
SELECT * FROM createColumnNamesList('test_columns_list', 'casfri_metrics', '(VALUES (''inv1''), (''inv2''))  AS foo(inventory)');

SELECT * FROM generateRawTextStats('(VALUES (''inv1''), (''inv2''))  AS foo(inventory)', 'generateRawTextStats_test_result', 'casfri_metrics', 'test_columns_list');

WITH row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory = 'inv1'  AND column_name = 'col1' AND value = 'test' AND count = 1 AND ordinal_position = 1)
		OR (inventory = 'inv1'  AND column_name = 'col1' AND value = 'test2' AND count = 1 AND ordinal_position = 1)
		OR (inventory = 'inv1'  AND column_name = 'col2' AND value = 'testy' AND count = 2 AND ordinal_position = 2)
		OR (inventory = 'inv2'  AND column_name = 'col3' AND value IS NULL AND count = 1 AND ordinal_position = 1)
		OR (inventory = 'inv2'  AND column_name = 'col3' AND value = 'tester' AND count = 1 AND ordinal_position = 1)
		OR (inventory = 'inv2'  AND column_name = 'col4' AND value = 'testy' AND count = 2 AND ordinal_position = 2)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM generateRawTextStats_test_result
)
INSERT INTO rawfri_test_results SELECT 'generateRawTextStats' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 6 rows in the result and the right amount of distinct values
	AND (SELECT count(*) FROM generateRawTextStats_test_result) = 6
	AND (SELECT count(DISTINCT inventory) FROM generateRawTextStats_test_result) = 2
	AND (SELECT count(DISTINCT column_name) FROM generateRawTextStats_test_result) = 4
	AND (SELECT count(DISTINCT value) FROM generateRawTextStats_test_result) = 4
	AND (SELECT count(DISTINCT count) FROM generateRawTextStats_test_result) = 2
	AND (SELECT count(DISTINCT ordinal_position) FROM generateRawTextStats_test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_metrics.inv1;
DROP TABLE IF EXISTS casfri_metrics.inv2;
DROP TABLE IF EXISTS casfri_metrics.generateRawTextStats_test_result;

-- Drop column source
DROP TABLE IF EXISTS casfri_metrics.test_columns_list;

----------------------------------------------------------------------------------

-- Test for generateRawDateStats(inventory_source TEXT, result_table_name TEXT, schema_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.inv1 (
	col1 TIMESTAMPTZ,
	col2 TIMESTAMPTZ
);

INSERT INTO inv1 VALUES 
	(TO_TIMESTAMP(
    '2017-03-31 9:30:20',
    'YYYY-MM-DD HH:MI:SS'
	), TO_TIMESTAMP(
    '2012-02-28 9:30:20',
    'YYYY-MM-DD HH:MI:SS'
	)),
	(TO_TIMESTAMP(
    '2019-04-06 11:20:21',
    'YYYY-MM-DD HH:MI:SS'
	), TO_TIMESTAMP(
    '2017-03-31 9:30:20',
    'YYYY-MM-DD HH:MI:SS'
	)),
	(TO_TIMESTAMP(
    '2024-10-13 3:32:23',
    'YYYY-MM-DD HH:MI:SS'
	), TO_TIMESTAMP(
    '2017-03-31 9:30:20',
    'YYYY-MM-DD HH:MI:SS'
	));

CREATE TABLE IF NOT EXISTS casfri_metrics.inv2 (
	col3 TIMESTAMPTZ,
	col4 TIMESTAMPTZ
);

INSERT INTO inv2 VALUES 
	(TO_TIMESTAMP(
    '2017-03-31 9:30:20',
    'YYYY-MM-DD HH:MI:SS'
	), TO_TIMESTAMP(
    '2012-11-30 9:30:20',
    'YYYY-MM-DD HH:MI:SS'
	)),
	(TO_TIMESTAMP(
    '2012-04-06 11:20:21',
    'YYYY-MM-DD HH:MI:SS'
	), TO_TIMESTAMP(
    '1980-03-30 9:12:20',
    'YYYY-MM-DD HH:MI:SS'
	)),
	(TO_TIMESTAMP(
    '2024-10-13 3:33:23',
    'YYYY-MM-DD HH:MI:SS'
	), TO_TIMESTAMP(
    '2019-09-09 9:30:20',
    'YYYY-MM-DD HH:MI:SS'
	));


SELECT * FROM generateRawDateStats('(VALUES (''inv1''), (''inv2''))  AS foo(inventory)', 'generateRawDateStats_test_result', 'casfri_metrics');

WITH row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory = 'inv1'  AND column_name = 'col1' AND min::TEXT = '2017-03-31' AND max::TEXT = '2024-10-13' AND avg::TEXT = '2020-06-06' AND median_year = 2019 AND mode_year = 2017 AND ordinal_position = 1)
		OR (inventory = 'inv1'  AND column_name = 'col2' AND min::TEXT = '2012-02-28' AND max::TEXT = '2017-03-31' AND avg::TEXT = '2015-07-21' AND median_year = 2017 AND mode_year = 2017 AND ordinal_position = 2)
		OR (inventory = 'inv2'  AND column_name = 'col3' AND min::TEXT = '2012-04-06' AND max::TEXT = '2024-10-13' AND avg::TEXT = '2018-02-05' AND median_year = 2017 AND mode_year = 2012 AND ordinal_position = 1)
		OR (inventory = 'inv2'  AND column_name = 'col4' AND min::TEXT = '1980-03-30' AND max::TEXT = '2019-09-09' AND avg::TEXT = '2004-04-13' AND median_year = 2012 AND mode_year = 1980 AND ordinal_position = 2)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM generateRawDateStats_test_result
)
INSERT INTO rawfri_test_results SELECT 'generateRawDateStats' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 4 distinct rows in the result
	AND (SELECT count(*) FROM generateRawDateStats_test_result) = 4
	AND (SELECT count(DISTINCT column_name) FROM generateRawDateStats_test_result) = 4
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_metrics.inv1;
DROP TABLE IF EXISTS casfri_metrics.inv2;
DROP TABLE IF EXISTS casfri_metrics.generateRawDateStats_test_result;

----------------------------------------------------------------------------------

-- Test for generateRawGeoStats(inventory_source TEXT, result_table_name TEXT, schema_name TEXT)
CREATE TABLE IF NOT EXISTS casfri_metrics.inv1 (
	wkb_geometry GEOMETRY
);

INSERT INTO inv1 VALUES 
	(ST_GeomFromText('POLYGON((1 1,2 3,2 1,1 1))')),
	(ST_GeomFromText('POLYGON((20 5,21 16,30 12, 20 5))'));

CREATE TABLE IF NOT EXISTS casfri_metrics.inv2 (
	wkb_geometry GEOMETRY
);

INSERT INTO inv2 VALUES 
	(ST_GeomFromText('POLYGON((3 3,6 9,6 2,3 3))')),
	(ST_GeomFromText('POLYGON((12 3,20 14,50 12, 12 3))'));


SELECT * FROM generateRawGeoStats('(VALUES (''inv1''), (''inv2''))  AS foo(inventory)', 'generateRawGeoStats_test_result', 'casfri_metrics');

WITH row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (inventory = 'inv1'  AND mean_area_m2 = 26.25 AND median_area_m2 = 26.25 AND mode_area_m2 = 1 AND mean_vertex_count = 4 AND median_vertex_count = 4 AND mode_vertex_count = 4
		AND mean_perimeter_m2 = 19.1684212061084 AND median_perimeter_m2 = 19.1684212061084 AND mode_perimeter_m2 = 5.23606797749979 AND mean_thinness_ratio = 0.524508286862011 AND median_thinness_ratio = 0.524508286862011 AND mode_thinness_ratio = 0.458352191003187)
		OR (inventory = 'inv2'  AND mean_area_m2 = 91.75 AND median_area_m2 = 91.75 AND mode_area_m2 = 10.5 AND mean_vertex_count = 4 AND median_vertex_count = 4 AND mode_vertex_count = 4
		AND mean_perimeter_m2 = 49.7948966188411 AND median_perimeter_m2 = 49.7948966188411 AND mode_perimeter_m2 = 16.8704815926677 AND mean_thinness_ratio = 0.3906595320813 AND median_thinness_ratio = 0.3906595320813 AND mode_thinness_ratio = 0.317718236736895)
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM generateRawGeoStats_test_result
)
INSERT INTO rawfri_test_results SELECT 'generateRawGeoStats' AS test,
	(CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is exactly 2 distinct rows in the result
	AND (SELECT count(*) FROM generateRawGeoStats_test_result) = 2
	AND (SELECT count(DISTINCT inventory) FROM generateRawGeoStats_test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_metrics.inv1;
DROP TABLE IF EXISTS casfri_metrics.inv2;
DROP TABLE IF EXISTS casfri_metrics.generateRawGeoStats_test_result;

----------------------------------------------------------------------------------

-- Display results
SELECT * FROM rawfri_test_results;
DROP TABLE IF EXISTS casfri_metrics.rawfri_test_results;

