SET search_path = casfri_post_processing, casfri_metrics, casfri50, casfri_web, rawfri, "translation", public;

-- Tests all functions in post-processing-fucntions.sql to ensure that the correct result is returned.
-- Run as script to see results of each test (you can also comment out the drop table at the end of the file to persist results)
-- Ensure that your search path is set with casfri_post_processing first. Most table creations should default to casfri_post_processing, but things may break if a different schema is used for this test

DROP TABLE IF EXISTS casfri_post_processing.validations_test_results;
CREATE TABLE IF NOT EXISTS casfri_post_processing.validations_test_results (test TEXT, result TEXT);
----------------------------------------------------------------------------------

-- Test for getStandAge(cas_id TEXT, layer INTEGER, origin_upper INTEGER, casfri_schema TEXT, validation_schema TEXT)
CREATE TABLE casfri_post_processing.lyr_all (
cas_id TEXT, 
layer INTEGER, 
origin_upper INTEGER
);

INSERT INTO casfri_post_processing.lyr_all VALUES 
('AB80-1', 1, 1990),
('AB80-1', 2, 2000);

CREATE TABLE casfri_post_processing.bc80_raw_lyr_all_joined (
cas_id TEXT, 
layer INTEGER, 
origin_upper INTEGER,
raw_projected_date TEXT,
raw_l1_proj_age_1 TEXT,
raw_l1_proj_age_class_cd_1 INTEGER
);

INSERT INTO casfri_post_processing.bc80_raw_lyr_all_joined VALUES 
('BC80-1', 1, 1990, '2020', '6', NULL),
('BC80-1', 2, 2000, '2020', NULL, 5);


WITH row_validity AS (
	SELECT getStandAge('AB80-1', 1, 1995, NULL, NULL) = date_part('year', current_date)::INTEGER - 1995 AS value
	UNION ALL
	SELECT getStandAge('AB80-1', 1, -8886, 'casfri_post_processing', 'casfri_post_processing') = date_part('year', current_date)::INTEGER - 2000 AS value
	UNION ALL
	SELECT getStandAge('BC80-1', 1, -8886, 'casfri_post_processing', 'casfri_post_processing') = date_part('year', current_date)::INTEGER - (2020 - 6) AS value
	UNION ALL
	SELECT getStandAge('BC80-1', 2, -8886, 'casfri_post_processing', 'casfri_post_processing') = date_part('year', current_date)::INTEGER - (2020 - 91) AS value
)
INSERT INTO validations_test_results SELECT 'getStandAge' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_post_processing.lyr_all;
DROP TABLE IF EXISTS casfri_post_processing.bc80_raw_lyr_all_joined;

----------------------------------------------------------------------------------

-- Display results
SELECT * FROM casfri_post_processing.validations_test_results;
DROP TABLE IF EXISTS casfri_post_processing.validations_test_results;

