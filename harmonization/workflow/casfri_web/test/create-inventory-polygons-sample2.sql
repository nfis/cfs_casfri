DROP MATERIALIZED VIEW casfri.inventories_sample;
CREATE MATERIALIZED VIEW casfri.inventories_sample AS
SELECT 
    LEFT(inventory_id, 2) AS jurisdiction,
    STRING_AGG(inventory_id, ', ' ORDER BY inventory_id) AS inventories,
    MAX(stand_photo_year) AS stand_photo_year,
    ST_ExteriorRing((ST_Dump(ST_Collect(geometry))).geom) as geometry
FROM 
    -- Get latest inventories for each jurisdiction
(   SELECT distinct on (jurisdiction) *
    FROM (
        SELECT LEFT(inventory_id,2) AS jurisdiction,
               inventory_id,
               stand_photo_year,
               geometry
        FROM casfri.cas_flat_all_layers_same_row
    ) a
    ORDER BY jurisdiction, stand_photo_year DESC NULLS FIRST
) h
GROUP BY LEFT(inventory_id, 2);



