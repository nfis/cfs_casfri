CREATE SCHEMA IF NOT EXISTS casfri_post_processing;

CREATE TABLE casfri_post_processing.nfl_latest_layer AS
SELECT n.*, g.geometry from casfri_web.nfl_latest n, casfri50.geo_all g
WHERE n.cas_id = g.cas_id;

CREATE INDEX ON casfri_post_processing.nfl_latest_layer USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_post_processing.nfl_latest_layer USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_post_processing.nfl_latest_layer USING gist(geometry);