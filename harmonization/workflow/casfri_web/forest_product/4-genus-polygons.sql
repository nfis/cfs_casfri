DROP TABLE IF EXISTS casfri_web.vi_genus_polygons;

CREATE UNLOGGED TABLE casfri_web.vi_genus_polygons AS (
  WITH all_clustered AS (
      SELECT
        genus,
        ST_ClusterKMeans(geometry,2000) over (PARTITION BY genus) AS geometry_cid,
        geometry
      FROM
        casfri_web.vi_forest_sp_genus)

     SELECT
       row_number() OVER () AS id,
       genus,
       ST_Union(geometry) as geometry
     FROM all_clustered
     GROUP BY geometry_cid, genus
);

ALTER TABLE casfri_web.vi_genus_polygons
ADD CONSTRAINT vi_genus_polygons_pk PRIMARY KEY (id);

CREATE INDEX vi_genus_polygons_geom_idx
ON casfri_web.vi_genus_polygons USING gist(geometry);

CREATE INDEX vi_genus_polygons_genus_idx
ON casfri_web.vi_genus_polygons USING btree(genus);

