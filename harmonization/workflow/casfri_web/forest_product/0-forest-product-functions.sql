-- getStandAge(cas_id TEXT, layer INTEGER, origin_upper INTEGER, casfri_schema TEXT, validation_schema TEXT)
-- Given a cas_id, layer, origin, and schemas, finds the age of the stand
-- If origin is not an error, will use that. Otherwise, attempts to use raw attributes to find origin. Returns -8886 if a stand age couldn't be found for that cas_id
-- Note that this function will return the same results as updateStandAge, however, it is typically significantly more efficient to use updateStandAges to update ages in an entire table, rather than using this function for each row
-- Input:
	-- cas_id: the cas_id to check
	-- layer: the specific layer to check
	-- origin_upper: the existing origin_upper value
	-- casfri_schema: the schema the casfri tables are found in (typically 'casfri50')
	-- validation_schema: the schema the raw joined tables are found in (typically 'casfri_validation')
-- Output: INTEGER 
	-- The age of the forest stand
CREATE OR REPLACE FUNCTION getStandAge(cas_id TEXT, layer INTEGER, origin_upper INTEGER, casfri_schema TEXT, validation_schema TEXT)
RETURNS INTEGER AS
	$func$
	DECLARE 
	   val INTEGER;
	   raw_projected_date TEXT;
	   raw_l1_proj_age_1 TEXT;
	   raw_l1_proj_age_class_cd_1 TEXT;
	   raw_year_org TEXT;
	   raw_origin TEXT;
	   raw_origin_sum TEXT;
	   raw_l1estabyr TEXT;
	   raw_photoyr TEXT;
	   raw_age TEXT;
	   raw_oyrorg TEXT;
	   raw_yrsource TEXT;
	   raw_photo_year TEXT;
	   age_var INTEGER;
	BEGIN
		IF isError(origin_upper) = FALSE
		THEN
			RETURN date_part('year', current_date) - origin_upper;
		-- AB will attempt to use the understory origin instead
		ELSIF left(cas_id,2) = 'AB'
		THEN
			EXECUTE format('SELECT origin_upper::INTEGER FROM %s.lyr_all WHERE cas_id = ''%s'' AND layer != %s LIMIT 1', casfri_schema, cas_id, layer) INTO val;
			-- Sub in error code if the val couldn't be determined. Otherwise, subtract origin from current year
			IF val IS NULL OR isError(val::TEXT) = TRUE
			THEN 
				RETURN -8886;
			ELSE
				RETURN date_part('year', current_date)::INTEGER - val;
			END IF;
		-- BC will attempt to use the projected_date and projected_age, and if that doesn't work, will use the age class
		ELSIF left(cas_id,2) = 'BC'
		THEN
			EXECUTE format('SELECT COALESCE(raw_projected_date::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_projected_date;
			EXECUTE format('SELECT COALESCE(raw_l1_proj_age_1::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_l1_proj_age_1;
			IF isError(raw_projected_date) = FALSE AND isError(raw_l1_proj_age_1) = FALSE
			THEN 	
				RETURN date_part('year', current_date)::INTEGER - (LEFT(raw_projected_date,4)::INTEGER - raw_l1_proj_age_1::INTEGER);
			ELSE
				EXECUTE format('SELECT COALESCE(raw_l1_proj_age_class_cd_1::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_l1_proj_age_class_cd_1;
				IF isError(raw_projected_date) = FALSE AND isError(raw_l1_proj_age_class_cd_1) = FALSE AND raw_l1_proj_age_class_cd_1::INTEGER >= 0 AND raw_l1_proj_age_class_cd_1::INTEGER <= 9
				THEN
					RETURN date_part('year', current_date)::INTEGER - (LEFT(raw_projected_date,4)::INTEGER - (CASE raw_l1_proj_age_class_cd_1::INTEGER
						WHEN 0 THEN 0
						WHEN 1 THEN 11
						WHEN 2 THEN 31
						WHEN 3 THEN 51
						WHEN 4 THEN 71
						WHEN 5 THEN 91
						WHEN 6 THEN 111
						WHEN 7 THEN 131
						WHEN 8 THEN 196
						WHEN 9 THEN 300 END));	
				END IF;
			END IF;
		-- MB FRI will try to use year_org
		ELSIF left(cas_id,4) IN ('MB03', 'MB11', 'MB13')
		THEN
			EXECUTE format('SELECT COALESCE(raw_year_org::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_year_org;
			IF isError(raw_year_org) = FALSE
			THEN
				RETURN date_part('year', current_date)::INTEGER - raw_year_org::INTEGER;
			END IF;
		-- MB FLI will attempt to use origin, then origin_sum, then age_2021, age_2012, or age_2016 depending on the inventory
		ELSIF left(cas_id,4) IN ('MB06', 'MB07', 'MB10', 'MB12')
		THEN
			EXECUTE format('SELECT COALESCE(raw_origin::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_origin;
			IF isError(raw_origin) = FALSE
			THEN
				RETURN date_part('year', current_date)::INTEGER - raw_origin::INTEGER;
			ELSE
				EXECUTE format('SELECT COALESCE(raw_origin_sum::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_origin_sum;
				IF isError(raw_origin_sum) = FALSE
				THEN
					RETURN date_part('year', current_date)::INTEGER - raw_origin_sum::INTEGER;
				ELSE
					age_var := CASE left(cas_id,4) 
						WHEN 'MB06' THEN 2021
						WHEN 'MB07' THEN 2012
						WHEN 'MB10' THEN 2016
						WHEN 'MB12' THEN 2012 END;
					EXECUTE format('SELECT COALESCE(raw_age_%s::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', age_var, validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_age;
					IF isError(raw_age) = FALSE
					THEN
						RETURN date_part('year', current_date)::INTEGER - (age_var - raw_age::INTEGER);
					END IF;
				END IF;
			END IF;
		-- NB will attempt to use a non-zero l1estabyr
		ELSIF left(cas_id,2) = 'NB'
		THEN
			EXECUTE format('SELECT COALESCE(raw_l1estabyr::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_l1estabyr;
			IF isError(raw_l1estabyr) = FALSE AND raw_l1estabyr::INTEGER > 0
			THEN
				RETURN date_part('year', current_date)::INTEGER - raw_l1estabyr::INTEGER;
			END IF;
		-- NT will attempt to use origin
		ELSIF left(cas_id,2) = 'NT'
		THEN
			EXECUTE format('SELECT COALESCE(raw_origin::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_origin;
			IF isError(raw_origin) = FALSE AND raw_origin::INTEGER > 0
			THEN
				RETURN date_part('year', current_date)::INTEGER - raw_origin::INTEGER;
			END IF;
		-- NS will attempt to use photoyr
		ELSIF left(cas_id,2) = 'NS'
		THEN
			EXECUTE format('SELECT COALESCE(raw_photoyr::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_photoyr;
			IF isError(raw_photoyr) = FALSE
			THEN
				RETURN date_part('year', current_date)::INTEGER - raw_photoyr::INTEGER;
			END IF;
		-- ON will attempt to use a non-zero oyrorg, otherwise will try yrsource
		ELSIF left(cas_id,2) = 'ON'
		THEN 
			EXECUTE format('SELECT COALESCE(raw_oyrorg::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_oyrorg;
			IF isError(raw_oyrorg) = FALSE AND raw_oyrorg::INTEGER > 0
			THEN
				RETURN date_part('year', current_date)::INTEGER - raw_oyrorg::INTEGER;
			ELSE
				EXECUTE format('SELECT COALESCE(raw_yrsource::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_yrsource;
				IF isError(raw_yrsource) = FALSE AND raw_yrsource::INTEGER > 0
				THEN
					RETURN date_part('year', current_date)::INTEGER - raw_yrsource::INTEGER;
				END IF;
			END IF;
		-- YT04 will try photo_year and age
		ELSIF left(cas_id,4) = 'YT04'
		THEN
			EXECUTE format('SELECT COALESCE(raw_photo_year::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_photo_year;
			EXECUTE format('SELECT COALESCE(raw_age::TEXT, ''-8886'') FROM %s.%s_raw_lyr_all_joined WHERE cas_id = ''%s'' AND layer = %s LIMIT 1', validation_schema, LEFT(cas_id,4), cas_id, layer) INTO raw_age;
			IF isError(raw_photo_year) = FALSE AND isError(raw_age) = FALSE
			THEN
				RETURN date_part('year', current_date)::INTEGER - (raw_photo_year::INTEGER - raw_age::INTEGER);
			END IF;
		END IF;
		RETURN -8886;
	END;
	$func$
LANGUAGE 'plpgsql';
