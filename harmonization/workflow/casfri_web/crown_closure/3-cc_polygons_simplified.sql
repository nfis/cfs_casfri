DROP TABLE IF EXISTS casfri_web.cc_polygons_simplified;
CREATE UNLOGGED TABLE casfri_web.cc_polygons_simplified AS
  SELECT id,
  crown_closure_classes,
  ST_Simplify(geometry, 400) AS geometry
  FROM casfri_web.crownclosure_polygons;

ALTER TABLE casfri_web.cc_polygons_simplified
ADD CONSTRAINT cc_polygons_simplified_pk PRIMARY KEY (id);

CREATE INDEX cc_polygons_simplified_geom_idx
ON casfri_web.cc_polygons_simplified USING gist(geometry);

CREATE INDEX cc_polygons_simplified_cc_idx
ON casfri_web.cc_polygons_simplified USING btree(crown_closure_classes);
