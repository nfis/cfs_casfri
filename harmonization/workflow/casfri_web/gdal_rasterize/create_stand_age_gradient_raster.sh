# Run script to generate a GeoTif of the stand_age data product, as a gradient

#Initialize variables
source common.sh

DATE=$(date '+%Y-%m-%d')
SCHEMA="casfri_post_processing"
TABLE="casfri_forest_layer"
ATTRIBUTE="stand_age"
ATT_SHORT="stand_age"
OUTPUT="stand_age_gradient"
OUTPUT_PATH="${RASTER_FILES_LOCATION}/${OUTPUT}_${DATE}"

mkdir -p $OUTPUT_PATH

echo "Creating raster from table in database"

gdal_rasterize \
-a $ATT_SHORT \
-sql "SELECT (CASE 
WHEN $ATTRIBUTE::INTEGER < 0 THEN 0 
ELSE $ATTRIBUTE::INTEGER END) AS $ATT_SHORT, geometry FROM $SCHEMA.$TABLE" \
-tr 30 30 \
-a_nodata 0 \
-ot Int16 \
-of GTiff \
-co TILED=YES \
-co BLOCKXSIZE=128 \
-co BLOCKYSIZE=128 \
-co COMPRESS=LZW \
-co INTERLEAVE=BAND \
-co TFW=YES \
"PG:dbname='$DB_NAME' host='$DB_HOST' user='$DB_USER' port='$DB_PORT' password='$DB_PASSWORD'" \
$OUTPUT_PATH/${OUTPUT}_unprojected.tif

echo "Reprojecting"

gdalwarp -t_srs ESRI:102001 $OUTPUT_PATH/${OUTPUT}_unprojected.tif $OUTPUT_PATH/${OUTPUT}.tif

echo "Adding pyramids"

gdaladdo \
$OUTPUT_PATH/${OUTPUT}.tif \
-ro 2 4 8 16 32 64 128 256

echo "Generating auxillary files"

gdalinfo ${OUTPUT_PATH}/${OUTPUT}.tif -mm -stats >/dev/null
gdalinfo ${OUTPUT_PATH}/${OUTPUT}.tif.ovr -mm -stats >/dev/null

echo "Copying style files"

cp styling_files/${OUTPUT}.qml ${OUTPUT_PATH}
cp styling_files/${OUTPUT}.sld ${OUTPUT_PATH}

echo "Zipping"

tar -czvf ${RASTER_FILES_LOCATION}/${OUTPUT}_${DATE}.tar.gz -C ${RASTER_FILES_LOCATION} ${OUTPUT}_${DATE}
