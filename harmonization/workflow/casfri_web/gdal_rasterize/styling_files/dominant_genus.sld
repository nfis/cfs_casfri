<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" version="1.0.0">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>dominant_genus_plain</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="values">
              <sld:ColorMapEntry quantity="1" label="Spruce (Picea)" color="#4c7300"/>
              <sld:ColorMapEntry quantity="2" label="Firs (Abies)" color="#134145"/>
              <sld:ColorMapEntry quantity="3" label="Cedars (Thuja)" color="#4b909c"/>
              <sld:ColorMapEntry quantity="4" label="Pine (Pinus)" color="#19bf9e"/>
              <sld:ColorMapEntry quantity="5" label="Hemlocks (Tsuga)" color="#79a9b1"/>
              <sld:ColorMapEntry quantity="6" label="Douglas-Fir (Pseudotsuga)" color="#740000"/>
              <sld:ColorMapEntry quantity="7" label="Poplars (Populus)" color="#c186bb"/>
              <sld:ColorMapEntry quantity="8" label="Maples (Acer)" color="#d87285"/>
              <sld:ColorMapEntry quantity="9" label="Birches (Betula)" color="#994b87"/>
              <sld:ColorMapEntry quantity="10" label="Tamaracks (Larix)" color="#dc9800"/>
              <sld:ColorMapEntry quantity="11" label="Other Genus" color="#a8a800"/>
              <sld:ColorMapEntry quantity="12" label="Unknown Genus" color="#ffff73"/>
            </sld:ColorMap>
            <sld:VendorOption name="brightness">0.511765</sld:VendorOption>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
