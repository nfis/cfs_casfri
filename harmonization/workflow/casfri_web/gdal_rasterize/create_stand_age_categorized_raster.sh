# Run script to generate a GeoTif of the stand_age data product, categorized by age classes

#Initialize variables
source common.sh

DATE=$(date '+%Y-%m-%d')
SCHEMA="casfri_post_processing"
TABLE="casfri_forest_layer"
ATTRIBUTE="stand_age"
ATT_SHORT="stand_age"
OUTPUT="stand_age_categorized"
OUTPUT_PATH="${RASTER_FILES_LOCATION}/${OUTPUT}_${DATE}"

mkdir -p $OUTPUT_PATH

echo "Creating raster from table in database"

gdal_rasterize \
-a $ATT_SHORT \
-sql "SELECT (CASE 
WHEN $ATTRIBUTE::INTEGER >= 1 AND $ATTRIBUTE::INTEGER <= 20 THEN 1 
WHEN $ATTRIBUTE::INTEGER >= 21 AND $ATTRIBUTE::INTEGER <= 40 THEN 2 
WHEN $ATTRIBUTE::INTEGER >= 41 AND $ATTRIBUTE::INTEGER <= 60 THEN 3 
WHEN $ATTRIBUTE::INTEGER >= 61 AND $ATTRIBUTE::INTEGER <= 80 THEN 4 
WHEN $ATTRIBUTE::INTEGER >= 81 AND $ATTRIBUTE::INTEGER <= 100 THEN 5 
WHEN $ATTRIBUTE::INTEGER >= 101 AND $ATTRIBUTE::INTEGER <= 120 THEN 6 
WHEN $ATTRIBUTE::INTEGER >= 121 AND $ATTRIBUTE::INTEGER <= 140 THEN 7 
WHEN $ATTRIBUTE::INTEGER >= 141 AND $ATTRIBUTE::INTEGER <= 250 THEN 8 
WHEN $ATTRIBUTE::INTEGER >= 251 THEN 9
ELSE 0 END) AS $ATT_SHORT, geometry FROM $SCHEMA.$TABLE" \
-tr 30 30 \
-a_nodata 0 \
-ot Byte \
-of GTiff \
-co TILED=YES \
-co BLOCKXSIZE=128 \
-co BLOCKYSIZE=128 \
-co COMPRESS=LZW \
-co INTERLEAVE=BAND \
-co NBITS=4 \
-co TFW=YES \
"PG:dbname='$DB_NAME' host='$DB_HOST' user='$DB_USER' port='$DB_PORT' password='$DB_PASSWORD'" \
$OUTPUT_PATH/${OUTPUT}_unprojected.tif

echo "Reprojecting"

gdalwarp -t_srs ESRI:102001 $OUTPUT_PATH/${OUTPUT}_unprojected.tif $OUTPUT_PATH/${OUTPUT}.tif

echo "Adding pyramids"

gdaladdo \
$OUTPUT_PATH/${OUTPUT}.tif \
-ro 2 4 8 16 32 64 128 256

echo "Generating auxillary files"

gdalinfo ${OUTPUT_PATH}/${OUTPUT}.tif -mm -stats >/dev/null
gdalinfo ${OUTPUT_PATH}/${OUTPUT}.tif.ovr -mm -stats >/dev/null

echo "Copying style files"

cp styling_files/${OUTPUT}.qml ${OUTPUT_PATH}
cp styling_files/${OUTPUT}.sld ${OUTPUT_PATH}

echo "Zipping"

tar -czvf ${RASTER_FILES_LOCATION}/${OUTPUT}_${DATE}.tar.gz -C ${RASTER_FILES_LOCATION} ${OUTPUT}_${DATE}
