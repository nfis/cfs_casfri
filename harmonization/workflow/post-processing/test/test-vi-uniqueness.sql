CREATE TABLE casfri_web.vi_unique_deslivered AS (
    WITH distinct_rows as (
        SELECT DISTINCT cas_id from casfri_web.vi_forest_sp_genus
    ),

    missing as (
        SELECT cas_id from casfri_web.vi_forest_sp_genus f
        WHERE NOT EXISTS (
            SELECT cas_id
            FROM distinct_rows d
            WHERE d.cas_id = f.cas_id
        )
    )
    SELECT * FROM casfri_web.vi_forest_sp_genus
    WHERE ST_AREA(geometry) > 1 and (cas_id in distinct_rows OR cas_id in missing);
);

COMMENT ON TABLE casfri_web.vi_unique_gh_test IS
'Table to test if gh unique query is properly de-duplicating gh records';

CREATE INDEX ON casfri_web.vi_unique_deslivered USING btree(left(cas_id, 2));
CREATE INDEX ON casfri_web.vi_unique_deslivered USING btree(left(cas_id, 4));
CREATE INDEX ON casfri_web.vi_unique_deslivered USING gist(geometry);