Create Table casfri_post_processing.vi_unique as (
SELECT cas_id,
    ST_Multi(ST_Union(geometry)) As geometry
FROM casfri_web.vi_forest_sp_genus
GROUP BY cas_id
)