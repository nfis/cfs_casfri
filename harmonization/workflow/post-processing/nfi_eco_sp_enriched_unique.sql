/*

This is a working script to  create enhanced nfis eco species list enriched with Species_Type column for broadleaf/conifer.

Written by: Kangakola Omendja

Created: November 12, 2022

Last Modified Date: November 13, 2022

*/


/* create gen_sp_susp variable throught concatenation of ec_genus, ec_species, ec_subspecies and replace null value occurrence with '###'*/

-- nfi_ecological_species_list can be pulled from xnetwork if needed
DROP TABLE IF EXISTS casfri_post_processing.nfi_eco_species_list_enriched;
CREATE TABLE casfri_post_processing.nfi_eco_species_list_enriched AS
SELECT *, 
        CASE WHEN  ec_subspecies IS  NOT NULL 
			 THEN  concat_ws('_', ec_genus, ec_species, ec_subspecies) 
			 WHEN ec_subspecies IS  NULL  
			 THEN  concat_ws('_', ec_genus, ec_species, '###') 
			 WHEN  ec_species IS NULL  AND ec_subspecies IS  NULL 
			 THEN  concat_ws('_', ec_genus, '###', '###') 
		END as gen_sp_susp
FROM casfri50_test.nfi_ecological_species_list;


/* create a new table with unique occurrence of gen_sp_susp*/


DROP TABLE IF EXISTS casfri_post_processing.nfi_eco_species_list_enriched_unique;
CREATE TABLE casfri_post_processing.nfi_eco_species_list_enriched_unique AS
SELECT distinct on (gen_sp_susp)
       *
FROM  casfri_post_processing.nfi_eco_species_list_enriched;


/*create the variable Species_Type: that store two values: Conifers & Broadleaf*/

/*
conifer_list = (ABIE, CEDR,CHAM, GENC, JUNI, LARI, PICE, PINU, PSEU, TAXU, THUJ, TSUG)
broadleaf_list = (ACER,AESC,AILA,ALNU,AMEL,ARBU,ASIM,ARAL, ARAU,BETU,CALO,CARA,CARP,CARY,CAST,CATA,CELT,CEPH,CERC,CLAD,CORN,ELAE,EUON,FAGU,FRAN,FRAX,GENH,
                         GINK,GLED,GYMN,HAMA,ILEX,JUGL,LIRI,MAGN,MALU,MORU,MYRI,NEMO,NYSS,OSTR,PLAT,POPU,PRUN,PTEL,PYRU,QUER,RHAM,RHUS,ROBI,SALI,SAMB,SASS,SEQU,
						  SHEP,SORB,SYRI,TILI,TOXI,ULMU,VIBU,ZANT)
*/
									
DROP TABLE IF EXISTS casfri_post_processing.nfi_eco_species_list_enriched_unique_1;
CREATE TABLE casfri_post_processing.nfi_eco_species_list_enriched_unique_1 AS
SELECT *,
     CASE WHEN ec_genus IN ('ABIE', 'CEDR','CHAM', 'GENC', 'JUNI', 'LARI', 'PICE', 'PINU', 'PSEU', 'TAXU', 'THUJ', 'TSUG')
		  THEN  'Conifers'
		  WHEN  ec_genus IN ('ACER','AESC','AILA','ALNU','AMEL','ARBU','ASIM','ARAL', 'ARAU','BETU','CALO','CARA','CARP','CARY','CAST','CATA','CELT','CEPH','CERC','CLAD','CORN','ELAE','EUON','FAGU','FRAN','FRAX','GENH',
                             'GINK','GLED','GYMN','HAMA','ILEX','JUGL','LIRI','MAGN','MALU','MORU','MYRI','NEMO','NYSS','OSTR','PLAT','POPU','PRUN','PTEL','PYRU','QUER','RHAM','RHUS','ROBI','SALI','SAMB','SASS','SEQU',
						     'SHEP','SORB','SYRI','TILI','TOXI','ULMU','VIBU','ZANT')
		  THEN 'Broadleaf'
	 END as Species_Type
FROM casfri_post_processing.nfi_eco_species_list_enriched_unique;

-- Clean up intermediate tables
DROP TABLE IF EXISTS casfri_post_processing.nfi_eco_species_list_enriched_unique;
DROP TABLE IF EXISTS casfri_post_processing.nfi_eco_species_list_enriched;



