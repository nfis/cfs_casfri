# CONVERSIONS

For storing conversion scripts not provided by the CASFRI team.

- load_ab29.sh & load_ab30.sh
   For loading crown_avi_jan2022.gpkg data from Alberta, received as a GDB file
   and converted with ogr2ogr to gpkg format so the load scripts can add a poly_id
   column

- load_on02.sh
  For loading INV2019.gdb data from Ontario.  Minor changes from the
  CASFRI version (featureclasses named differently, some fields 
  named differently, some fields do not exist).

- load_ns03.sh
  For loading NS03 data.  Only the pathing (to the shapefiles)  & the
  name of the feature is different.

- load_nb02.sh
  For loading NB02 data.  Only the names of the files are different.
