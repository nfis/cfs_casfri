#!/bin/bash -x

# This script loads the AB GOV crown and dataset.
# This file was received along with the AB30 inventory data.
# It contains data for AB29 and AB30 inventories

# The format of the source dataset is a table in a geopackage, converted from geodatabase recieved from AB.
# Converted to GPKG so sqlite commands can be run for AB30, since openFileGDB is limited to Read-Only for GDB format
# The data is stored in the AB30 folder.



# Year of photography is unknown.

# Load into a target table in the schema defined in the config file.

# If the table already exists, it can be overwritten by setting the "overwriteFRI" variable
# in the configuration file.

######################################## Set variables #######################################

source ./common.sh

inventoryID=AB29

srcFileName=crown_avi_jan2022
srcFullPath="$friDir/AB/AB29/data/inventory/$srcFileName.gpkg"
gdbTableName=AVI

AB_subFolder="$friDir/AB/AB29/data/inventory/"
fullTargetTableName=$targetFRISchema.ab29

########################################## Process ######################################


"$gdalFolder/ogr2ogr" \
-f PostgreSQL "$pg_connection_string" "$srcFullPath" "$gdbTableName" \
-nln $fullTargetTableName $layer_creation_options $other_options \
-sql "SELECT *, '$srcFileName' AS src_filename, '$inventoryID' AS inventory_id, 2022 AS acquisition_year FROM $gdbTableName" \
-progress $overwrite_tab

source ./common_postprocessing.sh