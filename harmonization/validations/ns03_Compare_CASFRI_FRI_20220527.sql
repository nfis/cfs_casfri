

/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: April 01, 2022 by Kangakola
Last Modified Date: May 27, 2022 by Kangakola

*/

/*
=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate CASFRI translation of various FRI of Juridiction forest inventory datata. 
	*** This is proceed by comparing the attributes values of CASFRI translate data as per CASFRI SCHEMAS to the attributes of inventory sources data. 
	     To assess if the translation was accurate and the translation rule was followed and the resulted translated data are similar to the original data as per translation rules.
	*** Update: Integration of crownclosurecompare, heightcompare and siteclasscompare code. Also, add code for validation of dst, eco and nfl tables.	  
	*** Output: Multiple  table of this attribute comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NS03 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between this attribute for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
	
 ================================================================================================================================================================

*/



/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/



/* LYR INVESTIGATION */

---------- Sub-setting layer 1 --------------------------
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.ns03_compare_species_fri_cas_l1;
CREATE TABLE casfri50_test.ns03_compare_species_fri_cas_l1 AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.sp1, ------ CHANGE THIS
	n.sp1p, ------ CHANGE THIS
	n.sp2, ------ CHANGE THIS
	n.sp2p, ------ CHANGE THIS
	n.sp3, ------ CHANGE THIS
	n.sp3p, ------ CHANGE THIS
	n.sp4, ------ CHANGE THIS
	n.sp4p, ------ CHANGE THIS
	n.age,
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.species_1 AS species_1_TL, 	 
	c.species_per_1  AS species_per_1_TL,
	c.species_2 AS species_2_TL, 	 
	c.species_per_2  AS species_per_2_TL,
	c.species_3 AS species_3_TL, 	 
	c.species_per_3  AS species_per_3_TL,
	c.species_4 AS species_4_TL, 	 
	c.species_per_4  AS species_per_4_TL,
 	CASE WHEN species_per_1 = (sp1p*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	CASE WHEN species_per_2 = (sp2p*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_2, ------- CHANGE THIS
	CASE WHEN species_per_3 = (sp3p*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_3, ------- CHANGE THIS   
	CASE WHEN species_per_4 = (sp4p*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_4 ------- CHANGE THIS
	
FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'ns03%' and layer =1
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;


---- layer 2

DROP TABLE IF EXISTS casfri50_test.ns03_compare_species_fri_cas_l2;
CREATE TABLE casfri50_test.ns03_compare_species_fri_cas_l2 AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.sp1, ------ CHANGE THIS
	n.sp1p, ------ CHANGE THIS
	n.sp2, ------ CHANGE THIS
	n.sp2p, ------ CHANGE THIS
	n.sp3, ------ CHANGE THIS
	n.sp3p, ------ CHANGE THIS
	n.sp4, ------ CHANGE THIS
	n.sp4p, ------ CHANGE THIS
	n.age,
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.species_1 AS species_1_TL, 	 
	c.species_per_1  AS species_per_1_TL,
	c.species_2 AS species_2_TL, 	 
	c.species_per_2  AS species_per_2_TL,
	c.species_3 AS species_3_TL, 	 
	c.species_per_3  AS species_per_3_TL,
	c.species_4 AS species_4_TL, 	 
	c.species_per_4  AS species_per_4_TL,
 	CASE WHEN species_per_1 = (sp1p*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	CASE WHEN species_per_2 = (sp2p*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_2, ------- CHANGE THIS
	CASE WHEN species_per_3 = (sp3p*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_3, ------- CHANGE THIS   
	CASE WHEN species_per_4 = (sp4p*10) THEN '1' ELSE 'x' END AS CheckTestVarTL_4 ------- CHANGE THIS
	
FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'ns03%' and layer =2
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;


--- CROWN CLOSURE INVESTIGATION 

/*
=============================================================================================================================================================
    
	*** Purpose: This code evaluate and validate  the layer 1 and layer 2 and Crown Closure attribute of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of this attribute between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of this attribute comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NS03 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between this attribute for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/

---------- Sub-setting layer 1 --------------------------
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.ns03_compare_crown_closure_fri_cas_l1;
CREATE TABLE casfri50_test.ns03_compare_crown_closure_fri_cas_l1 AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.crncl as TestVarTL, ------ CHANGE THIS
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.crown_closure_upper::INTEGER AS crown_closure_upper_TL, 	 
	c.crown_closure_lower::INTEGER  AS crown_closure_lower_TL,
	--CASE WHEN crown_closure_upper::INTEGER = crncl::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	--CASE WHEN crown_closure_lower::INTEGER = crncl::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 	CASE WHEN crown_closure_upper = crncl THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	CASE WHEN crown_closure_lower = crncl THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 
FROM rawfri.ns03 n
---FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM lyr_all WHERE cas_id ILIKE 'ns03%' and layer =1
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;

-- layer 2

DROP TABLE IF EXISTS casfri50_test.ns03_compare_crown_closure_fri_cas_l2;
CREATE TABLE casfri50_test.ns03_compare_crown_closure_fri_cas_l2 AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.crncl as TestVarTL, ------ CHANGE THIS
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.crown_closure_upper::INTEGER AS crown_closure_upper_TL, 	 
	c.crown_closure_lower::INTEGER  AS crown_closure_lower_TL,
	--CASE WHEN crown_closure_upper::INTEGER = crncl::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	--CASE WHEN crown_closure_lower::INTEGER = crncl::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 	CASE WHEN crown_closure_upper = crncl THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	CASE WHEN crown_closure_lower = crncl THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 
FROM rawfri.ns03 n
---FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM lyr_all WHERE cas_id ILIKE 'ns03%' and layer =2
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;


--- miscellaneous investigation

SELECT layer, crown_closure_upper FROM casfri50.lyr_all WHERE cas_id ILIKE 'NS03-xxxxxxxxxxPICTOU-xxA4525626-0000004597-0050799'; -- Did  not find it.



----- HEIGHT INVESTIGATION

/*
=============================================================================================================================================================
    
	*** Purpose: This code evaluate and validate  the layer 1 and layer 2 and Height attribute of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of this attribute between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of this attribute comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NS03 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between this attribute for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/


---------- Sub-setting layer 1 --------------------------
-----------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.ns03_compare_height_fri_cas_l1;
CREATE TABLE casfri50_test.ns03_compare_height_fri_cas_l1 AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.height as TestVarTL, ------ CHANGE THIS
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.height_upper::INTEGER AS height_upper_TL, 	 
	c.height_lower::INTEGER  AS height_lower_TL,
	--CASE WHEN height_upper::INTEGER = height::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	--CASE WHEN height_lower::INTEGER = height::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 	CASE WHEN height_upper = height THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	CASE WHEN height_lower = height THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 
FROM rawfri.ns03 n
--FROM fri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM lyr_all WHERE cas_id ILIKE 'ns03%' and layer =1
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;



---------- Sub-setting layer 12--------------------------
-----------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.ns03_compare_height_fri_cas_l2;
CREATE TABLE casfri50_test.ns03_compare_height_fri_cas_l2 AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.height as TestVarTL, ------ CHANGE THIS
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.height_upper::INTEGER AS height_upper_LU, 	 
	c.height_lower::INTEGER  AS height_lower_LL,
	--CASE WHEN height_upper::INTEGER = height::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	--CASE WHEN height_lower::INTEGER = height::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 	CASE WHEN height_upper = height THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	CASE WHEN height_lower = height THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 
FROM rawfri.ns03 n
--FROM fri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM lyr_all WHERE cas_id ILIKE 'ns03%' and layer =2
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;


--- SITE CLASS INVESTIGAION


/*
=============================================================================================================================================================
  
	*** Purpose: This code evaluate and validate  the layer 1 and layer 2 and Site class attribute of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of this attribute between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of this attribute comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NS03 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between this attribute for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/

---------- Sub-setting layer 1 --------------------------
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.ns03_compare_siteclass_fri_cas_l1;
CREATE TABLE casfri50_test.ns03_compare_siteclass_fri_cas_l1 AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.site_sw as TestVarTL_sw, ------ CHANGE THIS
	n.site_hw as TestVarTL_hw, ------ CHANGE THIS
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.site_class AS site_class_TL, 	 
	c.site_index  AS site_index_TL
	--CASE WHEN site_class::INTEGER = fornon::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	--CASE WHEN site_index::INTEGER = fornon::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 	-- CASE WHEN site_class = fornon THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	-- CASE WHEN site_index = fornon THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 

FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'ns03%' and layer =1
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;

-- layer 2

DROP TABLE IF EXISTS casfri50_test.ns03_compare_siteclass_fri_cas_l2;
CREATE TABLE casfri50_test.ns03_compare_siteclass_fri_cas_l2 AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.site_sw as TestVarTL_sw, ------ CHANGE THIS
	n.site_hw as TestVarTL_hw, ------ CHANGE THIS
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.site_class AS site_class_TL, 	 
	c.site_index  AS site_index_TL
	--CASE WHEN site_class::INTEGER = fornon::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	--CASE WHEN site_index::INTEGER = fornon::INTEGER THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 	-- CASE WHEN site_class = fornon THEN '1' ELSE 'x' END AS CheckTestVarTL_1,  ------ CHANGE THIS
	-- CASE WHEN site_index = fornon THEN '1' ELSE 'x' END AS CheckTestVarTL_2 ------- CHANGE THIS
 

FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'ns03%' and layer =2
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;

--- miscellaneous investigation

SELECT DISTINCT (TestVarTL_sw,TestVarTL_hw,site_class_TL,site_index_TL) FROM casfri50_test.ns03_compare_siteclass_fri_cas_l2;


/*  PRODUCTIVITY INVESTIGATION  */


DROP TABLE IF EXISTS casfri50_test.productivity_ns03_fri_cas;
CREATE TABLE casfri50_test.productivity_ns03_fri_cas AS
SELECT 
	c.cas_id,
	n.fornon,
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.productivity,
	c.productivity_type,
	--c.non_for_anth,
	--c.non_for_veg,
	c.crown_closure_upper,
	c.crown_closure_lower,
	c.height_upper,
	c.height_lower
FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'ns03%' 
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;


/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/



/* DST INVESTIGATION */


DROP TABLE IF EXISTS casfri50_test.disturbance_ns03_fri_cas;
CREATE TABLE casfri50_test.disturbance_ns03_fri_cas AS
SELECT 
	c.cas_id,
	n.fornon,
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.dist_type_1,
	c.dist_year_1
FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.dst_all WHERE cas_id ILIKE 'ns03%' 
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;


/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/



/* ECO INVESTIGATION  */


DROP TABLE IF EXISTS casfri50_test.eco_ns03_fri_cas;
CREATE TABLE casfri50_test.eco_ns03_fri_cas AS
SELECT 
	c.cas_id,
	c.wetland_type,
	c.wet_veg_cover,
	c.wet_landform_mod,
	c.wet_local_mod,
	c.eco_site,
	n.fornon,
	n.wetclass,
	n.wetlnd,
	--n.src_filename,
	n.forest_id, 
	n.forest_,   
	n.ogc_fid,
	n.mapsheet
	
 	
FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.eco_all WHERE cas_id ILIKE 'ns03%' 
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;


/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/



/* NFL INVESTIGATION  */


DROP TABLE IF EXISTS casfri50_test.nfl_ns03_fri_cas;
CREATE TABLE casfri50_test.nfl_ns03_fri_cas AS
SELECT 
	c.cas_id,
	n.fornon,
	n.forest_id, ------ CHANGE THIS
	n.forest_,   ------ CHANGE THIS
	n.ogc_fid,	
	c.nat_non_veg,
	c.non_for_anth,
	c.non_for_veg,
	c.crown_closure_upper,
	c.crown_closure_lower,
	c.height_upper,
	c.height_lower
FROM rawfri.ns03 n
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.nfl_all WHERE cas_id ILIKE 'ns03%' 
) c
ON n.mapsheet = c.map_id AND n.forest_id = c.poly_id::INTEGER AND n.forest_ = c.ogc_fid::INTEGER;


/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/



/*   STAND STRUCTURE INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.stand_structure_ns03;
CREATE TABLE casfri50_test.stand_structure_ns03 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'NS03%' 
GROUP BY stand_structure;

/* NFL LAYER COUNT INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_ns03;
CREATE TABLE casfri50_test.nfl_layer_count_ns03 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'NS03%' 
GROUP BY layer;




