-- This script is intended to run all automated validations functions
-- Results of validations are in 'validation_error_rows'
-- This is all done in dynamic functions since the raw tables all have very different column names and data types
-- Ensure your search path is set to create tables and functions in an appropriate location (likely casfri_validation)
-- Also, some functions used here are in the casfri_metrics schema, so that must be in the path as well
-- Suggested path*
SET search_path = casfri_validation, casfri_metrics, casfri50, rawfri, "translation", public;


-- If you want to "refresh" a raw joined table, it must bge dropped and then recreated
-- These functions refresh raw joined tables for you
-- The TRUE flag instructs the function to drop the raw_joined tables
-- Set to FALSE if you only want to create nonexistent tables and not drop existing ones

-- SELECT * FROM refreshRawJoinedTable('BC08', 'cas_all', TRUE);
-- SELECT * FROM refreshRawJoinedTablesInInventory('BC08', TRUE);
-- SELECT * FROM refreshRawJoinedTablesInJurisdiction('BC', TRUE);


-- The statement below runs EVERYTHING. It will:
	-- Create a new validation_error_rows result table if it doesn't exist
	-- Drop any existing error rows
	-- Attempt to create a raw_joined table for each casfri table in each inventory
	-- Update any null source layers in each raw_joined table
	-- Collect validation errors for each raw_joined table
	-- Run species/nfl/dst validation on each inventory
	-- Refresh the aggregated error summary
-- This function works whether or not the joined tables have been created and the source layers have been updated. So, it shouldn't ever break if tables don't exist or source layers haven't been added
-- Note that this is a total wipe, and will take many hours to run
-- The TRUE flag instructs the function to drop the raw_joined tables
-- Set to FALSE if you only want to rerun validtions, but don't want to drop all raw_joined tables

-- SELECT * FROM refreshAllValidations(TRUE);


-- Alternately (and recommended) is to run different jurisdictions seperately in parrallel
-- Since these scripts are in functions, they will automatically rollback if an error occurs within a function which could lose hours of progress, so its better to run validations in parrallel small chunks

-- The TRUE flag instructs the function to drop the raw_joined tables
-- Set to FALSE if you only want to rerun validations, but don't want to drop all raw_joined tables
SELECT * FROM refreshValidationsOnJurisdiction('AB', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('BC', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('MB', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('NB', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('NS', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('NT', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('ON', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('PE', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('QC', TRUE);
SELECT * FROM refreshValidationsOnJurisdiction('YT', TRUE);


-- View aggregated stats on the validation results
SELECT * FROM casfri_validation.validation_error_rows_summary;

-- If the validation_error_rows_summary gets unsynced for any reason, run this to fully refresh it
-- SELECT * FROM refreshValidationSummaryAll();

-- Run this to view the validation rows summary, but with a manual validation query
-- Copy paste and run the validation query to view the (likely) suspect attributes causing the error
SELECT *, getinvestigatevalidationrowquery(table_name, error_desc, source_layer, example_cas_id, 'translation.attribute_dependencies') AS manual_validation_query
FROM validation_error_rows_summary;



