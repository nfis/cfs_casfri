-- This file contains functions useful for visualizations on grafana


-- getTranslationTable(inventory TEXT, casfri_table TEXT)
-- Given a inventory and a casfri table, returns the name of the translation table for the specific standard
-- Input:
	-- inventory: the inventory to search for
	-- casfri_table: the casfri table to find translations for
-- Output: the name of the translation table
CREATE OR REPLACE FUNCTION getTranslationTable(
inventory TEXT,
casfri_table TEXT,
hdr_location TEXT
)
RETURNS TEXT AS
	$func$
	DECLARE
		standard TEXT;
	BEGIN
		EXECUTE format('SELECT standard_id FROM %s WHERE inventory_id = ''%s''', hdr_location, inventory) INTO standard;
		-- YT04 uses a different translation table for cas and geo entries
		IF inventory = 'YT04' AND casfri_table IN ('cas_all', 'geo_all')
		THEN
			standard := 'YVI01';
		ELSIF inventory = 'YT04'
		THEN
			standard := 'YVI03';
		END IF;
		RETURN left(inventory, 2) || '_' || standard || '_' || left(casfri_table, 3);
	END;
	$func$
LANGUAGE 'plpgsql';


-- getTranslationRow(inventory TEXT, casfri_table TEXT, attribute_name TEXT)
-- Given a inventory, a casfri table, and an attribute, returns the attribute's row of the translation table for the specific standard
-- Input:
	-- inventory: the inventory to search for
	-- casfri_table: the casfri table to find translations for
	-- attribute_name: the attribute to search for
-- Output: TABLE(rule_id TEXT, target_attribute TEXT, target_attribute_type TEXT, validation_rules TEXT, translation_rules TEXT, description TEXT)
CREATE OR REPLACE FUNCTION getTranslationRow(
inventory TEXT,
casfri_table TEXT,
attribute_name TEXT,
hdr_location TEXT,
translation_schema TEXT
)
RETURNS TABLE(rule_id TEXT, target_attribute TEXT, target_attribute_type TEXT, validation_rules TEXT, translation_rules TEXT, description TEXT) AS
	$func$
	DECLARE
		tt TEXT;
	BEGIN
		EXECUTE format('SELECT * FROM casfri_validation.getTranslationTable(''%s'', ''%s'', ''%s'')', inventory, casfri_table, hdr_location) INTO tt;
		RETURN QUERY EXECUTE format('SELECT rule_id::TEXT, target_attribute::TEXT, target_attribute_type::TEXT, validation_rules::TEXT, translation_rules::TEXT, description::TEXT
									FROM %s.%s
									WHERE lower(target_attribute) = lower(''%s'')', translation_schema, tt, attribute_name);
	END;
	$func$
LANGUAGE 'plpgsql';



-- getAttributeDependencies(inventory TEXT, casfri_table TEXT, attribute_name TEXT, attribute_dependencies_location TEXT)
-- Given a inventory, a casfri table, and an attribute, returns the relevant attribute dependencies rows for that attribute and standard
-- Input:
	-- inventory: the inventory to search for
	-- casfri_table: the casfri table to find translations for
	-- attribute_name: the attribute to search for
	-- -- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
-- Output: TABLE(inventory_id TEXT, layer TEXT, "table" TEXT, dependency TEXT)
	-- "dependency" is the column with the name of attribute_name
CREATE OR REPLACE FUNCTION getAttributeDependencies(
inventory TEXT,
casfri_table TEXT,
attribute_name TEXT,
attribute_dependencies_location TEXT
)
RETURNS TABLE(inventory_id TEXT, layer TEXT, "table" TEXT, dependency TEXT) AS
	$func$
	DECLARE
		inv_ogc_fid INT;
		standard_ogc_fid INT;
	BEGIN
		IF casfri_table = 'nfl_all' AND attribute_name IN ('height_upper', 'height_lower') THEN
			attribute_name := 'nfl_height';
		ELSIF casfri_table = 'nfl_all' AND attribute_name IN ('crown_closure_upper', 'crown_closure_lower') THEN
			attribute_name := 'nfl_crown_closure';
		END IF;
		EXECUTE format('SELECT min(ogc_fid) 
						FROM %s
						WHERE left(inventory_id, 4) = ''%s''', attribute_dependencies_location, inventory) INTO inv_ogc_fid;	
		EXECUTE format('SELECT max(ogc_fid) 
						FROM %s
						WHERE left(inventory_id, 2) = ''%s'' AND ttable_exists = ''yes'' AND ogc_fid < %s', 
						attribute_dependencies_location, LEFT(inventory,2), inv_ogc_fid) INTO standard_ogc_fid;				
		RETURN QUERY EXECUTE format('(SELECT inventory_id::TEXT, layer::TEXT, "table"::TEXT, %s::TEXT AS dependency 
									FROM %s
									WHERE ogc_fid::TEXT = %s::TEXT)
									UNION ALL
									(SELECT inventory_id::TEXT, layer::TEXT, "table"::TEXT, %s::TEXT AS dependency  
									FROM %s
									WHERE inventory_id = ''%s'' AND ttable_exists = ''no''
									ORDER BY ogc_fid)', attribute_name, attribute_dependencies_location, standard_ogc_fid, attribute_name, attribute_dependencies_location, inventory);
	END;
	$func$
LANGUAGE 'plpgsql';


-- appendCommaSeparated(arr TEXT[],string TEXT,prefix TEXT)
-- Given an existing array and a string of comma separated values, will append the array with each value
-- Values will be prefixed with the prefix value (unless the value is like 'NO_DEPENDENCY')
-- Duplicate values will be appended with their occurence number
-- Input:
	-- arr: array of text values
	-- string: string with comma separated strings
	-- prefix: a string to prepend to all values
-- Output: TEXT[]
	-- Array with new values appended
CREATE OR REPLACE FUNCTION appendCommaSeparated(
arr TEXT[],
string TEXT,
prefix TEXT
)
RETURNS TEXT[]
LANGUAGE 'plpgsql' AS
$func$
	DECLARE 
		val TEXT;
		i INTEGER;
		x INTEGER;
		remove INTEGER;
	BEGIN
		IF split_part(string, ',', 1) LIKE '%NO_DEPENDENCY%' OR split_part(string, ',', 1) LIKE '''CC'' AS dep_%'
		THEN
			val := trim(split_part(string, ',', 1));
		ELSEIF split_part(string, ',', 1) LIKE 'wkb_geometry%'
			THEN
				val := '''wkb_geometry'' AS wkb_geometry';
			ELSE
			val := prefix || trim(split_part(string, ',', 1));
		END IF;
		i := 2;
		WHILE val IS NOT NULL AND val != prefix
		LOOP 	
			IF val = ANY(arr)
			THEN
				x := 2;
				IF val NOT LIKE '% AS %'
				THEN
					val := val || ' AS ' || val || '_' || x;
				ELSE
					val := val || '_' || x;
				END IF;
				WHILE val = ANY(arr)
				LOOP
					remove := length(x::text);
					x := x + 1;
					val := substring(val, 1, length(val)-remove) || x;
				END LOOP;
			END IF;
			IF trim(split_part(string, ',', i-1)) !~ '^[0-9]+$' AND trim(split_part(string, ',', i-1)) != 'NULL'
			THEN
				arr := array_append(arr, val);
			END IF;
			IF split_part(string, ',', i) LIKE '%NO_DEPENDENCY%' OR split_part(string, ',', i) LIKE '''CC'' AS dep_%'
			THEN
				val := trim(split_part(string, ',', i));
			ELSEIF split_part(string, ',', i) LIKE '%wkb_geometry%'
			THEN
				val := '''wkb_geometry'' AS wkb_geometry';
			ELSE
				val := prefix || trim(split_part(string, ',', i));
			END IF;
			i := i + 1;
		END LOOP;
		RETURN arr;
	END;
$func$;


-- getInvestigateValidationRowQuery(table_name,error_desc,source_layer,cas_id,attribute_dependencies_location)
-- Given information from a row in validation_error_rows, creates a query to be ran to manually validate the row
-- This is a select statement with all attributes that are the likely cause of the error
-- Errors must be manually inspected to determine the cause
-- Will ensure there are no duplicate column names by appending a number to the end of aliases (this is to support grafana visualizations)
-- Input:
	-- table_name: the name of the casfri table the error was found in
	-- error_desc: the error description given in the validation result
	-- source_layer: the source layer of the specific error row
	-- cas_id: the cas_id of the specific error row
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
-- Output: TEXT query
	-- Query must be copied, pasted, then ran to get results
CREATE OR REPLACE FUNCTION getInvestigateValidationRowQuery(
table_name TEXT,
error_desc TEXT,
source_layer TEXT,
cas_id TEXT,
attribute_dependencies_location TEXT
)
RETURNS TEXT
LANGUAGE 'plpgsql' AS
$func$
	DECLARE 
		dynamic_sql TEXT;
		col TEXT;
		i INTEGER;
		arr TEXT[];
		dep TEXT;
		val TEXT;
	BEGIN
		arr := array_append(arr, 'cas_id');
		arr := array_append(arr, 'source_layer');
		dynamic_sql := 'SELECT ';
		IF error_desc LIKE 'species%'
		THEN 
			FOR i IN 1..10
			LOOP
				FOR col IN (VALUES ('species_'), ('species_per_'))
				LOOP
					SELECT * FROM casfri_validation.getattributedependency(LEFT(cas_id, 4), format('%s%s', col, i), attribute_dependencies_location, source_layer::INTEGER, 'lyr_all') INTO dep;
					SELECT * INTO arr FROM casfri_validation.appendcommaseparated(arr, col || i, ''); 
					EXIT WHEN dep IS NULL;
					IF dep = '' or trim(dep) = 'NULL'
					THEN
						dep := '''NO_DEPENDENCY'' AS dep_' ||col || i;
					END IF;
					SELECT * INTO arr FROM casfri_validation.appendcommaseparated(arr, dep, 'raw_'); 
				END LOOP;
			END LOOP;
		ELSIF error_desc LIKE 'dist%'
		THEN
			FOR i IN 1..3
			LOOP
				FOR col IN (VALUES ('dist_type_'), ('dist_year_'), ('dist_ext_upper_'), ('dist_ext_lower_'))
				LOOP
					SELECT * FROM casfri_validation.getattributedependency(LEFT(cas_id, 4), format('%s%s', col, i), attribute_dependencies_location, source_layer::INTEGER, 'dst_all') INTO dep;
					SELECT * INTO arr FROM casfri_validation.appendcommaseparated(arr, col || i, ''); 
					EXIT WHEN dep IS NULL;
					IF dep = ''
					THEN
						dep := '''NO_DEPENDENCY'' AS dep_' ||col || i;
					ELSIF dep = 'CC'''
					THEN
						dep := '''CC'' AS dep_' ||col || i;
					END IF;
					SELECT * INTO arr FROM casfri_validation.appendcommaseparated(arr, dep, 'raw_'); 
				END LOOP;
			END LOOP;
		ELSIF error_desc LIKE 'nat_non_veg%' OR error_desc LIKE 'non_for%'
		THEN
			FOR col IN (VALUES ('nat_non_veg'), ('non_for_anth'), ('non_for_veg'))
			LOOP
				SELECT * FROM casfri_validation.getattributedependency(LEFT(cas_id, 4), col, attribute_dependencies_location, source_layer::INTEGER, 'nfl_all') INTO dep;
				SELECT * INTO arr FROM casfri_validation.appendcommaseparated(arr, col, ''); 
				EXIT WHEN dep IS NULL;
				IF dep = ''
				THEN
					dep := '''NO_DEPENDENCY'' AS dep_' ||col;
				END IF;
				SELECT * INTO arr FROM casfri_validation.appendcommaseparated(arr, dep, 'raw_'); 
			END LOOP;
		ELSE
			col := trim(split_part(error_desc, ' ', 1));
			SELECT * FROM casfri_validation.getattributedependency(LEFT(cas_id, 4), col, attribute_dependencies_location, source_layer::INTEGER, table_name) INTO dep;
			SELECT * INTO arr FROM casfri_validation.appendcommaseparated(arr, col, ''); 
			IF dep IS NOT NULL
			THEN
				IF dep = ''
				THEN
					dep := '''NO_DEPENDENCY'' AS dep_' ||col || i;
				END IF;
				SELECT * INTO arr FROM casfri_validation.appendcommaseparated(arr, dep, 'raw_'); 
			END IF;
		END IF;
		FOREACH val IN ARRAY arr
		LOOP
			dynamic_sql := dynamic_sql || val || ', ';
		END LOOP;
		dynamic_sql := trim(TRAILING ', ' FROM dynamic_sql);
		dynamic_sql := dynamic_sql || ' FROM casfri_validation.' || left(cas_id, 4) || '_raw_' || table_name || '_joined where cas_id = ''' || cas_id || ''' and source_layer::integer = ' || source_layer::integer || ';';
		RETURN dynamic_sql;
	END;
$func$;

