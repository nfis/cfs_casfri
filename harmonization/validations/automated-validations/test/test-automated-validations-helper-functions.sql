SET search_path = casfri_validation, casfri_metrics, casfri50, casfri_web, casfri_post_processing, rawfri, "translation", public;


-- Tests all functions in automated-validation-helper-functions.sql to ensure that the correct result is returned.
-- Run as script to see results of each test (you can also comment out the drop table at the end of the file to persist results)
-- Ensure that your search path is set with casfri_validation first. Most table creations should default to casfri_validation, but things may break if a different schema is used for this test

DROP TABLE IF EXISTS casfri_validation.validations_test_results;
CREATE TABLE IF NOT EXISTS casfri_validation.validations_test_results (test TEXT, result TEXT);
----------------------------------------------------------------------------------

-- Test for isTranslationError(value TEXT)
WITH row_validity AS (
	SELECT isTranslationError('WRONG_TYPE') = TRUE AS value
	UNION ALL
	SELECT isTranslationError('EMPTY_STRING') = FALSE AS value
	UNION ALL
	SELECT isTranslationError('SOMETHING') = FALSE AS value
	UNION ALL
	SELECT isTranslationError(NULL) = FALSE AS value
	UNION ALL
	SELECT isTranslationError('-9999') = TRUE AS value
	UNION ALL
	SELECT isTranslationError('9999') = FALSE AS value
)
INSERT INTO validations_test_results SELECT 'isTranslationError' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

----------------------------------------------------------------------------------

-- Test for updateValidationErrorCodes(dest_table_name TEXT, creation_schema TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (error_type TEXT);

INSERT INTO casfri_validation.validation_error_rows_test VALUES
('-8888'),
('NULL_VALUE'),
('-3333'),
('OUT_OF_RANGE');

SELECT * FROM updateValidationErrorCodes('validation_error_rows_test', 'casfri_validation');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN error_type IN ('NULL_VALUE', 'TRANSLATION_ERROR', 'OUT_OF_RANGE')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runBasicNullValidation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are 3 distinct error types
	AND (SELECT count(DISTINCT error_type) FROM test_result) = 3
	-- Check that there are exactly 4 rows in the result
	AND (SELECT count(*) FROM test_result) = 4
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
----------------------------------------------------------------------------------

-- Test for runBasicNullValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, dependency, source_layer)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ab80_raw_cas_all_joined (cas_id TEXT, source_layer TEXT, stand_photo_year INTEGER, raw_year INTEGER);

INSERT INTO casfri_validation.ab80_raw_cas_all_joined VALUES
('AB80-1', '1', -8888, 2000),
('AB80-2', '1', 2000, NULL);

SELECT * FROM runBasicNullValidation('validation_error_rows_test', 'casfri_validation', 'ab80_raw_cas_all_joined', 'stand_photo_year', 'year', '1');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'AB80-1' AND table_name = 'cas_all' AND error_type = '-8888' AND error_desc = 'stand_photo_year is empty but raw value exists' AND source_layer = '1')
		OR (cas_id = 'AB80-2' AND table_name = 'cas_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'stand_photo_year is not an error code but raw value is empty' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runBasicNullValidation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 2 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 2
	-- Check that there are exactly 2 row in the result
	AND (SELECT count(*) FROM test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ab80_raw_cas_all_joined;
----------------------------------------------------------------------------------

-- Test for runNoMappingValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, source_layer)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ab80_raw_cas_all_joined (cas_id TEXT, source_layer TEXT, stand_photo_year INTEGER);

INSERT INTO casfri_validation.ab80_raw_cas_all_joined VALUES
('AB80-1', '1', -8888),
('AB80-2', '1', 2000);

SELECT * FROM runNoMappingValidation('validation_error_rows_test', 'casfri_validation', 'ab80_raw_cas_all_joined', 'stand_photo_year', '1');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'AB80-2' AND table_name = 'cas_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'stand_photo_year is not an error code but attribute does not exist in raw inventory' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runNoMappingValidation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ab80_raw_cas_all_joined;
----------------------------------------------------------------------------------

-- Test for runGeoDependencyValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, source_layer)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ab80_raw_cas_all_joined (cas_id TEXT, source_layer TEXT, stand_photo_year INTEGER);

INSERT INTO casfri_validation.ab80_raw_cas_all_joined VALUES
('AB80-1', '1', -8888),
('AB80-2', '1', 2000);

SELECT * FROM runGeoDependencyValidation('validation_error_rows_test', 'casfri_validation', 'ab80_raw_cas_all_joined', 'stand_photo_year', '1');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'AB80-1' AND table_name = 'cas_all' AND error_type = '-8888' AND error_desc = 'stand_photo_year is empty but raw value exists' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runGeoDependencyValidation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ab80_raw_cas_all_joined;
----------------------------------------------------------------------------------

-- Test for runNumericDependencyValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, dependency, source_layer)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ab80_raw_cas_all_joined (cas_id TEXT, source_layer TEXT, stand_photo_year INTEGER);

INSERT INTO casfri_validation.ab80_raw_cas_all_joined VALUES
('AB80-1', '1', -8888),
('AB80-2', '1', 2000);

SELECT * FROM runNumericDependencyValidation('validation_error_rows_test', 'casfri_validation', 'ab80_raw_cas_all_joined', 'stand_photo_year', '2000', '1');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'AB80-1' AND table_name = 'cas_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'stand_photo_year does not match its numeric mapping' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runNumericDependencyValidation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ab80_raw_cas_all_joined;
----------------------------------------------------------------------------------

-- Test for runGenericTranslationErrorValidation(dest_table_name TEXT,creation_schema TEXT, raw_merged_table TEXT, column_name, source_layer)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ab80_raw_cas_all_joined (cas_id TEXT, source_layer TEXT, stand_photo_year INTEGER);

INSERT INTO casfri_validation.ab80_raw_cas_all_joined VALUES
('AB80-1', '1', -9999),
('AB80-2', '1', 2000);

SELECT * FROM runGenericTranslationErrorValidation('validation_error_rows_test', 'casfri_validation', 'ab80_raw_cas_all_joined', 'stand_photo_year', '1');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'AB80-1' AND table_name = 'cas_all' AND error_type = '-9999' AND error_desc = 'stand_photo_year has a translation error' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runGenericTranslationErrorValidation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ab80_raw_cas_all_joined;
----------------------------------------------------------------------------------

-- Test for runNumOfLayersValidationOnInventory(inventory TEXT,dest_table_name TEXT,creation_schema TEXT,casfri_schema TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.cas_all (cas_id TEXT, num_of_layers INT);
INSERT INTO casfri_validation.cas_all VALUES
('AB80-1', '1'),
('AB80-2', '2'),
('AB80-3', '1'),
('AB80-4', '-8886');

CREATE TABLE IF NOT EXISTS casfri_validation.lyr_all (cas_id TEXT, layer INT);
INSERT INTO casfri_validation.lyr_all VALUES
('AB80-1', '1'),
('AB80-2', '1'),
('AB80-2', '2');

CREATE TABLE IF NOT EXISTS casfri_validation.nfl_all (cas_id TEXT, layer INT);
INSERT INTO casfri_validation.nfl_all VALUES
('AB80-2', '1'),
('AB80-4', '1');

SELECT * FROM runNumOfLayersValidationOnInventory('AB80', 'validation_error_rows_test', 'casfri_validation', 'casfri_validation');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'AB80-2' AND table_name = 'cas_all' AND error_type = 'ROW_TRANSLATION' AND error_desc = 'num_of_layers was 2 but lyr_count is 2 and nfl_count is 1' AND source_layer = '1')
		OR (cas_id = 'AB80-3' AND table_name = 'cas_all' AND error_type = 'ROW_TRANSLATION' AND error_desc = 'num_of_layers was 1 but lyr_count is 0 and nfl_count is 0' AND source_layer = '1')
		OR (cas_id = 'AB80-4' AND table_name = 'cas_all' AND error_type = 'ROW_TRANSLATION' AND error_desc = 'num_of_layers was -8886 but lyr_count is 0 and nfl_count is 1' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runNumOfLayersValidationOnInventory' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 3 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 3
	-- Check that there are exactly 3 rows in the result
	AND (SELECT count(*) FROM test_result) = 3
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.cas_all;
DROP TABLE IF EXISTS casfri_validation.nfl_all;
DROP TABLE IF EXISTS casfri_validation.lyr_all;
----------------------------------------------------------------------------------

-- Test for ab_avi01_stand_photo_year_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ab80_raw_cas_all_joined (cas_id TEXT, source_layer TEXT, stand_photo_year INTEGER, raw_photo_yr INTEGER);

INSERT INTO casfri_validation.ab80_raw_cas_all_joined VALUES
('AB80-1', '1', 2000, 2000),
('AB80-2', '1', 2000, 2010);

SELECT * FROM ab_avi01_stand_photo_year_validation('validation_error_rows_test', 'casfri_validation', 'AB80');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'AB80-2' AND table_name = 'cas_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'stand_photo_year does not match its numeric mapping' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'ab_avi01_stand_photo_year_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ab80_raw_cas_all_joined;
----------------------------------------------------------------------------------

-- Test for nb_nbi01_stand_photo_year_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.nb80_raw_cas_all_joined (cas_id TEXT, source_layer TEXT, stand_photo_year INTEGER, raw_l1datayr INTEGER, raw_l2datayr INTEGER);

INSERT INTO casfri_validation.nb80_raw_cas_all_joined VALUES
('NB80-1', '1', 2000, 2000, 0),
('NB80-2', '1', 2000, 0, 2010);

SELECT * FROM nb_nbi01_stand_photo_year_validation('validation_error_rows_test', 'casfri_validation', 'NB80');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'NB80-2' AND table_name = 'cas_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'stand_photo_year does not match its numeric mapping' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'nb_nbi01_stand_photo_year_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.nb80_raw_cas_all_joined;
----------------------------------------------------------------------------------

-- Test for yt_yvi03_nfl_soil_moist_reg_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.yt80_raw_nfl_all_joined (cas_id TEXT, source_layer TEXT, soil_moist_reg TEXT, raw_land_type TEXT, raw_landcov_cl TEXT, raw_cov_cl_mod TEXT, raw_landsc_pos TEXT, raw_cover_type TEXT, raw_soil_moist TEXT);

INSERT INTO casfri_validation.yt80_raw_nfl_all_joined VALUES
('YT80-1', '1', 'MESIC', 'Vegetated, Non-Forested', NULL, 'Low Shrub', NULL, NULL, 'Mesic'),
('YT80-2', '1', 'MESIC', 'Other', NULL, 'Low Shrub', NULL, NULL, 'Mesic'),
('YT80-3', '1', 'MESIC', 'Vegetated, Non-Forested', NULL, 'Low Shrub', NULL, NULL, 'Other');


SELECT * FROM yt_yvi03_nfl_soil_moist_reg_validation('validation_error_rows_test', 'casfri_validation', 'YT80');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'YT80-2' AND table_name = 'nfl_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'soil_moist_reg is not an error code but raw value is empty' AND source_layer = '1')
		OR (cas_id = 'YT80-3' AND table_name = 'nfl_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'soil_moist_reg is not an error code but raw value is empty' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'yt_yvi03_nfl_soil_moist_reg_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 2 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 2
	-- Check that there are exactly 2 rows in the result
	AND (SELECT count(*) FROM test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.yt80_raw_nfl_all_joined;
----------------------------------------------------------------------------------

-- Test for bc_vri01_height_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, height_upper FLOAT, height_lower FLOAT, raw_l1_proj_height_1 FLOAT, raw_l1_proj_height_2 FLOAT, raw_l1_species_pct_1 FLOAT, raw_l1_species_pct_2 FLOAT, raw_l2_proj_height_1 FLOAT, raw_l2_proj_height_2 FLOAT, raw_l2_species_pct_1 FLOAT, raw_l2_species_pct_2 FLOAT);

INSERT INTO casfri_validation.bc80_raw_lyr_all_joined VALUES
('BC80-1', '1', 16, 16,     12, 20, 50, 50, 30, 31, 80, 20),
('BC80-1', '2', 30.2, 30.2, 12, 20, 50, 50, 30, 31, 80, 20),
('BC80-2', '1', 17, 17,       12, 20, 50, 50, NULL, NULL, NULL, NULL),
('BC80-2', '2', -8888, -8888, 12, 20, 50, 50, NULL, NULL, NULL, NULL);


SELECT * FROM bc_vri01_height_validation('validation_error_rows_test', 'casfri_validation', 'BC80', 'height_upper');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-2' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'height_upper does not match its numeric mapping' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'bc_vri01_height_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 1
	-- Check that there are exactly 1 rows in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
----------------------------------------------------------------------------------

-- Test for lyr_mapping_validations(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, col TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.on80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, productivity TEXT, raw_formod TEXT, raw_polytype TEXT);

INSERT INTO casfri_validation.on80_raw_lyr_all_joined VALUES
('ON80-1', '1', 'PRODUCTIVE_FOREST', 'PF', 'BSH'),
('ON80-2', '1', 'SOMETHING', '', 'BSH'),
('ON80-3', '1', 'NULL_VALUE', '?', '?');

SELECT * FROM lyr_mapping_validations('validation_error_rows_test', 'casfri_validation', 'ON80', 'productivity');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'ON80-2' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'productivity does not match its direct mapping' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'lyr_mapping_validations' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct error_desc
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.on80_raw_lyr_all_joined;
----------------------------------------------------------------------------------

-- Test for bc_vri01_origin_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, origin_upper INTEGER, origin_lower INTEGER, raw_projected_date TEXT, raw_l1_proj_age_1 INTEGER, raw_l2_proj_age_1 INTEGER);

INSERT INTO casfri_validation.bc80_raw_lyr_all_joined VALUES
('BC80-1', '1', 1900, 1900, '2000', 100, 90),
('BC80-1', '2', 1800, 1800, '2000', 100, 90),
('BC80-2', '1', -8888, -8888, '1999', 100, 90),
('BC80-2', '2', -9999, -9999, '2000', 100, 90);

SELECT * FROM bc_vri01_origin_validation('validation_error_rows_test', 'casfri_validation', 'BC80', 'origin_upper');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-1' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'origin_upper does not match its numeric mapping' AND source_layer = '2')
		OR (cas_id = 'BC80-2' AND table_name = 'lyr_all' AND error_type = '-8888' AND error_desc = 'origin_upper is empty but a raw value exists' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'bc_vri01_origin_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 2 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 2
	-- Check that there are exactly 2 rows in the result
	AND (SELECT count(*) FROM test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
----------------------------------------------------------------------------------

-- Test for qc_origin_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.qc80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, origin_upper INTEGER, origin_lower INTEGER, raw_inf_cl_age_et TEXT, raw_an_pro_ori TEXT);

INSERT INTO casfri_validation.qc80_raw_lyr_all_joined VALUES
('QC80-1', '1', 1930, 1930, '7050', '2000'),
('QC80-1', '2', 1950, 1930, '7050', '2000'),
('QC80-2', '1', -9999, -9999, '8000', '2010'),
('QC80-2', '2', -8888, -8888, '', '19');

SELECT * FROM qc_origin_validation('validation_error_rows_test', 'casfri_validation', 'QC80', 'origin_upper');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'QC80-1' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'origin_upper does not match its numeric mapping' AND source_layer = '2')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'qc_origin_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct error_desc
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 1
	-- Check that there are exactly 1 row in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.qc80_raw_lyr_all_joined;
----------------------------------------------------------------------------------

-- Test for yt_ns_origin_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ns80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, origin_upper INTEGER, origin_lower INTEGER, raw_photoyr TEXT);

INSERT INTO casfri_validation.ns80_raw_lyr_all_joined VALUES
('NS80-1', '1', -8888, -8888, '2020'),
('NS80-1', '2', 2020, 2020, '2020');

SELECT * FROM yt_ns_origin_validation('validation_error_rows_test', 'casfri_validation', 'NS80', 'origin_upper');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'NS80-1' AND table_name = 'lyr_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'origin_upper is not an error code but raw value is empty' AND source_layer = '2')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'yt_ns_origin_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 1
	-- Check that there are exactly 1 rows in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ns80_raw_lyr_all_joined;
----------------------------------------------------------------------------------

-- Test for bc_vri01_site_index_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, site_index INTEGER, raw_l1_site_index TEXT, raw_l1_est_site_index TEXT, raw_l2_site_index TEXT, raw_l2_est_site_index TEXT);


INSERT INTO casfri_validation.bc80_raw_lyr_all_joined VALUES
('BC80-1', '1', 1, '1', '3', '2', '4'),
('BC80-1', '2', 4, '1', '3', '2', '4'),
('BC80-2', '1', -9999, '1', '3', '2', '4'),
('BC80-2', '2', -8886, '1', '3', '2', '4');

SELECT * FROM bc_vri01_site_index_validation('validation_error_rows_test', 'casfri_validation', 'BC80');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-1' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'site_index does not match its numeric mapping' AND source_layer = '2')
		OR (cas_id = 'BC80-2' AND table_name = 'lyr_all' AND error_type = '-8886' AND error_desc = 'site_index is empty but a raw value exists' AND source_layer = '2')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'bc_vri01_site_index_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 2 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 2
	-- Check that there are exactly 2 rows in the result
	AND (SELECT count(*) FROM test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
----------------------------------------------------------------------------------

-- Test for ab_nfl_crown_closure_height_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, col TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ab80_raw_nfl_all_joined (cas_id TEXT, source_layer TEXT, height_upper FLOAT, height_lower FLOAT, raw_nfl TEXT, raw_height FLOAT, raw_unfl TEXT, raw_uheight FLOAT);

INSERT INTO casfri_validation.ab80_raw_nfl_all_joined VALUES
('AB80-1', '3', 50, 50, 'SO', 50, 'SC', 40),
('AB80-1', '4', -8888, -8888, 'SO', 50, 'SC', 40);

SELECT * FROM ab_nfl_crown_closure_height_validation('validation_error_rows_test', 'casfri_validation', 'AB80', 'height_upper');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'AB80-1' AND table_name = 'nfl_all' AND error_type = '-8888' AND error_desc = 'height_upper is empty but a raw value exists' AND source_layer = '4')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'ab_nfl_crown_closure_height_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 1
	-- Check that there are exactly 1 rows in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ab80_raw_nfl_all_joined;
----------------------------------------------------------------------------------

-- Test for wetland_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, function_call TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.nb80_raw_eco_all_joined (cas_id TEXT, source_layer TEXT, wetland_type TEXT, wet_veg_cover TEXT, wet_landform_mod TEXT, wet_local_mod TEXT, raw_wc TEXT, raw_vt TEXT, raw_im TEXT);

INSERT INTO casfri_validation.nb80_raw_eco_all_joined VALUES
('NB80-1', '1', 'BOG', 'WOODED', 'NO_PERMAFROST_PATTERNING', 'NO_LAWN', 'BO', 'FS', ''),
('NB80-2', '1', 'NOT_APPLICABLE', 'BOG', 'OUT_OF_RANGE', 'NO_LAWN', 'BO', 'FS', '');

SELECT * FROM wetland_validation('validation_error_rows_test', 'casfri_validation', 'wetland_type', 'NB80', 'nb_nbi01_wetland_validation(raw_wc::text, raw_vt::text, raw_im::text, 1::text)');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'NB80-2' AND table_name = 'eco_all' AND error_type = 'NOT_APPLICABLE' AND error_desc = 'wetland_type is empty but a raw value exists' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'wetland_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 1
	-- Check that there are exactly 1 rows in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.nb80_raw_eco_all_joined;
----------------------------------------------------------------------------------

-- Test for bc_vri01_productivity_type_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, productivity_type TEXT, raw_non_productive_descriptor_cd TEXT, raw_for_mgmt_land_base_ind TEXT);


INSERT INTO casfri_validation.bc80_raw_lyr_all_joined VALUES
('BC80-1', '1', 'ALPINE_FOREST', 'AF', ''),
('BC80-2', '1', 'NOT_APPLICABLE', 'AF', 'Y');

SELECT * FROM bc_vri01_productivity_type_validation('validation_error_rows_test', 'casfri_validation', 'BC80');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-2' AND table_name = 'lyr_all' AND error_type = 'NOT_APPLICABLE' AND error_desc = 'productivity_type is empty but a raw value exists' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'bc_vri01_productivity_type_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 1
	-- Check that there are exactly 1 rows in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_lyr_all_joined;
----------------------------------------------------------------------------------

-- Test for mb_fli01_nfl_crown_closure_height_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.mb80_raw_nfl_all_joined (cas_id TEXT, source_layer TEXT, height_upper FLOAT, raw_nnf_anth TEXT, raw_ht FLOAT);

INSERT INTO casfri_validation.mb80_raw_nfl_all_joined VALUES
('MB80-1', '1', 12, 'S01', 13),
('MB80-2', '1', 12, 'IDK', 12);

SELECT * FROM mb_fli01_nfl_crown_closure_height_validation('validation_error_rows_test', 'casfri_validation', 'MB80', 'height_upper');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'MB80-1' AND table_name = 'nfl_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'height_upper is not an error code but raw value is empty' AND source_layer = '1')
		OR (cas_id = 'MB80-2' AND table_name = 'nfl_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'height_upper is not an error code but raw value is empty' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'mb_fli01_nfl_crown_closure_height_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 2 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 2
	-- Check that there are exactly 2 rows in the result
	AND (SELECT count(*) FROM test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.mb80_raw_nfl_all_joined;
----------------------------------------------------------------------------------

-- Test for mb_fli01_structure_range_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.mb80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, structure_range TEXT, raw_canlay TEXT, raw_comht TEXT);

INSERT INTO casfri_validation.mb80_raw_lyr_all_joined VALUES
('MB80-1', '1', '10', 'C', '2'),
('MB80-2', '1', '10', 'A', '5');

SELECT * FROM mb_fli01_structure_range_validation('validation_error_rows_test', 'casfri_validation', 'MB80');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'MB80-1' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'structure_range does not match its numeric mapping' AND source_layer = '1')
		OR (cas_id = 'MB80-2' AND table_name = 'lyr_all' AND error_type = 'MISSING_ERROR' AND error_desc = 'structure_range is not an error code but raw value is empty' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'mb_fli01_structure_range_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 2 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 2
	-- Check that there are exactly 2 rows in the result
	AND (SELECT count(*) FROM test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.mb80_raw_lyr_all_joined;
----------------------------------------------------------------------------------

-- Test for ns_nfl_height_crown_closure_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT, col TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.ns80_raw_nfl_all_joined (cas_id TEXT, source_layer TEXT, height_upper TEXT, raw_fornon TEXT);

INSERT INTO casfri_validation.ns80_raw_nfl_all_joined VALUES
('NS80-1', '1', '-8888', '5'),
('NS80-2', '1', '0.98', '5');

SELECT * FROM ns_nfl_height_crown_closure_validation('validation_error_rows_test', 'casfri_validation', 'NS80', 'height_upper');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'NS80-1' AND table_name = 'nfl_all' AND error_type = '-8888' AND error_desc = 'height_upper is empty but a raw value exists' AND source_layer = '1')
		OR (cas_id = 'NS80-2' AND table_name = 'nfl_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'height_upper does not match its numeric mapping' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'ns_nfl_height_crown_closure_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 2 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 2
	-- Check that there are exactly 2 rows in the result
	AND (SELECT count(*) FROM test_result) = 2
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.ns80_raw_nfl_all_joined;
----------------------------------------------------------------------------------

-- Test for on_site_index_validation(dest_table_name TEXT,creation_schema TEXT, inventory TEXT)
CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);
CREATE TABLE IF NOT EXISTS casfri_validation.on80_raw_lyr_all_joined (cas_id TEXT, source_layer TEXT, site_index TEXT, raw_osi TEXT, raw_usi TEXT);

INSERT INTO casfri_validation.on80_raw_lyr_all_joined VALUES
('ON80-1', '1', '-8888', '0', '12'),
('ON80-1', '2', '13', '0', '12');

SELECT * FROM on_site_index_validation('validation_error_rows_test', 'casfri_validation', 'ON80');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'ON80-1' AND table_name = 'lyr_all' AND error_type = 'INCORRECT_MAPPING' AND error_desc = 'site_index does not match its numeric mapping' AND source_layer = '2')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'on_site_index_validation' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there is 1 distinct cas_ids
	AND (SELECT count(DISTINCT cas_id) FROM test_result) = 1
	-- Check that there are exactly 1 rows in the result
	AND (SELECT count(*) FROM test_result) = 1
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.on80_raw_lyr_all_joined;
----------------------------------------------------------------------------------


-- Display results
SELECT * FROM casfri_validation.validations_test_results;
DROP TABLE IF EXISTS casfri_validation.validations_test_results;
