SET search_path = casfri_validation, casfri_metrics, casfri50, casfri_web, casfri_post_processing, rawfri, "translation", public;

-- Tests all functions in disturbance-validation-funtions.sql to ensure that the correct result is returned.
-- Run as script to see results of each test (you can also comment out the drop table at the end of the file to persist results)
-- Ensure that your search path is set with casfri_validation first. Most table creations should default to casfri_validation, but things may break if a different schema is used for this test

DROP TABLE IF EXISTS casfri_validation.validations_test_results;
CREATE TABLE IF NOT EXISTS casfri_validation.validations_test_results (test TEXT, result TEXT);
----------------------------------------------------------------------------------

-- Test for getDisturbanceCutoff(inventory, attribute_dependencies_location)
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	dist_type_1 TEXT,
	dist_type_2 TEXT,
	dist_type_3 TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
('TE00', '1', '''LYR''', '', '', ''),
('TE01', '1', '''LYR''', 'dist', '', ''),
('TE02', '1', '''LYR''', 'history', 'history', ''),
('TE03', '1', '''LYR''', 'd1', 'd2', 'd3');

WITH row_validity AS (
	SELECT getDisturbanceCutoff('TE00', 'casfri_validation.attribute_dependencies_test') = 1 AS value
	UNION ALL
	SELECT getDisturbanceCutoff('TE01', 'casfri_validation.attribute_dependencies_test') = 2 AS value
	UNION ALL
	SELECT getDisturbanceCutoff('TE02', 'casfri_validation.attribute_dependencies_test') = 3 AS value
	UNION ALL
	SELECT getDisturbanceCutoff('TE03', 'casfri_validation.attribute_dependencies_test') = 0 AS value
)
INSERT INTO validations_test_results SELECT 'getDisturbanceCutoff' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT value FROM row_validity)
	THEN 'Test passed' ELSE 'Test failed'
END) AS result
FROM row_validity
LIMIT 1;

DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
----------------------------------------------------------------------------------

-- Test for runDisturbanceValidationOnInventory(dest_table_name,inventory,creation_schema, attribute_dependencies_location)
-- NOTE: This is NOT a comprehensive test since there are too many inventories to distinctly test for, this just goes through one possible case
CREATE TABLE IF NOT EXISTS casfri_validation.attribute_dependencies_test (
	inventory_id TEXT,
	layer TEXT,
	"table" TEXT,
	dist_type_1 TEXT,
	dist_year_1 TEXT,
	dist_ext_upper_1 TEXT,
	dist_ext_lower_1 TEXT,
	dist_type_2 TEXT,
	dist_year_2 TEXT,
	dist_ext_upper_2 TEXT,
	dist_ext_lower_2 TEXT,
	dist_type_3 TEXT,
	dist_year_3 TEXT,
	dist_ext_upper_3 TEXT,
	dist_ext_lower_3 TEXT
);

INSERT INTO casfri_validation.attribute_dependencies_test VALUES 
-- Leave out dist_year_2 to have a missing dependency test
('BC80', '1', '''LYR''', 'd1', 'dy1', 'deu1', 'del1', 'd2', '', 'deu2', 'del2', '', '', '', '');

CREATE TABLE IF NOT EXISTS casfri_validation.bc80_raw_dst_all_joined (
	cas_id TEXT,
	source_layer TEXT,
	dist_type_1 TEXT,
	dist_year_1 TEXT,
	dist_ext_upper_1 TEXT,
	dist_ext_lower_1 TEXT,
	dist_type_2 TEXT,
	dist_year_2 TEXT,
	dist_ext_upper_2 TEXT,
	dist_ext_lower_2 TEXT,
	dist_type_3 TEXT,
	dist_year_3 TEXT,
	dist_ext_upper_3 TEXT,
	dist_ext_lower_3 TEXT,
	raw_d1 TEXT,
	raw_dy1 TEXT,
	raw_deu1 TEXT,
	raw_del1 TEXT,
	raw_d2 TEXT,
	raw_dy2 TEXT,
	raw_deu2 TEXT,
	raw_del2 TEXT
);

INSERT INTO casfri_validation.bc80_raw_dst_all_joined VALUES 
-- FALSE_MAPPED due to no dependency
('BC80-TEST-1', '1', 'BURN', '2000', '50', '25', 'CUT', '1990', '25', '12', 'NULL_VALUE', '-8888', '-8888', '-8888',
'B', '2000', '50', '25', 'CUT', '1990', '25', '12'),
-- FALSE_MAPPED due to null value
('BC80-TEST-2', '1', 'BURN', '2000', '50', '25', 'CUT', '-8888', '25', '12', 'NULL_VALUE', '-8888', '-8888', '-8888',
'B', NULL, '50', '25', 'CUT', '1990', '25', '12'),
-- FALSE_NULLs and translation error
('BC80-TEST-3', '1', 'BURN', '-8888', '50', '25', 'INVALID_VALUE', '-8888', '25', '12', 'NULL_VALUE', '-8888', '-8888', '-8888',
'B', '2000', '50', '25', 'CUT', '1990', '25', '12'),
-- Mapped past maximum possible
('BC80-TEST-4', '1', 'NULL_VALUE', '-8888', '-8888', '-8888', 'NULL_VALUE', '-8888', '-8888', '-8888', 'BURN', '-8888', '-8888', '-8888',
NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_test (cas_id TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT);

SELECT * FROM runDisturbanceValidationOnInventory('validation_error_rows_test', 'BC80', 'casfri_validation', 'casfri_validation.attribute_dependencies_test');

WITH test_result AS (
	SELECT * 
	FROM validation_error_rows_test
), row_validity AS (
	--Checks validity of each row
	SELECT CASE 
		-- Displays true if the row is valid, false otherwise
		WHEN (cas_id = 'BC80-TEST-1' AND table_name = 'dst_all' AND error_type = 'DISTURBANCE_ERROR' AND error_desc = 'dist_year_2 was mapped but a raw value doesnt exist' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-2' AND table_name = 'dst_all' AND error_type = 'DISTURBANCE_ERROR' AND error_desc = 'dist_year_1 was mapped but a raw value doesnt exist' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-3' AND table_name = 'dst_all' AND error_type = 'DISTURBANCE_ERROR' AND error_desc = 'dist_year_1 was null when it could have been mapped from raw' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-3' AND table_name = 'dst_all' AND error_type = 'INVALID_VALUE' AND error_desc = 'dist_type_2 has a translation error' AND source_layer = '1')
		OR   (cas_id = 'BC80-TEST-4' AND table_name = 'dst_all' AND error_type = 'DISTURBANCE_ERROR' AND error_desc = 'A disturbance was mapped when it should have been non-applicable (3-3)' AND source_layer = '1')
		THEN TRUE ELSE FALSE
	END AS row_result
	FROM test_result
)
INSERT INTO validations_test_results  SELECT 'runDisturbanceValidationOnInventory' AS test,
(SELECT CASE
	-- Check that the validity rows are all true (aka valid)
	WHEN true = ALL(SELECT row_result FROM row_validity)
	-- Check that there are 5 distinct error_descs
	AND (SELECT count(DISTINCT error_desc) FROM test_result) = 5
	-- Check that there are exactly 5 rows in the result
	AND (SELECT count(*) FROM test_result) = 5
	THEN 'Test passed' ELSE 'Test failed'
END) AS test_result
FROM row_validity
LIMIT 1;


DROP TABLE IF EXISTS casfri_validation.attribute_dependencies_test;
DROP TABLE IF EXISTS casfri_validation.validation_error_rows_test;
DROP TABLE IF EXISTS casfri_validation.bc80_raw_dst_all_joined;

----------------------------------------------------------------------------------

-- Display results
SELECT * FROM casfri_validation.validations_test_results;
DROP TABLE IF EXISTS casfri_validation.validations_test_results;
