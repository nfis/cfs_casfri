
# Automated validations instructions and help

  

  

### - Search path

Ensure your search path is set to create tables and functions in an appropriate location (likely casfri_validation)

  

Also, some functions used here are in the casfri_metrics schema, so that must be in the path as well

  

Suggested path:

```

SET search_path = casfri_validation, casfri_metrics, casfri50, rawfri, "translation", public;

```

  

### - Ignored columns

  

Add column names to this table below to be ignored by the validation function


These columns are either too complicated to validate in this function, generally this is species, nfl, disturbance, and some various other attributes that shouldn't be validated

This generally does not need to be touched by maintainers.

If this table doesn't exist, recreate it

```

CREATE TABLE IF NOT EXISTS casfri_validation.excluded_validation_columns

(column_name TEXT);

  

INSERT INTO casfri_validation.excluded_validation_columns VALUES

('cas_id'),

('layer'),

('structure_per'),

('inventory_id'),

('stand_structure'),

('num_of_layers'),

('non_for_anth'),

('nat_non_veg'),

('non_for_veg'),

('species_1'),

('species_per_1'),

('species_2'),

('species_per_2'),

('species_3'),

('species_per_3'),

('species_4'),

('species_per_4'),

('species_5'),

('species_per_5'),

('species_6'),

('species_per_6'),

('species_7'),

('species_per_7'),

('species_8'),

('species_per_8'),

('species_9'),

('species_per_9'),

('species_10'),

('species_per_10');

```

### Complex Mapping Attributes

This table stores all attributes for each inventory that have a special function for validation. These special functions are stored in automated_validation_helper_functions.sql

Any attributes not specified here will have the default validations ran on them instead

Run "bash load_tables.sh" to load the table from complexMappingAttributes.csv into the database


### - Raw joined tables

  

Raw joined tables are tables created by joining one of the casfri tables with a raw inventory.

  

The join condition is formed by the cas_id attribute dependency for the inventory in layer 1.

  

These tables do not contain any geometries to save space in the database, and geometries aren't used in validations anyways.

  

The raw joined tables also include a "source layer" which is an indication of which row in attribute dependencies the specific record was translated from. Layers can be reordered after translation due to stanjd heights and other factors, so a the getSourceLayer function is very important to recognize which source layer a row came from.

Note: the raw joined tables can take a LONG time to be generated in large inventories so be careful!


IMPORTANT: If the casfri50 tables are changed, raw joined tables for affected inventories should be dropped so they can be regenerated.

The raw joined tables do NOT automatically refresh when their harmonized data is re-translated

THEY ARE NOT MATERIALIZED VIEWS! (And hence can't be refreshed)

Instead, use these functions to refresh the tables:

```
refreshRawJoinedTable(inventory TEXT, casfri_table TEXT, drop_joined BOOLEAN DEFAULT FALSE)
refreshRawJoinedTablesInInventory(inventory TEXT, drop_joined BOOLEAN DEFAULT FALSE)
refreshRawJoinedTablesInJurisdiction(jurisdiction TEXT, drop_joined BOOLEAN DEFAULT FALSE)

Example:
To refresh all raw joined tables in BC08, call
SELECT * FROM refreshRawJoinedTablesInInventory('BC08', TRUE)
```
  

### - Workflow

  

createRawValidationTableRunner will perform all validation tasks. Running this function is all you need to validate an inventory

  

Steps for how the validation process works:

The function iterates through every provided inventory

For each inventory:

1. Any existing error entries in validation_error_rows are deleted

2. Each of the casfri tables lyr_all, eco_all, nfl_all, and cas_all are processed (dst and geo are intentionally skipped)

3. If the raw joined table for the specific casfri tabe doesn't exist, it creates a new one

4. Any null or errored source layers in the raw joined table are attempted to be updated. Any source_layers that couldn't be updated and are still null will be put into layersnotmapped

5. Basic validations are run on each attribute in the table, except the ones specified in excluded_validation_columns.

6. Then, species, NFL, and disturbance validations are run seperately on the inventory

7. After all inventories are processed, the validation error rows are updated to change numeric error codes into their string counterpart for readability

8. The validation_error_rows_summary view is created/refreshed to view aggregated stats on all errors flagged

The function ends. Optionally, use getinvestigatevalidationrowquery to easily investigate a validation row

  


### - Update/Refreshing Validations

  
Functions are provided to simplify the process of rerunning validations. These are in maintainer_functions.sql

 
- If drop_joined is TRUE, will drop the existing table and then recreate it

- If drop_joined is FALSE, will only attempt to create a new table if it doesn't exist. This functions the same as just calling createRawJoinedTable. This is the default behaviour to avoid destructive actions, but this must be set to TRUE if a table needs to be fully refreshed.

1. refreshRawJoinedTable(inventory TEXT, casfri_table TEXT, drop_joined BOOLEAN)

	Given the name of an inventory and casfri table, recreates the raw_joined_table in the casfri_validation schema
	Example call. This will drop casfri_validation.bc08_raw_cas_all_joined, and then recreate it
	```
	SELECT * FROM refreshRawJoinedTable('BC08', 'cas_all', TRUE)
	```
2. refreshRawJoinedTablesInInventory(inventory TEXT, drop_joined BOOLEAN)

	Given the name of an inventory, recreates all raw_joined_tables in the casfri_validation schema for that inventory
	Example call. This will create any nonexistent casfri_validation.bc08_raw_CASFRI_TABLE_joined tables. Note that the tables aren't dropped since drop_joined = FALSE
	```
	SELECT * FROM refreshRawJoinedTablesInInventory('BC08', FALSE)
	```
3. refreshRawJoinedTablesInJurisdiction(jurisdiction TEXT, drop_joined BOOLEAN)

	Given the name of a jurisdiction, recreates all raw_joined_tables in the casfri_validation schema for that jurisdiction
	Example call. This will drop all casfri_validation.bcXX_raw_CASFRI_TABLE_joined tables and then recreate them
	```
	SELECT * FROM refreshRawJoinedTablesInJurisdiction('BC', TRUE)
	```
4. refreshValidationsOnInventory(inventory TEXT, drop_joined BOOLEAN)

	Given the name of an inventory, recreates all raw_joined_tables in the casfri_validation schema for that inventory, and runs all validations on it
	Example call. This will create any nonexistent casfri_validation.bc08_raw_CASFRI_TABLE_joined tables. Then, it will run all validations on the inventory. Note that the tables aren't dropped since drop_joined = FALSE
	```
	SELECT * FROM refreshValidationsOnInventory('BC08', FALSE)
	```
5. refreshValidationsOnJurisdiction(jurisdiction TEXT, drop_joined BOOLEAN)

	Given the name of a jurisdiction, recreates all raw_joined_tables in the casfri_validation schema for that jurisdiction, and runs all validations on it
	Example call. This will drop all casfri_validation.bcXX_raw_CASFRI_TABLE_joined tables and then recreate them. Then, it will run all validations on the inventory.
	```
	SELECT * FROM refreshValidationsOnJurisdiction('BC', TRUE)
	```
6. refreshAllValidations(drop_joined BOOLEAN)

	Recreates all raw_joined_tables in the casfri_validation schema, and reruns all validations
	Example call. This will create any nonexistent raw joined tables. Then, it will run all validations on every inventory in CASFRI. Note that the tables aren't dropped since drop_joined = FALSE
	```
	SELECT * FROM refreshAllValidations(FALSE)
	```

### - Aggregated Error Stats

validation_error_rows_summary contains aggregated statistics on each error caught by validations.

These stats are stored in a table rather than a materialized view due to speed up updates and refreshes for individual inventories/jurisdictions, since reaggregating everything can take a long time.

The createRawValidationTableRunner() and refreshValidation functions will call these functions automatically after a validation has been run

1. refreshValidationSummaryAll()
	Completely recreates the validation_error_rows_summary table
	Example call.
	```
	SELECT * FROM refreshValidationSummaryAll()
	```
	
2. refreshValidationSummaryForJurisdiction(jurisdiction TEXT)
	Deletes and then recreates all entries for jurisdiction in the validation_error_rows_summary table
	Example call.
	```
	SELECT * FROM refreshValidationSummaryForJurisdiction('BC')
	```
	
3. refreshValidationSummaryForInventory(inventory TEXT)
	Deletes and then recreates all entries for inventory in the validation_error_rows_summary table
	Example call.
	```
	SELECT * FROM refreshValidationSummaryForInventory('BC08')
	```

Run this to view the validation rows summary, but with a manual validation query
Copy paste and run the validation query to view the (likely) suspect attributes causing the error
getinvestigatevalidationrowquery is used in the grafana validation inspector to display more information about an error

```
SELECT *, getinvestigatevalidationrowquery(table_name, error_desc, source_layer, example_cas_id, 'translation.attribute_dependencies') AS manual_validation_query
FROM validation_error_rows_summary;
```

### - Error types

  

#### Translation Error

  

Translation errors are any error codes found that indicate that an error occured during translation of an attribute. This includes OUT_OF_RANGE, NOT_IN_SET, INVALID_VALUE, WRONG_TYPE, INVALID_GEOMETRY, NO_INTERSECT, and TRANSLATION_ERROR error codes. These errors are generally expected behaviour, but if an entire inventory fills an attribute with translation errors, then we may want to change the expected behaviour.

  

#### Missing Error

  

Missing errors are when an attribute does NOT contain an error code, however it probably should be an error code. This happens either when a dependency doesn't exist for that attribute in that inventory, or when the raw value is empty.

  

#### False Null

  

A false null error is the inverse of a missing error. This occurs when the harmonized value is a NULL error code but a dependency and raw value exists. A NULL error code includes any of EMPTY_STRING, NULL_VALUE, NOT_APPLICABLE, UNKNOWN_VALUE.

  

#### Incorrect Mapping

Incorrect mapping errors specifically occur when an attribute was mapped to the wrong value. Not ALL attributes check this, but some will flag when the value was translated incorrectly.

  

#### Species Error

Species errors are any errors pertaining to species codes and percentages. These errors include missing errors, null errors, and species mapped past the maximum possible species count in an inventory. The error description must be used to differentiate between these errors.

  

#### Disturbance Error

Disturbance errors are any errors pertaining to disturbance types, years, and extents. These errors include missing errors, null errors, and disturbances mapped past the maximum possible disturbance count in an inventory. The error description must be used to differentiate between these errors.

  

#### NFL Error

  

NFL errors are any errors pertaining to the three NFL types, nat_non_veg, non_for_anth, and non_for_veg. These errors include missing errors and null errors. The error description must be used to differentiate between these errors.

  

#### Validation Error

Validation errors indicate an issue in the validation script itself, not on the status of the CASFRI database. These errors should not occur if the validation script is fully functioning.

  

#### Row Translation Error

  

Row translation errors indicate an issue in how a row was validated, so this may mean that a row was translated when it shouldn't have, or that layers are being miscounted.

  

  

### - Ingesting a new inventory/validating a new inventory:

**- If it has a standard/translation table that already exists:**

1. Go to getSourceLayer() and find rules for the other inventories in that standard and make sure that the new standard is included and that the rules make sense

2. Similarly for updateSourceLayers(), must check that the inventory is included in the correct rule

3. Go to runSpeciesValidationOnInventory() and ensure that the inventory is included in the corresponding rule

	3.1. If the inventory uses a single string to represent species composition, must also check singleSpeciesRowValidity()

4. Go to runDisturbanceValidationOnInventory() and ensure that the inventory is included in the corresponding rule

5. Go to automated-validation-helper-functions.sql. If any other inventories in the same standard are in the complex mappings attributes list, add the new inventory to the list. Then, must check that the helper function correctly handles the inventory

6. After these functions have been checked and updated, run the main createRawValidationTableRunner() function. This will first attempt to create a raw_joined table for each of the CASFRI tables, then update the source layers, then run all validations on the inventory. Troubleshoot any errors that may occur.

7. Check layersnotmapped. Are there any new entries? if so, getSourceLayer() is not functioning properly for this inventory. Fix the issue, delete the entries, and rerun.

8. Inspect the results in validation_error_rows_summary. Are there any new flagged validations that don't make sense?

9. Run all automated_validations test scripts. Did any tests fail? Investigate and potentially refactor tests to suit new function behaviour.

**- If it has a new standard/translation table:**

1. Go to getSourceLayer() and inspect the attribute dependencies table. Compare with how other similar inventories determine source layers and either add a new case for the new inventory or ingest it into other rules.

2. Go to updateSourceLayers() and make sure that every used variable in getSourceLayer() is being passed into the function for that inventory

3. Go to runSpeciesValidationOnInventory() and inspect the attribute dependencies table for species mappings. Determine if the species mappings use a single string representation or multiple mappings and add the inventory accordingly

	3.1. If the inventory uses a single string to represent species composition, must also check singleSpeciesRowValidity() and update accordingly

4. Go to runDisturbanceValidationOnInventory() and either add the inventory to one of the existing rules, or create a new case if needed

5. Go to automated-validation-helper-functions.sql. If the standard has multi mappings in attribute dependencies, then must add or edit helper functions to validate the attribute, and add the attribute to the complex mappings attribute list.

6. After these functions have been checked and updated, run the main createRawValidationTableRunner() function. This will first attempt to create a raw_joined table for each of the CASFRI tables, then update the source layers, then run all validations on the inventory. Troubleshoot any errors that may occur.

	6.1. For a fresh translation table, it is likely that errors will occur the first time running. Keep an eye on the 'RAISE NOTICE' statements to determine where the error occurred

7. Check layersnotmapped. Are there any new entries? If so, getSourceLayer() is not functioning properly for this inventory. Fix the issue, delete the entries, and rerun.

8. Inspect the results in validation_error_rows_summary. Are there any new flagged validations that don't make sense? Add special case functions to complex mappings attributes where needed.

9. Run all automated_validations test scripts. Did any tests fail? Investigate and potentially refactor tests to suit new function behaviour.


### - Grafana

There are two dashboards on Grafana relating to validations

1. Validation
	
This dashboard keeps generic stats about the overall state of validations, such as aggregated stats on error types and counts of each error code.

There is also a table that displays the largest count errors. Click on "View" next to an error to redirect to the Validation Inspector.

2. Validation Inspector

This dashboard is intended to give information about a specific error code, and allows for searching of all error codes.

It shows the specifics of the error code, the relevant entries in attribute dependencies and translation table, and gives an example row from the raw joined table where the error occurs.

At the bottom of a page is a table of all aggregated errors which can be filtered, searched, and clicking "View" on a particular error will update the dashboard to show information about that error.



### - Known Issues/To-do

1. getSourceLayers sometimes mislabels source layers in BC inventories in the lyr_all table. This is when the layer rank, height, and crown closure is exactly the same between layers, which is quite rare
2. May want to look into refactoring the raw-joined tables to be materialized views instead of tables. Not sure on feasibility since the source layers need to be updated after the fact currently.
3. Use standard names rather than inventory IDs to simplify new inventory ingestion
4. Make it easier to run individual categories of validations on a specific inventory rather than rerunning all of them
5. stand_structure, structure_per, and structure_range still aren't being validated
6. NFL crown closure in NT is currently being mapped to the LYR crown closure value, but this isn't being accurately represented in attribute dependencies or the translation tables
7. If TIMESTAMPTZ fields behave strangely on different machines, check that the timezone in the client is set the same everywhere. Boreal uses UTC. This was noticed in BC origin validations where dates would be inconsistent on Boreal compared to a local instance on DBeaver since they had different timezones.

