-- getNFLMatchList(inventory, nfl_col, source_layer)
-- Given an inventory, NFL column name, and source layer, gives the list of values that the raw attribute must match with to be transalted to a NFL type
-- Input:
	-- inventory: the inventory id that records NFL types
	-- nfl_col: the name of the NFL column (nat_non_veg, non_for_anth, or non_for_veg)
	-- source_layer: the layer the row was mapped from
-- Output: TEXT representation of the value match list
CREATE OR REPLACE FUNCTION getNFLMatchList(
inventory TEXT,
nfl_col TEXT,
source_layer TEXT)
RETURNS TEXT AS
	$func$
	BEGIN
		CASE 
		WHEN inventory = 'AB29' AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''NMB'',''NWF'',''NWL'',''NWI'',''NWR'',''NMC'',''NMR'',''NMS'',''MB'',''MC'',''MS'',''WF'',''WL'',''WR'',''NMG'',''NMM''}';
		WHEN inventory = 'AB29' AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '''{''''CIP'''',''''CIW'''',''''CA'''',''''CP'''',''''CPR'''',''''IG'''',''''IH'''',''''II'''',''''SR'''',''''CIU''''}'', ''{''''ASC'''',''''ASR'''',''''AIG'''',''''AIH'''',''''AIE'''',''''AIF'''',''''AIM'''',''''AII'''',''''IW'''',''''PR'''',''''IP'''',''''P'''',''''A'''',''''AIU'''',''''AIW''''}''';
		WHEN inventory = 'AB29' AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''HF'',''HG'',''SC'',''SO'',''BR''}';
		WHEN inventory IN ('MB02', 'MB04', 'MB06', 'MB07', 'MB10', 'MB12') AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''NMB'',''NMC'',''NMR'',''NMS'',''NMG'',''NWL'',''NWR'',''NWW'',''NMM'',''NMO'',''NWE'',''NWA'',''NSL'',''NMF''}';
		WHEN inventory IN ('MB02', 'MB04', 'MB06', 'MB07', 'MB10', 'MB12') AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''CP'', ''CA'', ''CPR'', ''ASB'', ''AFL'', ''ADD'', ''CIP'',''CIW'',''CIU'',''ASC'',''ASR'',''ASP'',''ASN'',''AIH'',''AIR'', ''AAR'', ''AIG'',''AII'',''AIW'',''AIA'',''AIF'',''AIU''}';
		WHEN inventory IN ('MB02', 'MB04', 'MB06', 'MB07', 'MB10', 'MB12') AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''SO'',''SO1'',''SO2'',''SO3'',''SO4'',''SO5'',''SO6'',''SO7'',''SO8'',''SO9'',''AL'',''SC'',''SC1'',''SC2'',''SC3'',''SC4'',''SC5'',''SC6'',''SC7'',''SC8'',''SC9'',''CC'',''HG'',''CS'',''HF'',''AS'',''HU'',''VI'',''BR'',''RA'',''CL'',''DL'',''AU''}';
		WHEN inventory IN ('MB03', 'MB11', 'MB13') AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{802,803,804,838,839,848,900,901,991,992,993,994,995}';
		WHEN inventory IN ('MB03', 'MB11', 'MB13') AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{810,811,812,813,815,816,841,842,843,844,845,846,847,849,851}';
		WHEN inventory IN ('MB03', 'MB11', 'MB13') AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{801,821,822,823,824,830,831,832,835}';
		WHEN left(inventory, 2) = 'NT' AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''BE'',''BR'',''BU'',''CB'',''ES'',''LA'',''LL'',''LS'',''MO'',''MU'',''PO'',''RE'',''RI'',''RO'',''RS'',''RT'',''SW''}';
		WHEN left(inventory, 2) = 'NT' AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''AP'',''BP'',''EL'',''GP'',''TS'',''RD'',''SH'',''SU'',''PM''}';
		WHEN left(inventory, 2) = 'NT' AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''BL'',''BM'',''BY'',''HE'',''HF'',''HG'',''SL'',''ST''}';
		WHEN LEFT(inventory, 2) = 'QC' AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''BAT'',''DS'',''EAU'',''ILE'',''INO''}';
		WHEN LEFT(inventory, 2) = 'QC' AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''AL'',''DH'',''NX'',''IMP''}';
		WHEN inventory IN ('QC03', 'QC08') AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''A'',''AEP'',''AER'',''AF'',''ANT'',''BAS'',''CFO'',''CU'',''DEF'',''DEP'',''GR'',''HAB'',''LTE'',''MI'',''NF'',''RO'',''US'',''VIL'',''BHE'',''BLE'',''CAM'',''CAR'',''CEX'',''CHE'',''CIM'',''CNE'',''CS'',''CV'',''DEM'',''GOL'',''PIC'',''PPN'',''QUA'',''SC'',''TOE'',''VRG'',''OBS'',''PAI''}';
		WHEN inventory IN ('QC04', 'QC09') AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''A'',''AEP'',''AER'',''AF'',''ANT'',''BAS'',''CFO'',''CU'',''DEF'',''DEP'',''GR'',''HAB'',''LTE'',''MI'',''NF'',''RO'',''US'',''VIL'',''BHE'',''BLE'',''CAM'',''CAR'',''CEX'',''CHE'',''CIM'',''CNE'',''CS'',''CV'',''DEM'',''GOL'',''PIC'',''PPN'',''QUA'',''SC'',''TOE'',''VRG''}';
		WHEN inventory IN ('QC05', 'QC10') AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''A'',''AEP'',''AER'',''AF'',''ANT'',''BAS'',''CFO'',''CU'',''DEF'',''DEP'',''GR'',''HAB'',''LTE'',''MI'',''NF'',''RO'',''US'',''VIL''}';
		WHEN LEFT(inventory, 2) = 'NS' AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{71,76,77,78,84,85,94}';
		WHEN LEFT(inventory, 2) = 'NS' AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{5,86,87,91,92,93,95,96,97,98,99}';
		WHEN LEFT(inventory, 2) = 'NS' AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{70,72,74,75,83,88,89}';
		WHEN inventory = 'PE01' AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''SO'',''SD'',''WW'',''FL''}';
		WHEN inventory = 'PE01' AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''CL'',''WF'',''PL'',''RN'',''RD'',''RR'',''AG'',''EP'',''UR''}';
		WHEN inventory = 'PE01' AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''BO''}';
		WHEN inventory IN ('PE02', 'PE03', 'PE04') AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''BAR'',''BSB'',''SDW'',''WWW'',''WAT''}';
		WHEN inventory IN ('PE02', 'PE03', 'PE04') AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''AGR'',''COM'',''RES'',''IND'',''NON'',''REC'',''TRN'',''URB'',''INT''}';
		WHEN inventory IN ('PE02', 'PE03', 'PE04') AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''BOW''}';
		WHEN inventory = 'YT03' AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''Lake'',''River'',''Beach'',''Bedrock'',''Cut Bank'',''Exposed Soil'',''Pond/Lake Sediment'',''River Sediment'',''Rubble'',''Sand'',''Talus'',''Snow/Ice'',''Exposed Land''}';
		WHEN inventory = 'YT03' AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''Airport'',''Anthropogenic Other'',''Cultivated'',''Generic Clearing'',''Gravel Pit'',''Industrial Corridor'',''Industrial Site'',''Mine Site'',''Mine Tailings'',''Railway'',''Road'',''Rural Residential'',''Seismic'',''Tower Site'', ''Urban/Settlement''}';
		WHEN inventory = 'YT03' AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''Low Shrub'',''Tall Shrub'',''Bryoid'',''Forb'',''Graminoid'',''Herb'',''Lichen'',''Moss''}';
		
		WHEN inventory = 'YT04' AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''NW'',''NS'',''NE''}';
		WHEN inventory = 'YT04' AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''NU''}';
		WHEN inventory = 'YT04' AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''VN''}';
		WHEN inventory = 'ON02' AND nfl_col = 'nat_non_veg'
		THEN 
			RETURN '{''ISL'',''RCK'',''WAT''}';
		WHEN inventory = 'ON02' AND nfl_col = 'non_for_anth'
		THEN 
			RETURN '{''DAL'',''UCL'',''RRW''}';
		WHEN inventory = 'ON02' AND nfl_col = 'non_for_veg'
		THEN 
			RETURN '{''GRS'',''OMS''}';
		ELSE 
			RETURN NULL;
		END CASE;
		RETURN NULL;
	END;
	$func$
LANGUAGE 'plpgsql';



-- runNFLValidationOnInventory(dest_table_name, inventory, creation_schema, attribute_dependencies_location)
-- Given a specific inventory, runs NFL validations on the inventory and stores flagged errors in the dest_table
-- Collects any generic validation errors and NFL types that were either mapped when a raw value doesn't exist, or mapped to an error code when a raw value does exists
-- Input:
	-- dest_table_name: the name of the table to store flagged errors in
	-- inventory: the inventory that records NFL rows
	-- creation_schema: the schema that the dest_table and raw_joined tables are in
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
-- Output: VOID 
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runNFLValidationOnInventory(
dest_table_name TEXT,
inventory TEXT,
creation_schema TEXT, 
attribute_dependencies_location TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		raw_merged_table TEXT;
		nfl_col TEXT;
		nfl_dependency TEXT;
		i INTEGER;
		validation_cond TEXT;
	BEGIN
		raw_merged_table := inventory || '_raw_nfl_all_joined';
		RAISE NOTICE 'Run NFL Validation- (%) Function started for inventory %', LEFT(clock_timestamp()::TEXT, 19), inventory;
		RAISE NOTICE 'Run NFL Validation- (%) 	Finding NFL null errors', LEFT(clock_timestamp()::TEXT, 19);
		-- Process basic inventories with simple mappings
		IF left(inventory, 2) IN ('MB', 'NT', 'QC', 'NS', 'PE', 'ON', 'YT')
		THEN 
			RAISE NOTICE 'Run NFL Validation- (%) 		Inventory % uses simple NFL mappings', LEFT(clock_timestamp()::TEXT, 19), inventory;
			FOR nfl_col IN (VALUES ('nat_non_veg'), ('non_for_anth'), ('non_for_veg'))
			LOOP
				RAISE NOTICE 'Run NFL Validation- (%) 		Checking %', LEFT(clock_timestamp()::TEXT, 19), nfl_col;
				FOR i IN 1..10
				LOOP
					EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''%s'', ''%s'', %s, ''nfl_all'')', inventory, nfl_col, attribute_dependencies_location, i) INTO nfl_dependency;
					EXIT WHEN nfl_dependency IS NULL;
					nfl_dependency := trim(split_part(nfl_dependency, ',', 1));
					-- Catch falsely mapped NFLs when a dependency doesn't exist
					IF trim(nfl_dependency) IN ('', 'NULL')
					THEN
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(%s::text) = FALSE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, i);
					ELSIF inventory = 'AB29' AND nfl_col = 'non_for_anth'
					THEN 
						IF i = 3
						THEN 
							nfl_dependency = 'raw_anth_veg::text, raw_anth_non::text';
						ELSE
							nfl_dependency = 'uraw_anth_veg::text, raw_uanth_non::text';
						END IF;
						-- Catch falsely null NFLs when a NFL could have been mapped
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was null when it could have been mapped from raw'' AS error_desc, source_layer
										FROM %s.%s
										WHERE %s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-9999'', ''-8886'', ''-8887'') AND 
										tt_matchlisttwice(%s, getNFLMatchList(''%s'', ''%s'', ''1''), ''FALSE'', ''FALSE'', ''TRUE'', ''FALSE'') = TRUE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, nfl_dependency, inventory, nfl_col, i);
						-- Catch falsely mapped NFLs when the raw value is null
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(%s::text) = FALSE AND 
										tt_matchlist(%s, getNFLMatchList(''%s'', ''%s'', ''1''), ''FALSE'', ''FALSE'', ''TRUE'', ''FALSE'') = FALSE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, nfl_dependency, inventory, nfl_col, i);
					ELSE
						-- Catch falsely null NFLs when a NFL could have been mapped
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was null when it could have been mapped from raw'' AS error_desc, source_layer
										FROM %s.%s
										WHERE %s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-9999'', ''-8886'', ''-8887'') AND 
										tt_matchlist(trim(raw_%s::text), getNFLMatchList(''%s'', ''%s'', ''1''), ''FALSE'', ''FALSE'', ''TRUE'', ''FALSE'') = TRUE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, nfl_dependency, inventory, nfl_col, i);
						-- Catch falsely mapped NFLs when the raw value is null
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(%s::text) = FALSE AND 
										tt_matchlist(trim(raw_%s::text), getNFLMatchList(''%s'', ''%s'', ''1''), ''FALSE'', ''FALSE'', ''TRUE'', ''FALSE'') = FALSE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, nfl_dependency, inventory, nfl_col, i);
					END IF;
				END LOOP;
			END LOOP;
		ELSIF left(inventory, 2) = 'BC'
		THEN
			RAISE NOTICE 'Run NFL Validation- (%) 		Inventory % uses complex BC NFL mappings', LEFT(clock_timestamp()::TEXT, 19), inventory;
			FOR nfl_col IN (VALUES ('nat_non_veg'), ('non_for_anth'), ('non_for_veg'))
			LOOP
				RAISE NOTICE 'Run NFL Validation- (%) 		Checking %', LEFT(clock_timestamp()::TEXT, 19), nfl_col;
				FOR i IN 1..10
				LOOP
					EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''%s'', ''%s'', %s, ''nfl_all'')', inventory, nfl_col, attribute_dependencies_location, i) INTO nfl_dependency;
					EXIT WHEN nfl_dependency IS NULL;
					nfl_dependency := trim(split_part(nfl_dependency, ',', 1));
					IF nfl_col = 'nat_non_veg'
					THEN
						validation_cond := 'translation.tt_vri01_nat_non_veg_validation(raw_inventory_standard_cd, raw_land_cover_class_cd_1, raw_bclcs_level_4, raw_non_productive_descriptor_cd, raw_non_veg_cover_type_1)';
					ELSIF nfl_col = 'non_for_anth'
					THEN
						validation_cond := 'translation.tt_vri01_non_for_anth_validation(raw_inventory_standard_cd, raw_land_cover_class_cd_1, raw_non_productive_descriptor_cd, raw_non_veg_cover_type_1)';
					ELSIF nfl_col = 'non_for_veg'
					THEN
						validation_cond := 'translation.tt_vri01_non_for_veg_validation(raw_inventory_standard_cd, raw_land_cover_class_cd_1, raw_bclcs_level_4, raw_non_productive_descriptor_cd)';
					END IF;
					-- Catch falsely mapped NFLs when a dependency doesn't exist
					IF trim(nfl_dependency) IN ('', 'NULL')
					THEN
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE (isError(%s::text) = FALSE) AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, i);
					ELSE
						-- Catch falsely null NFLs when a NFL could have been mapped
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was null when it could have been mapped from raw'' AS error_desc, source_layer
										FROM %s.%s
										WHERE %s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-9999'', ''-8886'', ''-8887'') AND %s = TRUE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, validation_cond, i);               
						-- Catch falsely mapped NFLs when the raw value is null
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(%s::text) = FALSE AND %s = FALSE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, validation_cond, i);
					END IF;
				END LOOP;
			END LOOP;
		ELSIF left(inventory, 2) = 'NB'
		THEN 
			RAISE NOTICE 'Run NFL Validation- (%) 		Inventory % uses complex NB mappings', LEFT(clock_timestamp()::TEXT, 19), inventory;
			FOR nfl_col IN (VALUES ('nat_non_veg'), ('non_for_anth'), ('non_for_veg'))
			LOOP
				RAISE NOTICE 'Run NFL Validation- (%) 		Checking %', LEFT(clock_timestamp()::TEXT, 19), nfl_col;
				FOR i IN 1..10
				LOOP
					EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''%s'', ''%s'', %s, ''nfl_all'')', inventory, nfl_col, attribute_dependencies_location, i) INTO nfl_dependency;
					EXIT WHEN nfl_dependency IS NULL;
					IF i = 3 AND inventory = 'NB02'
					THEN 
						nfl_dependency := 'raw_slu::text, raw_waterdefin::text';
					ELSIF i = 3 AND inventory = 'NB03'
					THEN 
						nfl_dependency := 'raw_slu::text, raw_water_code::text';
					ELSE
						nfl_dependency := '';
					END IF;
					IF nfl_col = 'nat_non_veg'
					THEN
						validation_cond := format('translation.tt_matchList(''{''''''||COALESCE(%s)||''''''}'',''{''''BL'''',''''RF'''',''''RO'''', ''''LK'''', ''''RV'''', ''''ON'''', ''''PN'''', ''''SL'''', ''''WA'''', 4, 6, 7, 8, 9, 100, 416}'')', nfl_dependency);
					ELSIF nfl_col = 'non_for_anth'
					THEN
						validation_cond := format('translation.tt_matchList(''{''''''||COALESCE(%s)||''''''}'',''{''''AI'''',''''AR'''',''''BA'''',''''CB'''',''''CG'''',''''CH'''',''''CL'''',''''CO'''',''''CS'''',''''CT'''',''''EA'''',''''FD'''',''''FP'''',''''GC'''',''''GP'''',''''IP'''',''''IZ'''',''''LE'''',''''LF'''',''''MI'''',''''PA'''',''''PB'''',''''PP'''',''''PR'''',''''QU'''',''''RD'''',''''RR'''',''''RU'''',''''RY'''',''''SG'''',''''SK'''',''''TM'''',''''TR'''',''''UR'''',''''WR'''',''''AQ'''',415,''''BR'''',''''BW'''',''''DM'''',''''GR'''',''''P1'''',''''P2'''',''''WF''''}'')', nfl_dependency);
					ELSIF nfl_col = 'non_for_veg'
					THEN
						validation_cond := format('translation.tt_matchList(''{''''''||COALESCE(%s)||''''''}'',''{''''BO''''}'')', nfl_dependency);
					END IF;  
					-- Catch falsely mapped NFLs when a dependency doesn't exist
					IF trim(nfl_dependency) IN ('', 'NULL')
					THEN
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE (isError(%s::text) = FALSE) AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, i);
					ELSE
						-- Catch falsely null NFLs when a NFL could have been mapped
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was null when it could have been mapped from raw'' AS error_desc, source_layer
										FROM %s.%s
										WHERE %s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-9999'', ''-8886'', ''-8887'') AND %s = TRUE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, validation_cond, i);               
						-- Catch falsely mapped NFLs when the raw value is null
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''nfl_all'' AS table_name, ''NFL_ERROR'' AS error_type, ''%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(%s::text) = FALSE AND %s = FALSE AND source_layer = ''%s'')', 
										creation_schema, dest_table_name, nfl_col, creation_schema, raw_merged_table, nfl_col, validation_cond, i);
					END IF;
				END LOOP;
			END LOOP;
		END IF;
		-- Find true translation errors- these should all count as true errors
		RAISE NOTICE 'Run NFL Validation- (%) 	Finding NFL translation errors in %', LEFT(clock_timestamp()::TEXT, 19), inventory;
		FOR nfl_col IN (VALUES ('nat_non_veg'), ('non_for_anth'), ('non_for_veg'))
		LOOP
			RAISE NOTICE 'Run NFL Validation- (%) 		Checking %', LEFT(clock_timestamp()::TEXT, 19), nfl_col;
			EXECUTE format('INSERT INTO %s.%s
							SELECT cas_id, ''nfl_all'' AS table_name,  %s::text AS error_type, ''%s has a translation error'' AS error_desc, source_layer 
							FROM %s.%s
							WHERE isTranslationError(s::text) = TRUE', 
							creation_schema, dest_table_name, nfl_col, nfl_col, creation_schema, raw_merged_table, nfl_col);
		END LOOP;
		RAISE NOTICE 'Run NFL Validation- (%) Function complete', LEFT(clock_timestamp()::TEXT, 19);
	END;
	$func$
LANGUAGE 'plpgsql';

