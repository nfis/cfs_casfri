
-- refreshRawJoinedTable(inventory TEXT, casfri_table TEXT, drop_joined BOOLEAN)
-- Given the name of an inventory and casfri table, recreates the raw_joined_table in the casfri_validation schema
-- If drop_joined is TRUE, will drop the existing table and then recreate it
-- If drop_joined is FALSE, will only attempt to create a new table if it doesn't exist. This functions the same as just calling createRawJoinedTable
-- Input:
	-- inventory: the inventory of the table to drop. Ex: 'BC08'
	-- casfri_table: teh casfri_table of the table to drop. Ex: 'cas_all'
	-- drop_joined: boolean to represent whether or not to drop the table. Default is FALSE to avoid accidental destructive actions
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshRawJoinedTable(inventory TEXT, casfri_table TEXT, drop_joined BOOLEAN DEFAULT FALSE)
RETURNS VOID AS
	$func$
	BEGIN
		IF drop_joined
		THEN
			EXECUTE format('DROP TABLE IF EXISTS casfri_validation.%s_raw_%s_joined CASCADE', inventory, casfri_table);
		END IF;
		EXECUTE format('SELECT * FROM createRawJoinedTable(''%s'', ''casfri50'', ''%s'', ''rawfri'', ''translation.attribute_dependencies'', ''casfri_validation'')', casfri_table, inventory);
	END	
	$func$
LANGUAGE 'plpgsql';


-- refreshRawJoinedTablesInInventory(inventory TEXT, drop_joined BOOLEAN)
-- Given the name of an inventory, recreates all raw_joined_tables in the casfri_validation schema for that inventory
-- If drop_joined is TRUE, will drop the existing tables and then recreate them
-- If drop_joined is FALSE, will only attempt to create a new table if it doesn't exist. This functions the same as just calling createRawJoinedTable
-- Input:
	-- inventory: the inventory of the table to drop. Ex: 'BC08'
	-- drop_joined: boolean to represent whether or not to drop the table. Default is FALSE to avoid accidental destructive actions
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshRawJoinedTablesInInventory(inventory TEXT, drop_joined BOOLEAN DEFAULT FALSE)
RETURNS VOID AS
	$func$
	DECLARE
		casfri_table TEXT;
	BEGIN
		FOR casfri_table IN (VALUES ('lyr_all'), ('eco_all'), ('nfl_all'), ('cas_all'), ('dst_all'))
		LOOP
			EXECUTE format('SELECT * FROM refreshRawJoinedTable(''%s'', ''%s'', %s)', inventory, casfri_table, drop_joined::TEXT);
		END LOOP;
	END	
	$func$
LANGUAGE 'plpgsql';



-- refreshRawJoinedTablesInJurisdiction(jurisdiction TEXT, drop_joined BOOLEAN)
-- Given the name of a jurisdiction, recreates all raw_joined_tables in the casfri_validation schema for that jurisdiction
-- If drop_joined is TRUE, will drop the existing tables and then recreate them
-- If drop_joined is FALSE, will only attempt to create a new table if it doesn't exist. This functions the same as just calling createRawJoinedTable
-- Input:
	-- jurisdiction: the jurisdiction of the table to drop. Ex: 'BC'
	-- drop_joined: boolean to represent whether or not to drop the table. Default is FALSE to avoid accidental destructive actions
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshRawJoinedTablesInJurisdiction(jurisdiction TEXT, drop_joined BOOLEAN DEFAULT FALSE)
RETURNS VOID AS
	$func$
	DECLARE
		inventory_record RECORD;
	BEGIN
		FOR inventory_record IN 
		EXECUTE format('SELECT * FROM getListInvs() WHERE left(inventory,2) = ''%s''', jurisdiction)
		LOOP
			EXECUTE format('SELECT * FROM refreshRawJoinedTablesInInventory(''%s'', %s)', inventory_record.inventory, drop_joined::TEXT);
		END LOOP;
	END	
	$func$
LANGUAGE 'plpgsql';


-- refreshValidationsOnInventory(inventory TEXT, drop_joined BOOLEAN)
-- Given the name of an inventory, recreates all raw_joined_tables in the casfri_validation schema for that inventory, and runs all validations on it
-- If drop_joined is TRUE, will drop the existing tables and then recreate them
-- If drop_joined is FALSE, will only attempt to create a new table if it doesn't exist
-- Input:
	-- inventory: the inventory of the table to drop. Ex: 'BC08'
	-- drop_joined: boolean to represent whether or not to drop the table. Default is FALSE to avoid accidental destructive actions
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshValidationsOnInventory(inventory TEXT, drop_joined BOOLEAN DEFAULT FALSE)
RETURNS VOID AS
	$func$
	DECLARE
		table_name TEXT;
	BEGIN
		RAISE NOTICE '%', format('SELECT * FROM dropRawJoinedTables(''casfri_validation'', ''getListInvs() WHERE inventory = ''''%s'''''')', inventory);
		IF drop_joined
		THEN
			EXECUTE format('SELECT * FROM dropRawJoinedTables(''casfri_validation'', ''getListInvs() WHERE inventory = ''''%s'''''')', inventory);
		END IF;
		EXECUTE format('SELECT * FROM createRawValidationTableRunner(''validation_error_rows'', ''casfri_validation'', ''casfri50'', 
						''getListInvs() WHERE inventory = ''''%s'''''', 
						''rawfri'', ''translation.attribute_dependencies'', ''casfri_validation.excluded_validation_columns'', ''casfri_validation.complexMappingAttributes'')', inventory);
	END	
	$func$
LANGUAGE 'plpgsql';


-- refreshValidationsOnJurisdiction(jurisdiction TEXT, drop_joined BOOLEAN)
-- Given the name of an jurisdiction, recreates all raw_joined_tables in the casfri_validation schema for that jurisdiction, and runs all validations on it
-- If drop_joined is TRUE, will drop the existing tables and then recreate them
-- If drop_joined is FALSE, will only attempt to create a new table if it doesn't exist
-- Input:
	-- jurisdiction: the jurisdiction of the table to drop. Ex: 'BC'
	-- drop_joined: boolean to represent whether or not to drop the table. Default is FALSE to avoid accidental destructive actions
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshValidationsOnJurisdiction(jurisdiction TEXT, drop_joined BOOLEAN DEFAULT FALSE)
RETURNS VOID AS
	$func$
	DECLARE
		table_name TEXT;
		inventory TEXT;
	BEGIN
		IF drop_joined
		THEN
			EXECUTE format('SELECT * FROM dropRawJoinedTables(''casfri_validation'', ''getListInvs() WHERE left(inventory,2) = ''''%s'''''')', jurisdiction);
		END IF;
		EXECUTE format('SELECT * FROM createRawValidationTableRunner(''validation_error_rows'', ''casfri_validation'', ''casfri50'', 
						''getListInvs() WHERE left(inventory,2) = ''''%s'''''', 
						''rawfri'', ''translation.attribute_dependencies'', ''casfri_validation.excluded_validation_columns'', ''casfri_validation.complexMappingAttributes'')', jurisdiction);
	END	
	$func$
LANGUAGE 'plpgsql';


-- refreshAllValidations(drop_joined BOOLEAN)
-- Recreates all raw_joined_tables in the casfri_validation schema, and reruns all validations
-- If drop_joined is TRUE, will drop the existing tables and then recreate them
-- If drop_joined is FALSE, will only attempt to create a new table if it doesn't exist
-- Input:
	-- drop_joined: boolean to represent whether or not to drop the table. Default is FALSE to avoid accidental destructive actions
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshAllValidations(drop_joined BOOLEAN DEFAULT FALSE)
RETURNS VOID AS
	$func$
	BEGIN
		IF drop_joined
		THEN
			EXECUTE format('SELECT * FROM dropRawJoinedTables(''casfri_validation'', ''getListInvs()'')');
		END IF;
		EXECUTE format('SELECT * FROM createRawValidationTableRunner(''validation_error_rows'', ''casfri_validation'', ''casfri50'', 
						''getListInvs()'', 
						''rawfri'', ''translation.attribute_dependencies'', ''casfri_validation.excluded_validation_columns'', ''casfri_validation.complexMappingAttributes'')');
	END	
	$func$
LANGUAGE 'plpgsql';


-- refreshValidationSummaryAll()
-- Refreshes all entries in the aggregated validation error summary
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshValidationSummaryAll()
RETURNS VOID AS
	$func$
	BEGIN
		EXECUTE format('CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_summary 
		(inventory TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT, error_count INT, example_cas_id TEXT);');
		EXECUTE format('DELETE FROM casfri_validation.validation_error_rows_summary');
		EXECUTE format('INSERT INTO casfri_validation.validation_error_rows_summary
						SELECT left(cas_id, 4) AS inventory, table_name, error_type, error_desc, source_layer, count(*) AS error_count, min(cas_id) AS example_cas_id
						FROM validation_error_rows
						GROUP BY inventory, table_name, error_type, error_desc, source_layer
						ORDER BY inventory, table_name, error_desc, source_layer;');
		EXECUTE format('SELECT * FROM updateValidationErrorCodes(''validation_error_rows_summary'',''casfri_validation'')');
	END	
	$func$
LANGUAGE 'plpgsql';


-- refreshValidationSummaryForJurisdiction(jurisdiction TEXT)
-- Refreshes all entries in the aggregated validation error summary for the specified jurisdiction
-- Input:
	-- jurisdiction: jurisdiction to refresh
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshValidationSummaryForJurisdiction(jurisdiction TEXT)
RETURNS VOID AS
	$func$
	BEGIN
		EXECUTE format('CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_summary 
		(inventory TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT, error_count INT, example_cas_id TEXT);');
		EXECUTE format('DELETE FROM casfri_validation.validation_error_rows_summary WHERE left(inventory,2) = ''%s''', jurisdiction);
		EXECUTE format('INSERT INTO casfri_validation.validation_error_rows_summary
						SELECT left(cas_id, 4) AS inventory, table_name, error_type, error_desc, source_layer, count(*) AS error_count, min(cas_id) AS example_cas_id
						FROM validation_error_rows
						WHERE left(cas_id,2) = ''%s''
						GROUP BY inventory, table_name, error_type, error_desc, source_layer
						ORDER BY inventory, table_name, error_desc, source_layer;', jurisdiction);
		EXECUTE format('SELECT * FROM updateValidationErrorCodes(''validation_error_rows_summary'',''casfri_validation'')');
	END	
	$func$
LANGUAGE 'plpgsql';


-- refreshValidationSummaryForInventory(inventory TEXT)
-- Refreshes all entries in the aggregated validation error summary for the specified inventory
-- Input:
	-- inventory: inventory to refresh
-- Output: VOID
CREATE OR REPLACE FUNCTION refreshValidationSummaryForInventory(inventory TEXT)
RETURNS VOID AS
	$func$
	BEGIN
		EXECUTE format('CREATE TABLE IF NOT EXISTS casfri_validation.validation_error_rows_summary 
		(inventory TEXT, table_name TEXT, error_type TEXT, error_desc TEXT, source_layer TEXT, error_count INT, example_cas_id TEXT);');
		EXECUTE format('DELETE FROM casfri_validation.validation_error_rows_summary WHERE inventory = ''%s''', inventory);
		EXECUTE format('INSERT INTO casfri_validation.validation_error_rows_summary
						SELECT left(cas_id, 4) AS inventory, table_name, error_type, error_desc, source_layer, count(*) AS error_count, min(cas_id) AS example_cas_id
						FROM validation_error_rows
						WHERE left(cas_id,4) = ''%s''
						GROUP BY inventory, table_name, error_type, error_desc, source_layer
						ORDER BY inventory, table_name, error_desc, source_layer;', inventory);
		EXECUTE format('SELECT * FROM updateValidationErrorCodes(''validation_error_rows_summary'',''casfri_validation'')');
	END	
	$func$
LANGUAGE 'plpgsql';

