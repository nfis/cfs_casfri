-- getDisturbanceCutoff(inventory, attribute_dependencies_location)
-- Given an inventory, gives the disturbance index of the first disturbance that isn't recorded by the raw inventory
-- Input:
	-- inventory: the inventory id that records disturbances
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
-- Output: INTEGER index of first non recorded dists
	-- Returns 0 if all 3 dists are recorded
CREATE OR REPLACE FUNCTION getDisturbanceCutoff(
inventory TEXT,
attribute_dependencies_location TEXT)
RETURNS INTEGER AS
	$func$
	DECLARE 
		cutoff INTEGER;
	BEGIN
		EXECUTE format('SELECT (CASE WHEN dist_type_3 != '''' THEN 0
						WHEN dist_type_1 = '''' THEN 1
						WHEN dist_type_1 != '''' AND dist_type_2 = '''' THEN 2
						WHEN dist_type_2 != '''' AND dist_type_3 = '''' THEN 3
						ELSE 0 END) AS cutoff
						FROM %s
						WHERE inventory_id = ''%s'' AND layer = ''1''', attribute_dependencies_location, inventory) INTO cutoff;	
		RETURN cutoff;
	END;
	$func$
LANGUAGE 'plpgsql';



-- runDisturbanceValidationOnInventory(dest_table_name, inventory, creation_schema, attribute_dependencies_location)
-- Given a specific inventory, runs disturbance validations on the inventory and stores flagged errors in the dest_table
-- Collects any generic validation errors, disturbances that are mapped past the maximum possible disturbances number for that inventory, 
-- and disturbances that were either mapped when a raw value doesn't exist, or mapped to an error code when a raw value does exists
-- Input:
	-- dest_table_name: the name of the table to store flagged errors in
	-- inventory: the inventory that records dists
	-- creation_schema: the schema that the dest_table and raw_joined tables are in
	-- attribute_dependencies_location: the source of the attribute_dependencies table. Must include a inventory_id, layer, and the specified attribute_name columns
		-- This is formatted as a 'SELECT * FROM %', so a select statement in brackets, a function call, a table, or a value list are all appropriate
		-- ex: 'translation.attribute_dependencies'
-- Output: VOID 
	-- Inserts rows into dest_table with descriptions of each error found
CREATE OR REPLACE FUNCTION runDisturbanceValidationOnInventory(
dest_table_name TEXT,
inventory TEXT,
creation_schema TEXT, 
attribute_dependencies_location TEXT
)
RETURNS VOID AS
	$func$
	DECLARE 
		dynamic_sql TEXT;
		raw_merged_table TEXT;
		dist_cutoff INTEGER;
		loop_end INTEGER;
		i INTEGER; 
		dist_col TEXT;
		dist_dependency TEXT;
		dep TEXT[];
		cond TEXT[];
		values_string TEXT;
		tt_type TEXT;
	BEGIN
		raw_merged_table := inventory || '_raw_dst_all_joined';
		RAISE NOTICE 'Run Disturbance Validation- (%) Function started for inventory %', LEFT(clock_timestamp()::TEXT, 19), inventory;
		RAISE NOTICE 'Run Disturbance Validation- (%) 	Finding disturbance null errors', LEFT(clock_timestamp()::TEXT, 19);
		EXECUTE format('SELECT * FROM getDisturbanceCutoff(''%s'', ''%s'')', inventory, attribute_dependencies_location) INTO dist_cutoff;
		IF dist_cutoff = 0 
		THEN 
		-- Disturbances in CASFRI go up to 3
			loop_end = 3;
		ELSE 
			loop_end = dist_cutoff -1;
		END IF;
		-- Process basic inventories with simple mappings
		IF inventory IN ('ON02', 'YT04', 'MB06', 'MB07', 'MB10', 'MB12', 'AB30') OR left(inventory, 2) IN ('NB', 'NT', 'BC', 'NS')
		THEN 
			RAISE NOTICE 'Run Disturbance Validation- (%) 		Inventory % uses separated disturbance mappings', LEFT(clock_timestamp()::TEXT, 19), inventory;
			FOR i IN 1..loop_end
			LOOP
				RAISE NOTICE 'Run Disturbance Validation- (%) 		Checking dist_%', LEFT(clock_timestamp()::TEXT, 19), i;
				FOR dist_col IN (VALUES ('dist_type'), ('dist_year'), ('dist_ext_upper'), ('dist_ext_lower'))
				LOOP
					EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''%s_%s'', ''%s'', 1, ''dst_all'')', inventory, dist_col, i, attribute_dependencies_location) INTO dist_dependency;
					-- AB30 has a direct mapping to 'CUT', but will use harv_yr to validate since it is never empty in this inventory anyways
					IF dist_col = 'dist_type' AND inventory = 'AB30' AND i = 1
					THEN 
						dist_dependency := 'harv_yr';
					END IF;
					-- BC's dist_year also uses a numeric year cutoff as a dependency but it is not needed for this validation
					IF dist_col = 'dist_year' AND LEFT(inventory,2) = 'BC' AND i = 1
					THEN 
						dist_dependency := trim(split_part(dist_dependency, ',', 1));
					END IF;
					EXIT WHEN dist_dependency IS NULL;
					-- Catch falsely mapped disturbances when a dependency doesn't exist
					IF trim(dist_dependency)IN ('', 'NULL')
					THEN
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''%s_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(%s_%s::text) = FALSE)', 
										creation_schema, dest_table_name, dist_col, i, creation_schema, raw_merged_table, dist_col, i);
					ELSE
						-- Catch falsely null disturbances when a disturbance could have been mapped
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''%s_%s was null when it could have been mapped from raw'' AS error_desc, source_layer
										FROM %s.%s
										WHERE %s_%s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-8886'', ''-8887'') AND trim(COALESCE(raw_%s::text, ''NULL'')) NOT IN ('''', ''NULL'', ''0''))', 
										creation_schema, dest_table_name, dist_col, i, creation_schema, raw_merged_table, dist_col, i, dist_dependency);
						-- Catch falsely mapped disturbances when the raw value is null
						EXECUTE format('INSERT INTO %s.%s 
										(SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''%s_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
										FROM %s.%s
										WHERE isError(%s_%s::text) = FALSE AND trim(COALESCE(raw_%s::text, ''NULL'')) IN ('''', ''NULL'', ''0''))', 
										creation_schema, dest_table_name, dist_col, i, creation_schema, raw_merged_table, dist_col, i, dist_dependency);
					END IF;
				END LOOP;
			END LOOP;
			-- Find disturbances that are mapped past the maximum possible number of disturbances in an inventory	
			RAISE NOTICE 'Run Disturbance Validation- (%) 	Finding disturbances mapped past maximum possible in %', LEFT(clock_timestamp()::TEXT, 19), inventory;
			IF dist_cutoff > 0
			THEN
				dynamic_sql := format('INSERT INTO %s.%s
								SELECT cas_id, ''dst_all'' as table_name, ''DISTURBANCE_ERROR'' AS error_type, ''A disturbance was mapped when it should have been non-applicable (%s-3)'' AS error_desc, source_layer
								FROM %s.%s
								WHERE (', creation_schema, dest_table_name, dist_cutoff, creation_schema, raw_merged_table);
				FOR i IN dist_cutoff..3
				LOOP
					dynamic_sql := dynamic_sql || 'isError(dist_type_' || i || '::text) = FALSE OR isError(dist_year_' || i || '::text) = FALSE OR isError(dist_ext_upper_' || i || '::text) = FALSE OR isError(dist_ext_lower_' || i || '::text) = FALSE OR ';
				END LOOP;
				dynamic_sql := trim(TRAILING ' OR ' FROM dynamic_sql);
				dynamic_sql := dynamic_sql || ')';
				EXECUTE dynamic_sql;
			END IF;
		-- If the inventory isn't one of the basic mapping inventories, must use custom validations
		-- YT03 uses helper functions to check that the expected value equals the actual for each disturbance column
		ELSIF inventory = 'YT03'
		THEN
			FOR i IN 1..3
			LOOP
				EXECUTE format('INSERT INTO %s.%s
								(WITH mappings AS (
									SELECT cas_id, layer, 
									dist_type_%s, dist_year_%s, dist_ext_upper_%s, dist_ext_lower_%s, 
									"translation".tt_yt_yvi02_disturbance_mapText(
									concat(''{'''''', raw_disturbance1_code, '''''', '''''', raw_disturbance2_code, '''''', '''''', raw_disturbance3_code, '''''', '''''', raw_disturbance4_code, ''''''}''),
									concat(''{'', (CASE WHEN raw_disturbance1_year IS NULL THEN ''NULL'' ELSE raw_disturbance1_year::text END), '', '', (CASE WHEN raw_disturbance2_year IS NULL THEN ''NULL'' ELSE raw_disturbance2_year::text END), '', '', (CASE WHEN raw_disturbance3_year IS NULL THEN ''NULL'' ELSE raw_disturbance3_year::text END), '', '', (CASE WHEN raw_disturbance4_year IS NULL THEN ''NULL'' ELSE raw_disturbance4_year::text END), ''}''),
									concat(''{'''''', raw_disturbance1_layer, '''''', '''''', raw_disturbance2_layer, '''''', '''''', raw_disturbance3_layer, '''''', '''''', raw_disturbance4_layer, ''''''}''), layer::text, ''%s'',
									''{''''DB'''',''''DF'''',''''DI'''',''''DL'''',''''DM'''',''''DS'''',''''DW'''',''''SI'''',''''SL'''',''''UC''''}'', ''{''''BURN'''',''''FLOOD'''',''''INSECT'''',''''CUT'''',''''OTHER'''',''''OTHER'''',''''WINDFALL'''',''''SILVICULTURE_TREATMENT'''',''''SLIDE'''',''''OTHER''''}'') AS dist_type,
									"translation".tt_yt_yvi02_disturbance_copyInt(
									concat(''{'', (CASE WHEN raw_disturbance1_year IS NULL THEN ''NULL'' ELSE raw_disturbance1_year::text END), '', '', (CASE WHEN raw_disturbance2_year IS NULL THEN ''NULL'' ELSE raw_disturbance2_year::text END), '', '', (CASE WHEN raw_disturbance3_year IS NULL THEN ''NULL'' ELSE raw_disturbance3_year::text END), '', '', (CASE WHEN raw_disturbance4_year IS NULL THEN ''NULL'' ELSE raw_disturbance4_year::text END), ''}''),
									concat(''{'', (CASE WHEN raw_disturbance1_year IS NULL THEN ''NULL'' ELSE raw_disturbance1_year::text END), '', '', (CASE WHEN raw_disturbance2_year IS NULL THEN ''NULL'' ELSE raw_disturbance2_year::text END), '', '', (CASE WHEN raw_disturbance3_year IS NULL THEN ''NULL'' ELSE raw_disturbance3_year::text END), '', '', (CASE WHEN raw_disturbance4_year IS NULL THEN ''NULL'' ELSE raw_disturbance4_year::text END), ''}''),
									concat(''{'''''', raw_disturbance1_layer, '''''', '''''', raw_disturbance2_layer, '''''', '''''', raw_disturbance3_layer, '''''', '''''', raw_disturbance4_layer, ''''''}''), layer::text, ''%s'') AS dist_year,
									"translation".tt_yt_yvi02_disturbance_copyInt(
									concat(''{'', (CASE WHEN raw_disturbance1_extent_percent IS NULL THEN ''NULL'' ELSE raw_disturbance1_extent_percent::text END), '', '', (CASE WHEN raw_disturbance2_extent_percent IS NULL THEN ''NULL'' ELSE raw_disturbance2_extent_percent::text END), '', '', (CASE WHEN raw_disturbance3_extent_percent IS NULL THEN ''NULL'' ELSE raw_disturbance3_extent_percent::text END), '', '', (CASE WHEN raw_disturbance4_extent_percent IS NULL THEN ''NULL'' ELSE raw_disturbance4_extent_percent::text END), ''}''),
									concat(''{'', (CASE WHEN raw_disturbance1_year IS NULL THEN ''NULL'' ELSE raw_disturbance1_year::text END), '', '', (CASE WHEN raw_disturbance2_year IS NULL THEN ''NULL'' ELSE raw_disturbance2_year::text END), '', '', (CASE WHEN raw_disturbance3_year IS NULL THEN ''NULL'' ELSE raw_disturbance3_year::text END), '', '', (CASE WHEN raw_disturbance4_year IS NULL THEN ''NULL'' ELSE raw_disturbance4_year::text END), ''}''),
									concat(''{'''''', raw_disturbance1_layer, '''''', '''''', raw_disturbance2_layer, '''''', '''''', raw_disturbance3_layer, '''''', '''''', raw_disturbance4_layer, ''''''}''), layer::text, ''%s'') AS dist_ext
									FROM %s.%s
								), cases AS (
									SELECT cas_id, layer, 
									(CASE WHEN isError(dist_type_%s::text) = TRUE THEN NULL ELSE dist_type_%s::text END) AS dist_type_cas,
									(CASE WHEN isError(dist_year_%s::text) = TRUE THEN NULL ELSE dist_year_%s::text END) AS dist_year_cas,
									(CASE WHEN isError(dist_ext_upper_%s::text) = TRUE THEN NULL ELSE dist_ext_upper_%s::text END) AS dist_ext_upper_cas,
									(CASE WHEN isError(dist_ext_lower_%s::text) = TRUE THEN NULL ELSE dist_ext_lower_%s::text END) AS dist_ext_lower_cas,
									dist_type::text, dist_year::text, dist_ext::text
									FROM mappings
								)
								SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_%s was not mapped correctly'' AS error_desc, layer::text AS source_layer
								FROM cases
								WHERE dist_type_cas != dist_type OR dist_year_cas != dist_year OR dist_ext_upper_cas != dist_ext OR dist_ext_lower_cas != dist_ext)', 
								creation_schema, dest_table_name, i, i, i, i, i, i, i, creation_schema, raw_merged_table, i, i, i, i, i, i, i, i, i, i, i, i);
			END LOOP;
			-- Inventories below this ELSE all have specific lists of disturbance columns that have no dependency mapping and must be checked for FALSE_MAPPED errors
		ELSE
			-- These inventories have 5 seperate mappings for disturbance 1, must check that at least one of them has a value to be mapped
			IF inventory IN ('MB03', 'MB11')
			THEN
				RAISE NOTICE 'Run Disturbance Validation- (%) 		Inventory % uses a unique Manitoba disturbance mapping', LEFT(clock_timestamp()::TEXT, 19), inventory;
				EXECUTE format('SELECT * FROM getAttributeDependency(''%s'', ''dist_type_1'', ''%s'', 1, ''dst_all'')', inventory, attribute_dependencies_location) INTO dist_dependency;
				FOR i IN 1..5
				LOOP
					IF TRIM(SPLIT_PART(dist_dependency, ', ', i)) = '0' THEN
						dep[i] := 'NULL';
					ELSE
						dep[i] = 'raw_'||trim(SPLIT_PART(dist_dependency, ',', i));
					END IF;
				END LOOP;
				RAISE NOTICE 'Run Disturbance Validation- (%) 		Checking dist_1', LEFT(clock_timestamp()::TEXT, 19);
				FOR dist_col IN (VALUES ('dist_type'), ('dist_year'))
				LOOP
					-- Catch falsely null disturbances when a disturbance could have been mapped
					EXECUTE format('INSERT INTO %s.%s 
									(SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''%s_1 was null when it could have been mapped from raw'' AS error_desc, source_layer
									FROM %s.%s
									WHERE %s_1::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-9999'', ''-8886'', ''-8887'') 
									AND (trim(COALESCE(%s::text, ''NULL'')) NOT IN ('''', ''NULL'', ''0'') OR
										 trim(COALESCE(%s::text, ''NULL'')) NOT IN ('''', ''NULL'', ''0'') OR
										 trim(COALESCE(%s::text, ''NULL'')) NOT IN ('''', ''NULL'', ''0'') OR
										 trim(COALESCE(%s::text, ''NULL'')) NOT IN ('''', ''NULL'', ''0'') OR
										 trim(COALESCE(%s::text, ''NULL'')) NOT IN ('''', ''NULL'', ''0'')))', 
									creation_schema, dest_table_name, dist_col, creation_schema, raw_merged_table, dist_col, dep[1], dep[2], dep[3], dep[4], dep[5]);
					-- Catch falsely mapped disturbances when the raw value is null
					EXECUTE format('INSERT INTO %s.%s 
									(SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''%s_1 was mapped but a raw value doesnt exist'' AS error_desc, source_layer
									FROM %s.%s
									WHERE isError(%s_1::text) = FALSE
									AND (trim(COALESCE(%s::text, ''NULL'')) IN ('''', ''NULL'', ''0'') AND
										 trim(COALESCE(%s::text, ''NULL'')) IN ('''', ''NULL'', ''0'') AND
										 trim(COALESCE(%s::text, ''NULL'')) IN ('''', ''NULL'', ''0'') AND
										 trim(COALESCE(%s::text, ''NULL'')) IN ('''', ''NULL'', ''0'') AND
										 trim(COALESCE(%s::text, ''NULL'')) IN ('''', ''NULL'', ''0'')))', 
									creation_schema, dest_table_name, dist_col, creation_schema, raw_merged_table, dist_col, dep[1], dep[2], dep[3], dep[4], dep[5]);
				END LOOP;
				values_string := '(VALUES (''dist_ext_upper_1''), (''dist_ext_lower_1''), (''dist_type_2''), (''dist_year_2''), (''dist_ext_upper_2''), (''dist_ext_lower_2''), (''dist_type_3''), (''dist_year_3''), (''dist_ext_upper_3''), (''dist_ext_lower_3'')) AS foo(vals)';
			-- PE01 extracts dist_year from the org_hist value so must treat this specially
			ELSIF inventory = 'PE01'
			THEN
				RAISE NOTICE 'Run Disturbance Validation- (%) 		Inventory % uses a unique PE01 disturbance mapping', LEFT(clock_timestamp()::TEXT, 19), inventory;
				FOR i IN 1..2
				LOOP
					IF i = 1
					THEN
						cond[1] := '(raw_org_hist IS NOT NULL OR raw_landtype IS NOT NULL)';
						cond[2] := 'raw_org_hist IS NULL AND raw_landtype IS NULL';
					ELSE
						cond[1] := 'length(raw_org_hist) = 4';
						cond[2] := 'length(raw_org_hist) != 4';
					END IF;
					EXECUTE format('INSERT INTO %s.%s 
					(SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_type_%s is empty but a raw value exists'' AS error_desc, source_layer
					FROM %s.%s
					WHERE dist_type_%s IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-8886'', ''-8887'') AND %s
					UNION ALL
					SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_type_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
					FROM %s.%s
					WHERE isError(dist_type_%s) = FALSE AND %s)',
					creation_schema, dest_table_name, i, creation_schema, raw_merged_table, i, cond[1], i, creation_schema, raw_merged_table, i, cond[2]);
				END LOOP;
				values_string := '(VALUES (''dist_year_1''), (''dist_ext_upper_1''), (''dist_ext_lower_1''), (''dist_year_2''), (''dist_ext_upper_2''), (''dist_ext_lower_2''), (''dist_type_3''), (''dist_year_3''), (''dist_ext_upper_3''), (''dist_ext_lower_3'')) AS foo(vals)';
			-- Other PE inventories behave similar, but go up to dist_2 and have different mappings
			ELSIF inventory IN ('PE02', 'PE03', 'PE04')
			THEN
				RAISE NOTICE 'Run Disturbance Validation- (%) 		Inventory % uses a unique PEI disturbance mapping', LEFT(clock_timestamp()::TEXT, 19), inventory;
				FOR i IN 1..2
				LOOP
					IF i = 1
					THEN
						cond[1] := format('(trim(coalesce(raw_history%s::text, ''NULL'')) NOT IN ('''', ''NULL'') OR raw_subuse IS NOT NULL)', i);
						cond[2] := format('trim(coalesce(raw_history%s::text, ''NULL'')) IN ('''', ''NULL'') AND raw_subuse IS NULL', i);
					ELSE
						cond[1] := format('trim(coalesce(raw_history%s::text, ''NULL'')) NOT IN ('''', ''NULL'')', i);
						cond[2] := format('trim(coalesce(raw_history%s::text, ''NULL'')) IN ('''', ''NULL'')', i);
					END IF;
					EXECUTE format('INSERT INTO %s.%s 
					(SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_type_%s is empty but a raw value exists'' AS error_desc, source_layer
					FROM %s.%s
					WHERE dist_type_%s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-8886'', ''-8887'') AND %s
					UNION ALL
					SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_type_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
					FROM %s.%s
					WHERE isError(dist_type_%s::text) = FALSE AND %s
					UNION ALL
					SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_year_%s is empty but a raw value exists'' AS error_desc, source_layer
					FROM %s.%s
					WHERE dist_year_%s::text IN (''NULL_VALUE'', ''EMPTY_STRING'', ''UNKNOWN_VALUE'', ''NOT_APPLICABLE'', ''-8888'', ''-8886'', ''-8887'') AND length(raw_history%s) = 5
					UNION ALL
					SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_year_%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
					FROM %s.%s
					WHERE isError(dist_year_%s::text) = FALSE AND length(raw_history%s) != 5)',
					creation_schema, dest_table_name, 
					i, creation_schema, raw_merged_table, i, cond[1],
					i, creation_schema, raw_merged_table, i, cond[2],
					i, creation_schema, raw_merged_table, i, i,
					i, creation_schema, raw_merged_table, i, i);
				END LOOP;
				values_string := '(VALUES (''dist_ext_upper_1''), (''dist_ext_lower_1''), (''dist_ext_upper_2''), (''dist_ext_lower_2''), (''dist_type_3''), (''dist_year_3''), (''dist_ext_upper_3''), (''dist_ext_lower_3'')) AS foo(vals)';
			-- AB29 has simple separated disturbance mappings, but a helper function swaps orders of disturbances based on dist_year
			ELSIF inventory = 'AB29'
			THEN
				FOR i IN 1..2
				LOOP
					-- tt_type determines whether to use the min functions or the max ones
					IF i = 1
					THEN
						tt_type = 'min';
					ELSE 
						tt_type = 'max';
					END IF;
					EXECUTE format('INSERT INTO %s.%s 
									(WITH mappings AS (
										SELECT cas_id, layer, 
										dist_type_%s, dist_year_%s, dist_ext_upper_%s, dist_ext_lower_%s, 
										"translation".TT_%sIndexMapText(
										concat(''{'', (CASE WHEN raw_mod1_yr IS NULL THEN ''NULL'' ELSE raw_mod1_yr::text END), '', '', (CASE WHEN raw_mod2_yr IS NULL THEN ''NULL'' ELSE raw_mod2_yr::text END), ''}''), 
										concat(''{'', (CASE WHEN raw_mod1 = '''' THEN ''NULL'' ELSE raw_mod1 END), '', '', (CASE WHEN raw_mod2 = '''' THEN ''NULL'' ELSE raw_mod2 END), ''}''), 
										''{''''BU'''',''''CC'''',''''CL'''',''''CU'''',''''DT'''',''''PL'''',''''SC'''',''''SL'''',''''SN'''',''''ST'''',''''SU'''',''''TH'''',''''UK'''',''''WF'''', ''''SI'''', ''''DI'''', ''''IK'''', ''''WE'''', ''''BT'''', ''''GR'''', ''''IR'''', ''''CW'''', ''''TL'''', ''''FL'''', ''''PI'''', ''''FT'''', ''''AS'''', ''''MT''''}'', 
										''{''''BURN'''',''''CUT'''',''''OTHER'''',''''OTHER'''',''''OTHER'''',''''SILVICULTURE_TREATMENT'''',''''SILVICULTURE_TREATMENT'''',''''OTHER'''',''''DEAD_UNKNOWN'''',''''OTHER'''',''''OTHER'''',''''SILVICULTURE_TREATMENT'''',''''DEAD_UNKNOWN'''',''''WINDFALL'''', ''''SILVICULTURE_TREATMENT'''',''''DISEASE'''',''''INSECT'''',''''WEATHER'''',''''OTHER'''',''''OTHER'''',''''OTHER'''',''''OTHER'''',''''OTHER'''',''''OTHER'''',''''OTHER'''',''''OTHER'''',''''OTHER'''',''''OTHER''''}'', 
										''9999'', ''9999'') AS dist_type,
										"translation".TT_%sIndexCopyInt(
										concat(''{'', (CASE WHEN raw_mod1_yr IS NULL THEN ''NULL'' ELSE raw_mod1_yr::text END), '', '', (CASE WHEN raw_mod2_yr IS NULL THEN ''NULL'' ELSE raw_mod2_yr::text END), ''}''), 
										concat(''{'', (CASE WHEN raw_mod1_yr IS NULL THEN ''NULL'' ELSE raw_mod1_yr::text END), '', '', (CASE WHEN raw_mod2_yr IS NULL THEN ''NULL'' ELSE raw_mod2_yr::text END), ''}''), 
										''9999'', ''9999'') AS dist_year,
										"translation".TT_%sIndexMapInt(
										concat(''{'', (CASE WHEN raw_mod1_yr IS NULL THEN ''NULL'' ELSE raw_mod1_yr::text END), '', '', (CASE WHEN raw_mod2_yr IS NULL THEN ''NULL'' ELSE raw_mod2_yr::text END), ''}''), 
										concat(''{'', (CASE WHEN raw_mod1_ext IS NULL THEN ''NULL'' ELSE raw_mod1_ext::text END), '', '', (CASE WHEN raw_mod2_ext IS NULL THEN ''NULL'' ELSE raw_mod2_ext::text END), ''}''), 
										''{1,2,3,4,5}'', ''{25,50,75,95,100}'', ''9999'', ''9999'') AS dist_ext_upper,
										"translation".TT_%sIndexMapInt(
										concat(''{'', (CASE WHEN raw_mod1_yr IS NULL THEN ''NULL'' ELSE raw_mod1_yr::text END), '', '', (CASE WHEN raw_mod2_yr IS NULL THEN ''NULL'' ELSE raw_mod2_yr::text END), ''}''), 
										concat(''{'', (CASE WHEN raw_mod1_ext IS NULL THEN ''NULL'' ELSE raw_mod1_ext::text END), '', '', (CASE WHEN raw_mod2_ext IS NULL THEN ''NULL'' ELSE raw_mod2_ext::text END), ''}''), 
										''{1,2,3,4,5}'', ''{1,26,51,76,96}'', ''9999'', ''9999'') AS dist_ext_lower
										FROM %s.%s
									), cases AS (
										SELECT cas_id, layer, 
										(CASE WHEN isError(dist_type_%s::text) = TRUE THEN NULL ELSE dist_type_%s::text END) AS dist_type_cas,
										(CASE WHEN isError(dist_year_%s::text) = TRUE THEN NULL ELSE dist_year_%s::text END) AS dist_year_cas,
										(CASE WHEN isError(dist_ext_upper_%s::text) = TRUE THEN NULL ELSE dist_ext_upper_%s::text END) AS dist_ext_upper_cas,
										(CASE WHEN isError(dist_ext_lower_%s::text) = TRUE THEN NULL ELSE dist_ext_lower_%s::text END) AS dist_ext_lower_cas,
										dist_type::text, 
										(CASE WHEN dist_year = 0 THEN NULL ELSE dist_year::TEXT END), 
										dist_ext_upper::text, dist_ext_lower::text
										FROM mappings
									)
									SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_%s was not mapped correctly'' AS error_desc, layer::text AS source_layer
									FROM cases
									WHERE dist_type_cas != dist_type OR dist_year_cas != dist_year OR dist_ext_upper_cas != dist_ext_upper OR dist_ext_lower_cas != dist_ext_lower)',
									creation_schema, dest_table_name, i, i, i, i, tt_type, tt_type, tt_type, tt_type, creation_schema, raw_merged_table, i, i, i, i, i, i, i, i, i);
					END LOOP;
				values_string := '(VALUES (''dist_type_3''), (''dist_year_3''), (''dist_ext_upper_3''), (''dist_ext_lower_3'')) AS foo(vals)';
			-- QC inventories have separated disturbance mappings, but a helper function swaps orders of disturbances based on dist_year, and some mappings have multiple terms
			ELSIF left(inventory, 2) = 'QC'
			THEN
				FOR i IN 1..2
				LOOP
					-- tt_type determines whethere to use the min functions or the max ones
					IF i = 1
					THEN
						tt_type = 'min';
					ELSE 
						tt_type = 'max';
					END IF;
					EXECUTE format('INSERT INTO %s.%s 
									(WITH mappings AS (
										SELECT cas_id, layer, 
										dist_type_%s, dist_year_%s,
										"translation".TT_lookupText("translation".TT_%sIndexCopyText(
										concat(''{'', (CASE WHEN raw_an_origine IS NULL THEN ''NULL'' ELSE raw_an_origine::text END), '', '', (CASE WHEN raw_an_perturb IS NULL THEN ''NULL'' ELSE raw_an_perturb::text END), ''}''), 
										concat(''{'', (CASE WHEN raw_origine IS NULL THEN ''NULL'' ELSE raw_origine::text END), '', '', (CASE WHEN raw_perturb IS NULL THEN ''NULL'' ELSE raw_perturb::text END), ''}''), 
										''9999'', ''9999''), ''translation'',''qc_disturbance_lookup'',''source_val'',''dist_type'') AS dist_type,
										"translation".TT_%sInt(
										concat(''{'', (CASE WHEN raw_an_origine IS NULL THEN ''NULL'' ELSE raw_an_origine::text END), '', '', (CASE WHEN raw_an_perturb IS NULL THEN ''NULL'' ELSE raw_an_perturb::text END), ''}'')) AS dist_year
										FROM %s.%s
									), cases AS (
										SELECT cas_id, layer, 
										(CASE WHEN isError(dist_type_%s::text) = TRUE THEN NULL ELSE dist_type_%s::text END) AS dist_type_cas,
										(CASE WHEN isError(dist_year_%s::text) = TRUE THEN NULL ELSE dist_year_%s::text END) AS dist_year_cas,
										dist_type::text, dist_year::text
										FROM mappings
									)
									SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''dist_%s was not mapped correctly'' AS error_desc, layer::text AS source_layer
									FROM cases
									WHERE dist_type_cas != dist_type OR dist_year_cas != dist_year)',
									creation_schema, dest_table_name, i, i, tt_type, tt_type, creation_schema, raw_merged_table, i, i, i, i, i);		
				END LOOP;
				values_string := '(VALUES (''dist_ext_upper_1''), (''dist_ext_lower_1''), (''dist_ext_upper_2''), (''dist_ext_lower_2''), (''dist_type_3''), (''dist_year_3''), (''dist_ext_upper_3''), (''dist_ext_lower_3'')) AS foo(vals)';
			ELSE 
				-- Any inventories left behind will raise a notice. Function must be updated to include missed inventories
				RAISE NOTICE 'Run Disturbance Validation- (%) ALERT: Inventory % does not have rules set for disturbance validation!', LEFT(clock_timestamp()::TEXT, 19), inventory;
			END IF;
			-- All inventories above then checked for FALSE_MAPPED errors for each disturbance column that isn't recorded
			RAISE NOTICE 'Run Disturbance Validation- (%) 	Checking mappings that dont exist', LEFT(clock_timestamp()::TEXT, 19);
			FOR dist_col IN 
				EXECUTE format('SELECT vals FROM %s', values_string)
			LOOP
				-- Catch falsely mapped disturbances when a dependency doesn't exist
				EXECUTE format('INSERT INTO %s.%s 
								(SELECT cas_id, ''dst_all'' AS table_name, ''DISTURBANCE_ERROR'' AS error_type, ''%s was mapped but a raw value doesnt exist'' AS error_desc, source_layer
								FROM %s.%s
								WHERE isError(%s::text) = FALSE)', 
								creation_schema, dest_table_name, dist_col, creation_schema, raw_merged_table, dist_col);
			END LOOP;
		END IF;
		-- Find true translation errors- these should all count as true errors
		RAISE NOTICE 'Run Disturbance Validation- (%) 	Finding disturbance translation errors in %', LEFT(clock_timestamp()::TEXT, 19), inventory;
		FOR i IN 1..3
		LOOP
			RAISE NOTICE 'Run Disturbance Validation- (%) 	Checking dist_%', LEFT(clock_timestamp()::TEXT, 19), i;
			FOR dist_col IN (VALUES ('dist_type'), ('dist_year'), ('dist_ext_upper'), ('dist_ext_lower'))
			LOOP
				EXECUTE format('INSERT INTO %s.%s
								SELECT cas_id, ''dst_all'' AS table_name,  %s_%s::text AS error_type, ''%s_%s has a translation error'' AS error_desc, source_layer 
								FROM %s.%s
								WHERE isTranslationError(%s_%s::text) = TRUE', 
								creation_schema, dest_table_name, dist_col, i, dist_col, i, creation_schema, raw_merged_table, dist_col, i);
			END LOOP;
		END LOOP;
		RAISE NOTICE 'Run Disturbance Validation- (%) Function complete', LEFT(clock_timestamp()::TEXT, 19);
	END;
	$func$
LANGUAGE 'plpgsql';



