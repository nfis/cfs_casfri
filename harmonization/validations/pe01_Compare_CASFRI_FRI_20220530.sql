/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: April 05, 2022 by Kangakola
Last Modified Date: May 30, 2022 by Kangakola

*/

/*
=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate  the layer 1 and layer 2 and several attributes (Species, Height, Crown closure, Origin & Productivity) of layer 1 and layer 2 in CASFRI translated data. 
	   Then assess the accuracy of these attributes between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Update: Integration of crownclosurecompare, heightcompare and siteclasscompare code. Also, add code for validation of dst, eco and nfl tables.	   
	*** Output: Multiple  table of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for PE01 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between these attributes for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
	
 ================================================================================================================================================================

*/


/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/


/* LYR INVESTIGATION. */
---------- Sub-setting layer 1 --------------------------
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l1;
CREATE TABLE casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l1 AS
SELECT
c.cas_id,
c.crown_closure_lower, c.crown_closure_upper, n.crown, 
--c.dist_type_1, n.org_hist, 
--c.dist_type_2, 
n.org_hist, 
c.height_lower, c.height_upper, n.height, 
n.landtype, 
c.map_sheet_id, n.map, 
c.orig_stand_id, n.stand, 
c.species_1, n.spec1, 
c.species_2, n.spec2, 
c.species_3, n.spec3, 
c.species_4, n.spec4, 
c.species_5, n.spec5, 
c.species_per_1, n.per1, 
c.species_per_2, n.per2, 
c.species_per_3, n.per3, 
c.species_per_4, n.per4, 
c.species_per_5, n.per5, 
--c.src_inv_area, n.area, 
c.layer,
n.ogc_fid,
	--CASE WHEN height_upper::INTEGER = height::INTEGER THEN '1' ELSE '0' END AS Checkheight_1,  ------ CHANGE THIS
	--CASE WHEN height_lower::INTEGER = height::INTEGER THEN '1' ELSE '0' END AS Checkheight_2 ------- CHANGE THIS
 	--CASE WHEN height_upper = height THEN '1' ELSE '0' END AS Checkheight_1,  ------ CHANGE THIS
	--CASE WHEN height_lower = height THEN '1' ELSE '0' END AS Checkheight_2, ------- CHANGE THIS
 	CASE WHEN height_upper = height AND height_lower = height THEN '1' ELSE '0' END AS Checkheight_1 ------- CHANGE THIS

FROM rawfri.pe01 n ----- 107220
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_sheet_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS orig_stand_id, --- changed 0 to x
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid --- changed 0 to x
FROM casfri50.lyr_all WHERE cas_id ILIKE 'pe01%' and layer =1 ----- 81074 rows
) c
ON n.map::TEXT = c.map_sheet_id AND n.stand::TEXT = c.orig_stand_id AND n.ogc_fid::TEXT = c.ogc_fid;

--- INVESTIGATION ON NULL CAS_ID: this was created by the left join initiated by SISHIR

-- INVESTIGATE THE SPECIES LI OCCURRENCE.
SELECT DISTINCT(species_1, spec1) AS speciesTest FROM casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l1 ORDER BY speciesTest;



--- layer 2 if exists

DROP TABLE IF EXISTS casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l2;
CREATE TABLE casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l2 AS
SELECT
c.cas_id,
c.crown_closure_lower, c.crown_closure_upper, n.crown, 
--c.dist_type_1, n.org_hist, 
--c.dist_type_2, 
n.org_hist, 
c.height_lower, c.height_upper, n.height, 
n.landtype, 
c.map_sheet_id, n.map, 
c.orig_stand_id, n.stand, 
c.species_1, n.spec1, 
c.species_2, n.spec2, 
c.species_3, n.spec3, 
c.species_4, n.spec4, 
c.species_5, n.spec5, 
c.species_per_1, n.per1, 
c.species_per_2, n.per2, 
c.species_per_3, n.per3, 
c.species_per_4, n.per4, 
c.species_per_5, n.per5, 
--c.src_inv_area, n.area, 
c.layer,
n.ogc_fid,
	--CASE WHEN height_upper::INTEGER = height::INTEGER THEN '1' ELSE '0' END AS Checkheight_1,  ------ CHANGE THIS
	--CASE WHEN height_lower::INTEGER = height::INTEGER THEN '1' ELSE '0' END AS Checkheight_2 ------- CHANGE THIS
 	--CASE WHEN height_upper = height THEN '1' ELSE '0' END AS Checkheight_1,  ------ CHANGE THIS
	--CASE WHEN height_lower = height THEN '1' ELSE '0' END AS Checkheight_2, ------- CHANGE THIS
 	CASE WHEN height_upper = height AND height_lower = height THEN '1' ELSE '0' END AS Checkheight_1 ------- CHANGE THIS

FROM rawfri.pe01 n ----- 107220
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_sheet_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS orig_stand_id, --- changed 0 to x
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid --- changed 0 to x
FROM casfri50.lyr_all WHERE cas_id ILIKE 'pe01%' and layer =2 ----- 0 rows
) c
ON n.map = c.map_sheet_id::INTEGER AND n.stand = c.orig_stand_id::INTEGER AND n.ogc_fid = c.ogc_fid::INTEGER;

-- ALTER TABLE test_casfri50.test_fri_cas_l1_LJ ALTER COLUMN height_upper_TL TYPE INTEGER;

--- SELECT cas_id, height_upper_TL, height from test_casfri50.test_fri_cas_l1_LJ WHERE height_upper_tl::INTEGER != height::INTEGER;



-- Height translation problem.

DROP TABLE IF EXISTS casfri50_test.pei_height_translation_problem;
CREATE TABLE casfri50_test.pei_height_translation_problem AS
SELECT height, height_upper, height_lower FROM casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l1 WHERE height > 0 AND (height_upper IS NULL OR height_lower IS NULL); -- 2

DROP TABLE IF EXISTS casfri50_test.pei_height_translation_problem_2;
CREATE TABLE casfri50_test.pei_height_translation_problem_2 AS
SELECT height, height_upper, height_lower FROM casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l1 WHERE height = 0 AND (height_upper IS NULL OR height_lower IS NULL); -- 26144

DROP TABLE IF EXISTS casfri50_test.pei_height_translation_problem_3;
CREATE TABLE casfri50_test.pei_height_translation_problem_3 AS
SELECT * FROM casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l1 WHERE height_upper IS NULL OR height_lower IS NULL; -- 26144

-- cas_id null 
DROP TABLE IF EXISTS casfri50_test.pei_height_translation_problem_4_cas_id_null;
CREATE TABLE casfri50_test.pei_height_translation_problem_4_cas_id_null AS
SELECT * FROM casfri50_test.pei_multiples_vars_comapare_fri_cas_J_l1 WHERE cas_id IS NULL; -- 26144



/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/

/*  DST INVESTIGATION */


DROP TABLE IF EXISTS casfri50_test.disturbance_pei01_fri_cas;
CREATE TABLE casfri50_test.disturbance_pei01_fri_cas AS
SELECT
c.cas_id,
c.dist_type_1, 
c.dist_type_2, 
n.org_hist, 
n.landtype, 
n.map, 
n.stand, 
n.ogc_fid

FROM rawfri.pe01 n ----- 107220
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_sheet_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS orig_stand_id, --- changed 0 to x
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid --- changed 0 to x
FROM casfri50.dst_all WHERE cas_id ILIKE 'pe01%' 
) c
ON n.map = c.map_sheet_id::INTEGER AND n.stand = c.orig_stand_id::INTEGER AND n.ogc_fid = c.ogc_fid::INTEGER;

/*
================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/


/* ECO INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.eco_pei01_fri_cas;
CREATE TABLE casfri50_test.eco_pei01_fri_cas AS
SELECT
c.cas_id,
c.wetland_type,
c.wet_veg_cover,
c.wet_landform_mod,
c.wet_local_mod,
c.eco_site,
n.org_hist, 
--c.height_lower, c.height_upper, n.height, 
n.landtype,
n.map,
n.stand,
n.ogc_fid
FROM rawfri.pe01 n ----- 107220
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_sheet_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS orig_stand_id, --- changed 0 to x
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid --- changed 0 to x
FROM casfri50.eco_all WHERE cas_id ILIKE 'pe01%' 
) c
ON n.map = c.map_sheet_id::INTEGER AND n.stand = c.orig_stand_id::INTEGER AND n.ogc_fid = c.ogc_fid::INTEGER;


/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/


/* NFL INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.nfl_pei01_fri_cas;
CREATE TABLE casfri50_test.nfl_pei01_fri_cas AS
SELECT
c.cas_id,
c.crown_closure_lower, c.crown_closure_upper, n.crown, 
c.height_lower, c.height_upper, n.height, 
c.nat_non_veg,
c.non_for_anth,
c.non_for_veg,
n.org_hist,  
n.landtype, 
n.map,
n.stand,
n.ogc_fid
	
FROM rawfri.pe01 n ----- 107220
JOIN 
(
	select *, 
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_sheet_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS orig_stand_id, --- changed 0 to x
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid --- changed 0 to x
FROM casfri50.nfl_all WHERE cas_id ILIKE 'pe01%' 
) c
ON n.map = c.map_sheet_id::INTEGER AND n.stand = c.orig_stand_id::INTEGER AND n.ogc_fid = c.ogc_fid::INTEGER;


/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/


/* STAND STRUCTURE INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.stand_structure_pei01;
CREATE TABLE casfri50_test.stand_structure_pei01 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'PEI01%' 
GROUP BY stand_structure;


/* NFL LAYER COUNT */

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_pei01;
CREATE TABLE casfri50_test.nfl_layer_count_pei01 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'PEI01%' 
GROUP BY layer;


