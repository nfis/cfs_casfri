/*
This is a working script to evaluate and validate CASFRI translation of various FRI. Once the CASFRI translation rules deemed good, the following variables were tested and verified they are translated as expected.

Written by: Sishir Gautam

Last Modified Date: May 25, 2021
Last Modified Date: December 09, 2021 by Kangakola
Last Modified Date: April 01, 2022 by Kangakola
Last Modified Date: May 27, 2022 by Kangakola

*/

/*
=============================================================================================================================================================
    -- CASFRI LEVEL 3 VALIDATION.
	
    *** Author: Kangakola omendja.
	*** Purpose: This script evaluate and validate  the layer 1 and layer 2 and several attributes (Species, Height, Crown closure, Origin & Productivity) of layer 1 and layer 2 in CASFRI translated data. 
	   Then assess the accuracy of these attributes between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Update: Integration of crownclosurecompare, heightcompare and siteclasscompare code. Also, add code for validation of dst, eco and nfl tables.	    
	*** Output: Multiple  table of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NB02 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between these attributes for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
	
	
	**** NOTE: This script can be run at once or comment certain part of the script to run just a specific code.
	      MAIN SCRIPT COMPONENTS: 
		                                               1. LYR TABLE PROCESSING
													   2. DST TABLE PROCESSING
													   3. ECO TABLE PROCESSING
													   4. NFL TABLLE PROCESSING
													   5. STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 
													   6.  MULTIPLE INVESTIGATION  FOR GATHERING INTEL ON RELATION BETWEEN NFL AND LYR TABLE
 ================================================================================================================================================================

*/


/*   ================================================================

     1.    LYR TABLE PROCESSING  

    ================================================================
*/



/*   LYR INVESTIGATION */

---------- Sub-setting layer 1 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 were used then they are common infromation to both layers 
-----------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.nb02_rafri_cas_all_l1;
CREATE TABLE casfri50_test.nb02_rafri_cas_all_l1 AS
SELECT
c.cas_id,
c.soil_moist_reg,
c.structure_per,
c.structure_range,
--c.layer,
--c.layer_rank,
c.crown_closure_upper,
c.crown_closure_lower,
c.height_upper,
c.height_lower,
c.productivity,
c.productivity_type,
c.species_1,
c.species_per_1,
c.species_2,
c.species_per_2,
--c.species_3,
--c.species_per_3,
--c.species_4,
--c.species_per_4,
--c.species_5,
--c.species_per_5,
--c.species_6,
--c.species_per_6,
--c.species_7,
--c.species_per_7,
--c.species_8,
--c.species_per_8,
--c.species_9,
--c.species_per_9,
--c.species_10,
--c.species_per_10,
c.origin_upper,
c.origin_lower,
c.site_class,
c.site_index,
n.ogc_fid,
n.nid,
n.poly_id,
n.src_filename,
n.inventory_id,
--n.datasrc,
--n.datayr,
--n.plu,
--n.slu,
--n.status,
--n.lc,
--n.globalid,
--n.wloc,
--n.wc,
--n.wri,
--n.im,
--n.vt,
--n.spvc,
--n.water_code,
--n.watershed,
--n.fst,
n.sitei,
n.voli,
n.origin,
n.trt,
n.trtyr,
n.h1,
n.h1yr,
n.h2,
n.h2yr,
--n.h3,
--n.h3yr,
--n.h4,
--n.h4yr,
n.rf,
n.rfyr,
n.si,
n.siyr,
n.untreati,
n.lpriority,
n.l1datasrc,
n.l1datayr,
n.l1funa,
n.l1estabyr,
n.l1aryr,
n.l1s1,
n.l1ds1,
n.l1pr1,
n.l1s2,
n.l1ds2,
n.l1pr2,
--n.l1s3,
--n.l1ds3,
--n.l1pr3,
--n.l1s4,
--n.l1ds4,
--n.l1pr4,
--n.l1s5,
--n.l1ds5,
--n.l1pr5,
n.l1ds,
n.l1cci,
n.l1cc,
n.l1vol,
n.l1ba,
n.l1pstock,
n.l1vs,
n.l1ht,
n.l1dc,
n.l1sc,
n.harv_id,
n.plant_id,
n.thin_id,
n.lharvtrt
FROM rawfri.nb02 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'nb02%' AND layer = 1
) c
--ON n.poly_id = c.poly_id:: INTEGER AND n.ogc_fid = c.ogc_fid:: INTEGER;
ON n.ogc_fid = c.ogc_fid:: INTEGER;

---------- Sub-setting layer 2 --------------------------
-- Prefix of Column name l1 indicates the top layer and is the layer 1 in the case of CASFRI
-- If no prefix like l1 and l2 used in both layers 
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.nb02_rafri_cas_all_l2;
CREATE TABLE casfri50_test.nb02_rafri_cas_all_l2 AS
SELECT
c.cas_id,
c.soil_moist_reg,
c.structure_per,
c.structure_range,
--c.layer,
--c.layer_rank,
c.crown_closure_upper,
c.crown_closure_lower,
c.height_upper,
c.height_lower,
c.productivity,
c.productivity_type,
c.species_1,
c.species_per_1,
c.species_2,
c.species_per_2,
--c.species_3,
--c.species_per_3,
--c.species_4,
--c.species_per_4,
--c.species_5,
--c.species_per_5,
--c.species_6,
--c.species_per_6,
--c.species_7,
--c.species_per_7,
--c.species_8,
--c.species_per_8,
--c.species_9,
--c.species_per_9,
--c.species_10,
--c.species_per_10,
c.origin_upper,
c.origin_lower,
c.site_class,
c.site_index,
n.ogc_fid,
--n.nid,
n.poly_id,
n.src_filename,
n.inventory_id,
--n.datasrc,
--n.datayr,
--n.plu,
--n.slu,
--n.status,
--n.lc,
--n.globalid,
--n.wloc,
--n.wc,
--n.wri,
--n.im,
--n.vt,
--n.spvc,
--n.water_code,
--n.watershed,
--n.fst,
--n.sitei,
--n.voli,
n.origin,
n.trt,
n.trtyr,
n.h1,
n.h1yr,
n.h2,
n.h2yr,
--n.h3,
--n.h3yr,
--n.h4,
--n.h4yr,
--n.rf,
--n.rfyr,
n.si,
--n.siyr,
n.untreati,
--n.lpriority,
--n.l2datasrc,
--n.l2datayr,
n.l2funa,
n.l2estabyr,
--n.l2aryr,
n.l2s1,
n.l2ds1,
n.l2pr1,
n.l2s2,
n.l2ds2,
n.l2pr2,
--n.l2s3,
--n.l2ds3,
--n.l2pr3,
--n.l2s4,
--n.l2ds4,
--n.l2pr4,
--n.l2s5,
--n.l2ds5,
--n.l2pr5,
n.l2ds,
n.l2cci,
n.l2cc,
--n.l2pstock,
--n.l2nstock,
--n.l2vs,
n.l2ht,
n.l2dc,
n.harv_id,
n.plant_id,
n.thin_id,
n.lharvtrt

FROM rawfri.nb02 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.lyr_all WHERE cas_id ILIKE 'nb02%' AND layer = 2
) c
--ON n.poly_id = c.poly_id:: INTEGER AND n.ogc_fid = c.ogc_fid:: INTEGER;
ON n.ogc_fid = c.ogc_fid:: INTEGER;

/*


  CROWN CLOSURE  

=============================================================================================================================================================
    
	*** Purpose: This code evaluate and validate  the layer 1 and layer 2 and Crown closure of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of this attribute between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NB02 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between this attribute for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/




---------- Combine with Raw Inventory --------------------------
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.nb02_cc_TEST_FRI_CAS;
CREATE TABLE casfri50_test.nb02_cc_TEST_FRI_CAS AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.l1cc as TestVarTL,
	n.l2cc as TestVarLL,
	n.poly_id,
	n.ogc_fid,	
	c.crown_closure_upper_TL, 	 
	c.crown_closure_lower_TL, 
	c.crown_closure_upper_LL, 
	c.crown_closure_lower_LL 

FROM rawfri.nb02 n
JOIN 
(
	select *, 
	--ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename1,
	--ltrim(split_part(cas_id, '-', 3), 'x') AS src_filename2,
	--CONCAT(ltrim(split_part(cas_id, '-', 2), 'x'), '-', ltrim(split_part(cas_id, '-', 3), 'x')) as src_filename,
	--RIGHT(LEFT(cas_id, 20), 15) AS src_filename,
	CAST ('GEONB_FOREST-FO' AS varchar) AS src_filename, --- this seems to derived from original 
	ltrim(split_part(cas_id, '-', 4), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 5), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 6), 'x') AS ogc_fid
FROM casfri50_test.nb02_cc_TEST_CAS
) c

ON n.ogc_fid::TEXT = c.ogc_fid;


-------------------- Checking original and translated variables -----------
----- Top and lower layers ------------------------

DROP TABLE IF EXISTS casfri50_test.test_cc_summary_NB;
CREATE TABLE casfri50_test.test_cc_summary_NB AS
SELECT *,
  CASE WHEN crown_closure_upper_TL = TestVarTL THEN '1' ELSE '0' END AS CheckTestVarTL,
  CASE WHEN crown_closure_upper_LL = TestVarLL THEN '1' ELSE '0' END AS CheckTestVarLL,
  CASE WHEN crown_closure_upper_TL = TestVarLL THEN '1' ELSE '0' END AS LL2TL  
FROM casfri50_test.nb02_cc_TEST_FRI_CAS;




------    CROWN CLOSURE     INVESTIGATION  ---------------------------------------------------------------------

-- CREATE TABLE WHERE testVarTL = '0' for further analysis
DROP TABLE IF EXISTS casfri50_test.nb02_testVarTL_0_from_fri_cas;
CREATE TABLE casfri50_test.nb02_testVarTL_0_from_fri_cas AS
SELECT * FROM casfri50_test.nb02_cc_TEST_FRI_CAS WHERE testVarTL = '0'; -- 148948

DROP TABLE IF EXISTS casfri50_test.nb02_testVarTL_1_from_fri_cas;
CREATE TABLE casfri50_test.nb02_testVarTL_1_from_fri_cas AS
SELECT * FROM casfri50_test.nb02_cc_TEST_FRI_CAS WHERE testVarTL = '1'; -- 19684

DROP TABLE IF EXISTS casfri50_test.nb02_testVarTL_2_from_fri_cas;
CREATE TABLE casfri50_test.nb02_testVarTL_2_from_fri_cas AS
SELECT * FROM casfri50_test.nb02_cc_TEST_FRI_CAS WHERE testVarTL = '2'; -- 87342

DROP TABLE IF EXISTS casfri50_test.nb02_testVarTL_3_from_fri_cas;
CREATE TABLE casfri50_test.nb02_testVarTL_3_from_fri_cas AS
SELECT * FROM casfri50_test.nb02_cc_TEST_FRI_CAS WHERE testVarTL = '3'; -- 262933

DROP TABLE IF EXISTS casfri50_test.nb02_testVarTL_4_from_fri_cas;
CREATE TABLE casfri50_test.nb02_testVarTL_4_from_fri_cas AS
SELECT * FROM casfri50_test.nb02_cc_TEST_FRI_CAS WHERE testVarTL = '4'; -- 311273

DROP TABLE IF EXISTS casfri50_test.nb02_testVarTL_5_from_fri_cas;
CREATE TABLE casfri50_test.nb02_testVarTL_5_from_fri_cas AS
SELECT * FROM casfri50_test.nb02_cc_TEST_FRI_CAS WHERE testVarTL = '5'; -- 40745


--- PRODUCTIVITY INVESTIGATION
/*
=============================================================================================================================================================
 
	*** Purpose: This code evaluate and validate  the layer 1 and layer 2 and Productivity attribute of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of this attribute between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NB02 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between this attribute for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/


---------- Top Layer --------------------------
----------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.nb02_c_productivity_TL;
CREATE TABLE casfri50_test.nb02_c_productivity_TL AS
SELECT 
	cas_id, 
	productivity as productivity_TL, --- in the new CASFRI version productivity_for is broken down into productivity and productiovity 
	productivity_type as productivity_type_TL 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'nb02%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------
DROP TABLE IF EXISTS  casfri50_test.nb02_c_productivity_LL;
CREATE TABLE casfri50_test.nb02_c_productivity_LL AS
SELECT 
	cas_id as cas_id_L2, 
	productivity as productivity_LL, 
	productivity_type as productivity_type_LL 
FROM casfri50.lyr_all 
WHERE cas_id ILIKE 'nb02%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.nb02_c_productivity_CAS;
CREATE TABLE casfri50_test.nb02_c_productivity_CAS AS
SELECT U.cas_id, 
	U.productivity_TL, 
	U.productivity_type_TL, 
	L.productivity_LL,
	L.productivity_type_LL 
FROM casfri50_test.nb02_c_productivity_TL U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	productivity_LL, 
	productivity_type_LL 
FROM casfri50_test.nb02_c_productivity_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.nb02_c_productivity_FRI_CAS;
CREATE TABLE casfri50_test.nb02_c_productivity_FRI_CAS AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.fst as TestVarTL_1,
	n.wc as TestVarTL_2,
	n.poly_id,
	n.ogc_fid,	
	c.productivity_TL, 	 
	c.productivity_type_TL, 
	c.productivity_LL, 
	c.productivity_type_LL 

FROM rawfri.nb02 n
JOIN 
(
	select *, 
	--ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename1,
	--ltrim(split_part(cas_id, '-', 3), 'x') AS src_filename2,
	--CONCAT(ltrim(split_part(cas_id, '-', 2), 'x'), '-', ltrim(split_part(cas_id, '-', 3), 'x')) as src_filename,
	--RIGHT(LEFT(cas_id, 20), 15) AS src_filename,
	CAST ('GEONB_FOREST-FO' AS varchar) AS src_filename, --- this seems to derived from original 
	ltrim(split_part(cas_id, '-', 4), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 5), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 6), 'x') AS ogc_fid
FROM casfri50_test.nb02_c_productivity_CAS
) c

ON n.ogc_fid::TEXT = c.ogc_fid;


--- SPECIES INVESTIGATION


/*
=============================================================================================================================================================
  
	*** Purpose: This code evaluate and validate  the layer 1 and layer 2 and Species compositon of layer 1 and layer 2 in CASFRI translated data. Then assess the accuracy of this attribute between CASFRI and  FRI of Juridiction forest inventory datata. 
	*** Output: Multiple  table of these attributes comparison between layer 1 and layer 2 and join between CASFRI DATA and  RAWFRI for NB02 for both layer 1 & 2. Relevant table are assessed visually to report if the relationship 
	     between this attribute for layer 1 and layer 2.
	*** Software: PgAdmin 4.6
	
 ================================================================================================================================================================

*/


---------- Top Layer --------------------------
----------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.nb02_species_TEST_TL;
CREATE TABLE casfri50_test.nb02_species_TEST_TL AS
SELECT 
	cas_id, 
	species_1 as species_1_TL, 
	species_per_1 as species_per_1_TL 
FROM lyr_all 
WHERE cas_id ILIKE 'nb02%' AND layer =1;


---------- Lower Layer --------------------------
----------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.nb02_speciesTEST_LL;
CREATE TABLE casfri50_test.nb02_speciesTEST_LL AS
SELECT 
	cas_id as cas_id_L2, 
	species_1 as species_1_LL, 
	species_per_1 as species_per_1_LL 
FROM lyr_all 
WHERE cas_id ILIKE 'nb02%' AND layer =2;


---------- Combine top and lower layers ------------------
----------------------------------------------------------------------------------------
DROP TABLE IF EXISTS casfri50_test.nb02_species_TEST_CAS;
CREATE TABLE casfri50_test.nb02_species_TEST_CAS AS
SELECT U.cas_id, 
	U.species_1_TL, 
	U.species_per_1_TL, 
	L.species_1_LL,
	L.species_per_1_LL 
FROM casfri50_test.nb02_species_TEST_TL U 
LEFT JOIN 
(SELECT 
	cas_id_l2, 
	species_1_LL, 
	species_per_1_LL 
FROM casfri50_test.nb02_speciesTEST_LL) L
ON U.cas_id = L.cas_id_l2;


---------- Combine with Raw Inventory --------------------------
-----------------------------------------------------------------

DROP TABLE IF EXISTS casfri50_test.nb02_species_TEST_FRI_CAS;
CREATE TABLE casfri50_test.nb02_species_TEST_FRI_CAS AS
SELECT 
	c.cas_id,
	n.src_filename,
	n.l1s1 as TestVarTL_1,
	n.l2s1 as TestVarLL_1,
	n.l1pr1 as TestVarTL_2,
	n.l2pr1 as TestVarLL_2,
	n.poly_id,
	n.ogc_fid,	
	c.species_1_TL, 	 
	c.species_per_1_TL, 
	c.species_1_LL, 
	c.species_per_1_LL 

FROM rawfri.nb02 n
JOIN 
(
	select *, 
	CAST ('GEONB_FOREST-FO' AS varchar) AS src_filename, --- this seems to derived from original 
	ltrim(split_part(cas_id, '-', 4), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 5), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 6), 'x')AS ogc_fid
FROM casfri50_test.nb02_species_TEST_CAS
) c
ON n.ogc_fid::TEXT = c.ogc_fid;


-------------------- Checking original and translated variables -----------
----- Top and lower layers ------------------------

DROP TABLE IF EXISTS casfri50_test.test_summary_NB;
CREATE TABLE casfri50_test.test_summary_NB AS
SELECT *,
  CASE WHEN species_per_1_TL = TestVarTL_2*10 THEN '1' ELSE '0' END AS CheckTestVarTL,
  CASE WHEN species_per_1_LL = TestVarLL_2*10 THEN '1' ELSE '0' END AS CheckTestVarLL,
  CASE WHEN species_per_1_TL = TestVarLL_2*10 THEN '1' ELSE '0' END AS LL2TL  
FROM casfri50_test.nb02_species_TEST_FRI_CAS;



/*   ================================================================

     2.    DST TABLE PROCESSING  

    ================================================================
*/



/*   DST INVESTIGATION */


DROP TABLE IF EXISTS casfri50_test.disturbance_nb02_cas_all;
CREATE TABLE casfri50_test.disturbance_nb02_cas_all AS
SELECT
c.cas_id,
c.dist_type_1,
c.dist_year_1,
n.ogc_fid,
n.origin,
n.trt,
n.trtyr,
n.h1,
n.h2,
n.h3,
n.h4,
n.untreati
--n.harv_id,
--n.plant_id,
--n.thin_id,
--n.lharvtrt

FROM rawfri.nb02 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.dst_all WHERE cas_id ILIKE 'nb02%' 
) c
ON n.ogc_fid = c.ogc_fid:: INTEGER;


----- DEEP INVESTIGATION --------

DROP TABLE IF EXISTS casfri50_test.disturbance_nb02_cas_fri_summary;
CREATE TABLE casfri50_test.disturbance_nb02_cas_fri_summary AS
SELECT DISTINCT(dist_type_1),trt,h1,untreati,h2,h3,h4,COUNT(*)
FROM casfri50_test.disturbance_nb02_cas_all
GROUP BY dist_type_1,trt,h1,untreati,h2,h3,h4;


/*   ================================================================

     3.    ECO TABLE PROCESSING  

    ================================================================
*/



/*   ECO INVESTIGATION */


DROP TABLE IF EXISTS casfri50_test.eco_nb02_rafri_cas_all;
CREATE TABLE casfri50_test.eco_nb02_rafri_cas_all AS
SELECT
c.cas_id,
c.wetland_type,
c.wet_veg_cover,
c.wet_landform_mod,
c.wet_local_mod,
c.eco_site,
n.ogc_fid,
n.formon,
n.slu,
n.water_code,
n.watershed
--n.trt,
--n.trtyr

FROM rawfri.nb02 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.eco_all WHERE cas_id ILIKE 'nb02%' 
) c
--ON n.poly_id = c.poly_id:: INTEGER AND n.ogc_fid = c.ogc_fid:: INTEGER;
ON n.ogc_fid = c.ogc_fid:: INTEGER;



/*   ================================================================

     4.    NFL TABLE PROCESSING  

    ================================================================
*/


/*   NFL  INVESTIGATION */


DROP TABLE IF EXISTS casfri50_test.nfl_nb02_rafri_cas_all;
CREATE TABLE casfri50_test.nfl_nb02_rafri_cas_all AS
SELECT
c.cas_id,
c.soil_moist_reg,
--c.structure_per,
--c.structure_range,
--c.layer,
--c.layer_rank,
--c.crown_closure_upper,
--c.crown_closure_lower,
--c.height_upper,
--c.height_lower,
c.nat_non_veg,
c.non_for_anth,
c.non_for_veg,
--c.productivity,
--c.productivity_type,
--c.origin_upper,
--c.origin_lower,
--c.site_class,
--c.site_index,
n.ogc_fid,
--n.nid,
--n.poly_id,
--n.src_filename,
--n.inventory_id,
--n.datasrc,
--n.datayr,
--n.plu,
n.slu,
--n.status,
--n.lc,
--n.globalid,
--n.wloc,
--n.wc,
--n.wri,
--n.im,
--n.vt,
--n.spvc,
n.water_code
--n.watershed,
--n.fst,
--n.sitei,
--n.voli,
--n.origin,
--n.trt,
--n.trtyr,
--n.h1,
--n.h1yr,
--n.h2,
--n.h2yr,
--n.h3,
--n.h3yr,
--n.h4,
--n.h4yr,
--n.rf,
--n.rfyr,
--n.si,
--n.siyr,
--n.untreati,
--n.lpriority,
--n.l2datasrc,
--n.l2datayr,
--n.l2funa,
--n.l2estabyr,
--n.l2aryr,
--n.l2s1,
--n.l2ds1,
--n.l2pr1,
--n.l2s2,
--n.l2ds2,
--n.l2pr2,
--n.l2s3,
--n.l2ds3,
--n.l2pr3,
--n.l2s4,
--n.l2ds4,
--n.l2pr4,
--n.l2s5,
--n.l2ds5,
--n.l2pr5,


FROM rawfri.nb02 n 
JOIN (
	select *,
	ltrim(split_part(cas_id, '-', 1), 'x') AS inventory_id,
	ltrim(split_part(cas_id, '-', 2), 'x') AS src_filename,
	ltrim(split_part(cas_id, '-', 3), 'x') AS map_id,	
	ltrim(split_part(cas_id, '-', 4), 'x') AS poly_id, 
	ltrim(split_part(cas_id, '-', 5), 'x') AS ogc_fid
FROM casfri50.nfl_all WHERE cas_id ILIKE 'nb02%' 
) c
--ON n.poly_id = c.poly_id:: INTEGER AND n.ogc_fid = c.ogc_fid:: INTEGER;
ON n.ogc_fid = c.ogc_fid:: INTEGER;

---- INVESTIGATION NFL SUMMARY -----------------------

DROP TABLE IF EXISTS casfri50_test.nfl_nb02_cas_fri_summary;
CREATE TABLE casfri50_test.nfl_nb02_cas_fri_summary AS
SELECT DISTINCT(nat_non_veg),non_for_anth,non_for_veg,slu,water_code,COUNT(*)
FROM casfri50_test.nfl_nb02_rafri_cas_all
GROUP BY nat_non_veg,non_for_anth,non_for_veg,slu,water_code;



/*   ==================================================================================

     5.    STAND STRUCTURE AND NFL LAYER COUNTS PROCESSING 

    ==================================================================================
*/


/* STAND STRUCTURE  INVESTIGATION  */

DROP TABLE IF EXISTS casfri50_test.stand_structure_nb02;
CREATE TABLE casfri50_test.stand_structure_nb02 AS
SELECT stand_structure, count(*)
FROM casfri50.cas_all
WHERE cas_id ILIKE 'NB02%' 
GROUP BY stand_structure;


/* NFL LAYER COUNT INVESTIGATION */

DROP TABLE IF EXISTS casfri50_test.nfl_layer_count_nb02;
CREATE TABLE casfri50_test.nfl_layer_count_nb02 AS
SELECT layer, count(*)
FROM casfri50.nfl_all
WHERE cas_id ILIKE 'NB02%' 
GROUP BY layer;

